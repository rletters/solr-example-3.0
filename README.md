# RLetters Solr Example

**Development on this version of RLetters (3.0) is stopped and has been
superseded by [Sciveyor](https://www.sciveyor.com/).** For a sample Solr server
that works with Sciveyor, see the repository at
<https://codeberg.org/sciveyor/solr-example>.

This repository contains an example Solr instance that can be used with
[RLetters.](https://www.rletters.net) Note that this Solr instance is to be used
with the new, development version of RLetters (3.0 and its betas, beginning in
May 2018), _not_ the currently released version. If you are looking for a Solr
example to go with RLetters 2.x, see
[this other repository.](https://codeberg.org/rletters/solr-example-2.0)

The scripts are those provided by Solr itself. Start the server with
`bin/solr start`, stop it with `bin/solr stop`, and update the data by calling
`bin/solr post -c core1 data/*.xml`.

We are currently bundling Solr 7.3.0.

The data here is taken from two sources: the
[PLoS Neglected Tropical Diseases journal](http://journals.plos.org/plosntds/),
available under the
[CC-BY license,](https://creativecommons.org/licenses/by/2.0/) and from
[Project Gutenberg,](https://www.gutenberg.org/) available under the
[Project Gutenberg license.](https://www.gutenberg.org/wiki/Gutenberg:The_Project_Gutenberg_License)
Licensing is that of Solr itself; see the `LICENSE.txt` file and the `licenses`
folder.
