<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000837</field>
<field name="doi">10.1371/journal.pntd.0000837</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Nadia Wauquier, Pierre Becquart, Cindy Padilla, Sylvain Baize, Eric M. Leroy</field>
<field name="title">Human Fatal Zaire Ebola Virus Infection Is Associated with an Aberrant Innate Immunity and with Massive Lymphocyte Apoptosis</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">10</field>
<field name="pages">e837</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Ebolavirus species Zaire (ZEBOV) causes highly lethal hemorrhagic fever,
resulting in the death of 90% of patients within days. Most information on
immune responses to ZEBOV comes from in vitro studies and animal models. The
paucity of data on human immune responses to this virus is mainly due to the
fact that most outbreaks occur in remote areas. Published studies in this
setting, based on small numbers of samples and limited panels of immunological
markers, have given somewhat different results.

Methodology/Principal Findings

Here, we studied a unique collection of 56 blood samples from 42 nonsurvivors
and 14 survivors, obtained during the five outbreaks that occurred between
1996 and 2003 in Gabon and Republic of Congo. Using Luminex technology, we
assayed 50 cytokines in all 56 samples and performed phenotypic analyses by
flow cytometry. We found that fatal outcome was associated with hypersecretion
of numerous proinflammatory cytokines (IL-1β, IL-1RA, IL-6, IL-8, IL-15 and
IL-16), chemokines and growth factors (MIP-1α, MIP-1β, MCP-1, M-CSF, MIF,
IP-10, GRO-α and eotaxin). Interestingly, no increase of IFNα2 was detected in
patients. Furthermore, nonsurvivors were also characterized by very low levels
of circulating cytokines produced by T lymphocytes (IL-2, IL-3, IL-4, IL-5,
IL-9, IL-13) and by a significant drop of CD3+CD4+ and CD3+CD8+ peripheral
cells as well as a high increase in CD95 expression on T lymphocytes.

Conclusions/Significance

This work, the largest study to be conducted to date in humans, showed that
fatal outcome is associated with aberrant innate immune responses and with
global suppression of adaptive immunity. The innate immune reaction was
characterized by a “cytokine storm,” with hypersecretion of numerous
proinflammatory cytokines, chemokines and growth factors, and by the
noteworthy absence of antiviral IFNα2. Immunosuppression was characterized by
very low levels of circulating cytokines produced by T lymphocytes and by
massive loss of peripheral CD4 and CD8 lymphocytes, probably through Fas/FasL-
mediated apoptosis.

Author Summary

Ebolavirus, especially the species Zaïre (ZEBOV), causes a fulminating
hemorrhagic fever syndrome resulting in the death of most patients within a
few days. In vitro studies and animal models have brought some insight as to
the immune responses to ZEBOV infection. However, human immune responses have
as yet been poorly investigated, mainly due to the fact that most outbreaks
occur in remote areas of central Africa. Published studies, based on small
numbers of biological samples have given conflicting results. We studied a
unique collection of 50 blood samples obtained during five outbreaks that
occurred between 1996 and 2003 in Gabon and Republic of Congo. We measured the
plasma levels of 50 soluble factors known to be involved in immune responses
to viral diseases. For the first time, using a cell staining technique, we
analyzed circulating lymphocytes from ZEBOV-infected patients. We found that
fatal outcome in humans is associated with aberrant innate immunity
characterized by a “cytokine storm,” with hypersecretion of numerous
proinflammatory mediators and by the noteworthy absence of antiviral
interferon. The adaptive response is globall</field></doc>
</add>
