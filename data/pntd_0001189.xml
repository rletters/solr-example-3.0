<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001189</field>
<field name="doi">10.1371/journal.pntd.0001189</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Liesbeth Carolien Van Nieuwenhove, Stijn Rogé, Fatima Balharbi, Tessa Dieltjens, Thierry Laurent, Yves Guisez, Philippe Büscher, Veerle Lejon</field>
<field name="title">Identification of Peptide Mimotopes of Trypanosoma brucei gambiense Variant Surface Glycoproteins</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">6</field>
<field name="pages">e1189</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The current antibody detection tests for the diagnosis of gambiense human
African trypanosomiasis (HAT) are based on native variant surface
glycoproteins (VSGs) of Trypanosoma brucei (T.b.) gambiense. These native VSGs
are difficult to produce, and contain non-specific epitopes that may cause
cross-reactions. We aimed to identify mimotopic peptides for epitopes of T.b.
gambiense VSGs that, when produced synthetically, can replace the native
proteins in antibody detection tests.

Methodology/Principal Findings

PhD.-12 and PhD.-C7C phage display peptide libraries were screened with mouse
monoclonal antibodies against the predominant VSGs LiTat 1.3 and LiTat 1.5 of
T.b. gambiense. Thirty seven different peptide sequences corresponding to a
linear LiTat 1.5 VSG epitope and 17 sequences corresponding to a discontinuous
LiTat 1.3 VSG epitope were identified. Seventeen of 22 synthetic peptides
inhibited the binding of their homologous monoclonal to VSG LiTat 1.5 or LiTat
1.3. Binding of these monoclonal antibodies to respectively six and three
synthetic mimotopic peptides of LiTat 1.5 and LiTat 1.3 was significantly
inhibited by HAT sera (p&lt;0.05).

Conclusions/Significance

We successfully identified peptides that mimic epitopes on the native
trypanosomal VSGs LiTat 1.5 and LiTat 1.3. These mimotopes might have
potential for the diagnosis of human African trypanosomiasis but require
further evaluation and testing with a large panel of HAT positive and negative
sera.

Author Summary

The control of human African trypanosomiasis or sleeping sickness, a deadly
disease in sub-Saharan Africa, mainly depends on a correct diagnosis and
treatment. The aim of our study was to identify mimotopic peptides (mimotopes)
that may replace the native proteins in antibody detection tests for sleeping
sickness and hereby improve the diagnostic sensitivity and specificity. We
selected peptide expressing phages from the PhD.-12 and PhD.-C7C phage display
libraries with mouse monoclonal antibodies specific to variant surface
glycoprotein (VSG) LiTat 1.3 or LiTat 1.5 of Trypanosoma brucei gambiense. The
peptide coding genes of the selected phages were sequenced and the
corresponding peptides were synthesised. Several of the synthetic peptides
were confirmed as mimotopes for VSG LiTat 1.3 or LiTat 1.5 since they were
able to inhibit the binding of their homologous monoclonal to the
corresponding VSG. These peptides were biotinylated and their diagnostic
potential was assessed with human sera. We successfully demonstrated that
human sleeping sickness sera recognise some of the mimotopes of VSG LiTat 1.3
and LiTat 1.5, indicating the diagnostic potential of such peptides.

Introduction

Human African trypanosomiasis (HAT), or sleeping sickness, is caused by the
protozoan flagellar parasites Trypanosoma brucei (T.b.) gambiense and T.b.
rhodesiense. The disease is transmitted by tsetse flies (Glossina spp.) and
therefore only occurs in sub-Saharan Africa. The number of cases is currently
estimated between 50 000 and 70 000 [1].

Control of T.b. gambiense HAT is largely based on accurate diagnosis and
treatment of the human reservoir [2]. Detection of parasites in blood, lymph
node aspirate or cerebrospinal flui</field></doc>
</add>
