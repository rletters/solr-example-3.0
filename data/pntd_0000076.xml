<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000076</field>
<field name="doi">10.1371/journal.pntd.0000076</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Sara Lustigman, James P. McCarter</field>
<field name="title">Ivermectin Resistance in Onchocerca volvulus: Toward a Genetic Basis</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2007</field>
<field name="volume">1</field>
<field name="number">1</field>
<field name="pages">e76</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Background

Onchocerciasis (river blindness) is a human disease caused by the filarial
worm Onchocerca volvulus. Adult worms can live for over a decade in skin
nodules of affected humans, releasing millions of microfilariae that cause
debilitating itching and blindness [1]. An estimated 37 million people are
infected [2], and there are 46,000 new cases of blindness annually
(http://www.apoc.bf/).

International programs supported by the World Health Organization and many
other groups have worked to control the impact of onchocerciasis using vector
control with insecticides beginning in 1974 and mass drug administration (MDA)
with ivermectin (IVM, brand name Mectizan) beginning in 1987 (Figure 1) [3].
IVM is a highly effective microfilaricide and inhibits female worm
microfilarial production for several months. Annual IVM MDA reduces morbidity
[4,5] and lowers transmission [6,7]. From 1974 to 2002, the Onchocerciasis
Control Programme (OCP) in West Africa greatly decreased O. volvulus
transmission in the 11 OCP countries and prevented 600,000 cases of blindness
[8–10]. IVM without vector control has been the principal tool for the
Onchocerciasis Elimination Program of the Americas (1992–present) [9] and the
African Programme for Onchocerciasis Control (1995–present). In the Americas,
where O. volvulus is less common, the Onchocerciasis Elimination Program has
substantially reduced transmission and is on track to eliminate the disease
[9].

Figure 1

Distribution of Onchocerciasis Showing Current Status of Global Onchocerciasis
Control.

Red shading represents areas receiving ivermectin treatment. Yellow shading
represents areas requiring further epidemiological surveys. Green shading
indicates the area covered by the OCP in West Africa. Pink zones indicate the
special intervention zones, i.e., previous OCP areas receiving ivermectin and
some vector control. Figure from [10].

The African Programme for Onchocerciasis Control has extended treatment to 19
countries beyond those originally included in the OCP through sustainable
community-directed IVM treatment [1,11]. By the end of 2005, 400 million
treatments had been supplied in Africa by Merck’s Mectizan Donation Program,
with an estimated 40 million people treated by nearly 300,000 community
distributors (http://www.apoc.bf/). Nevertheless, the ecology of the disease
in Africa, including the broad geographic range of O. volvulus and its
blackfly vector, leads to the estimation that IVM treatment of at least 65% of
the population for 25 or more years will be necessary to eliminate infection
[9,12]. There are significant logistical obstacles to achieving such broad-
ranging and prolonged treatment, and there is also concern that O. volvulus
resistance to IVM will emerge. IVM resistance has become widespread in many
parasitic nematodes of livestock [13,14]. At present there are no alternative
drugs for IVM for use in the Onchocerca MDA programs that reduce microfilariae
or kill adult worms, which can live up to 15 years in the human host.

The emergence of drug-resistant O. volvulus has been suggested by reports of
patients failing to respond to IVM treatment [15,16]. A recent report from
Ghana has provided the first proof of IVM resistance in O. volvulus: Mike
Osei-Atweneboana and colleagues showed that the ability of IVM to suppress
skin microfilariae repopulation was reduced in som</field></doc>
</add>
