<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000981</field>
<field name="doi">10.1371/journal.pntd.0000981</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Estrella Lasry-Levy, Aki Hietaharju, Vivek Pai, Ramaswamy Ganapati, Andrew S. C. Rice, Maija Haanpää, Diana N. J. Lockwood</field>
<field name="title">Neuropathic Pain and Psychological Morbidity in Patients with Treated Leprosy: A Cross-Sectional Prevalence Study in Mumbai</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">3</field>
<field name="pages">e981</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Neuropathic pain has been little studied in leprosy. We assessed the
prevalence and clinical characteristics of neuropathic pain and the validity
of the Douleur Neuropathique 4 questionnaire as a screening tool for
neuropathic pain in patients with treated leprosy. The association of
neuropathic pain with psychological morbidity was also evaluated.

Methodology/Principal Findings

Adult patients who had completed multi-drug therapy for leprosy were recruited
from several Bombay Leprosy Project clinics. Clinical neurological
examination, assessment of leprosy affected skin and nerves and pain
evaluation were performed for all patients. Patients completed the Douleur
Neuropathique 4 and the 12-item General Health Questionnaire to identify
neuropathic pain and psychological morbidity.

Conclusions/Significance

One hundred and one patients were recruited, and 22 (21.8%) had neuropathic
pain. The main sensory symptoms were numbness (86.4%), tingling (68.2%),
hypoesthesia to touch (81.2%) and pinprick (72.7%). Neuropathic pain was
associated with nerve enlargement and tenderness, painful skin lesions and
with psychological morbidity. The Douleur Neuropathique 4 had a sensitivity of
100% and specificity of 92% in diagnosing neuropathic pain. The Douleur
Neuropathique 4 is a simple tool for the screening of neuropathic pain in
leprosy patients. Psychological morbidity was detected in 15% of the patients
and 41% of the patients with neuropathic pain had psychological morbidity.

Author Summary

Neuropathic pain has only recently been recognised as a complication of
leprosy. We assessed 101 treated leprosy patients in Mumbai and found that 22
of them had neuropathic pain. The pain occurred as numbness 86%, tingling 68%,
and decreased sensation to light touch 81%. This pain was significantly
associated with nerve enlargement and tenderness, which suggests that ongoing
inflammation may be important in causation. A questionnaire-based screening
tool (Douleur Neuropathique 4) for detecting neuropathic pain has been
developed and validated in other patients groups. We are the first group to
have used the DN4 as a screening tool in leprosy patients and found that it
worked well, detecting 78% of patients with no inappropriate diagnoses. There
is also an increasing recognition that leprosy is associated with
psychological morbidity. Neuropathic pain is also associated with
psychological morbidity. We also assessed psychological morbidity using the
12-item General Health Questionnaire and found that neuropathic pain and
psychological morbidity are associated with leprosy patients. Leprosy patients
with neuropathic pain thus have a double hit for psychological morbidity.
Clinicians looking after leprosy patients should warn their patients about
neuropathic pain and assess their patients for psychological morbidity.

Introduction

Leprosy

Leprosy is a chronic granulomatous disease caused by Mycobacterium leprae that
principally affects skin and peripheral nerves.[1] Leprosy is still present
throughout the tropics and sub-tropics. Worldwide 249,007 new cases were
registered in 2008 with India registering 134,184.[2] Leprosy affects
peripheral nerves causing enlargement, sensory loss and motor </field></doc>
</add>
