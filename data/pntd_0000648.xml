<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000648</field>
<field name="doi">10.1371/journal.pntd.0000648</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Shuyi Zhang, Charles C. Kim, Sajeev Batra, James H. McKerrow, P'ng Loke</field>
<field name="title">Delineation of Diverse Macrophage Activation Programs in Response to Intracellular Parasites and Cytokines</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">3</field>
<field name="pages">e648</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The ability to reside and proliferate in macrophages is characteristic of
several infectious agents that are of major importance to public health,
including the intracellular parasites Trypanosoma cruzi (the etiological agent
of Chagas disease) and Leishmania species (etiological agents of Kala-Azar and
cutaneous leishmaniasis). Although recent studies have elucidated some of the
ways macrophages respond to these pathogens, the relationships between
activation programs elicited by these pathogens and the macrophage activation
programs elicited by bacterial pathogens and cytokines have not been
delineated.

Methodology/Principal Findings

To provide a global perspective on the relationships between macrophage
activation programs and to understand how certain pathogens circumvent them,
we used transcriptional profiling by genome-wide microarray analysis to
compare the responses of mouse macrophages following exposure to the
intracellular parasites T. cruzi and Leishmania mexicana, the bacterial
product lipopolysaccharide (LPS), and the cytokines IFNG, TNF, IFNB, IL-4,
IL-10, and IL-17. We found that LPS induced a classical activation state that
resembled macrophage stimulation by the Th1 cytokines IFNG and TNF. However,
infection by the protozoan pathogen L. mexicana produced so few
transcriptional changes that the infected macrophages were almost
indistinguishable from uninfected cells. T. cruzi activated macrophages
produced a transcriptional signature characterized by the induction of
interferon-stimulated genes by 24 h post-infection. Despite this delayed IFN
response by T. cruzi, the transcriptional response of macrophages infected by
the kinetoplastid pathogens more closely resembled the transcriptional
response of macrophages stimulated by the cytokines IL-4, IL-10, and IL-17
than macrophages stimulated by Th1 cytokines.

Conclusions/Significance

This study provides global gene expression data for a diverse set of
biologically significant pathogens and cytokines and identifies the
relationships between macrophage activation states induced by these stimuli.
By comparing macrophage activation programs to pathogens and cytokines under
identical experimental conditions, we provide new insights into how macrophage
responses to kinetoplastids correlate with the overall range of macrophage
activation states.

Author Summary

Macrophages are a type of immune cell that engulf and digest microorganisms.
Despite their role in protecting the host from infection, many pathogens have
developed ways to hijack the macrophage and use the cell for their own
survival and proliferation. This includes the parasites Trypanosoma cruzi and
Leishmania mexicana. In order to gain further understanding of how these
pathogens interact with the host macrophage, we compared macrophages that have
been infected with these parasites to macrophages that have been stimulated in
a number of different ways. Macrophages can be activated by a wide variety of
stimuli, including common motifs found on pathogens (known as pathogen
associated molecular patterns or PAMPs) and cytokines secreted by other immune
cells. In this study, we have delineated the relationships between the
macrophage activation programs elicited by a number of cytokines and PAMPs.
Furthermore, we have placed</field></doc>
</add>
