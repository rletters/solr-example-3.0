<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000829</field>
<field name="doi">10.1371/journal.pntd.0000829</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Daniel P. Depledge, Lorna M. MacLean, Michael R. Hodgkinson, Barbara A. Smith, Andrew P. Jackson, Saufung Ma, Silvia R. B. Uliana, Deborah F. Smith</field>
<field name="title">Leishmania-Specific Surface Antigens Show Sub-Genus Sequence Variation and Immune Recognition</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">9</field>
<field name="pages">e829</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

A family of hydrophilic acylated surface (HASP) proteins, containing extensive
and variant amino acid repeats, is expressed at the plasma membrane in
infective extracellular (metacyclic) and intracellular (amastigote) stages of
Old World Leishmania species. While HASPs are antigenic in the host and can
induce protective immune responses, the biological functions of these
Leishmania-specific proteins remain unresolved. Previous genome analysis has
suggested that parasites of the sub-genus Leishmania (Viannia) have lost HASP
genes from their genomes.

Methods/Principal Findings

We have used molecular and cellular methods to analyse HASP expression in New
World Leishmania mexicana complex species and show that, unlike in L. major,
these proteins are expressed predominantly following differentiation into
amastigotes within macrophages. Further genome analysis has revealed that the
L. (Viannia) species, L. (V.) braziliensis, does express HASP-like proteins of
low amino acid similarity but with similar biochemical characteristics, from
genes present on a region of chromosome 23 that is syntenic with the
HASP/SHERP locus in Old World Leishmania species and the L. (L.) mexicana
complex. A related gene is also present in Leptomonas seymouri and this may
represent the ancestral copy of these Leishmania-genus specific sequences. The
L. braziliensis HASP-like proteins (named the orthologous (o) HASPs) are
predominantly expressed on the plasma membrane in amastigotes and are
recognised by immune sera taken from 4 out of 6 leishmaniasis patients tested
in an endemic region of Brazil. Analysis of the repetitive domains of the
oHASPs has shown considerable genetic variation in parasite isolates taken
from the same patients, suggesting that antigenic change may play a role in
immune recognition of this protein family.

Conclusions/Significance

These findings confirm that antigenic hydrophilic acylated proteins are
expressed from genes in the same chromosomal region in species across the
genus Leishmania. These proteins are surface-exposed on amastigotes (although
L. (L.) major parasites also express HASPB on the metacyclic plasma membrane).
The central repetitive domains of the HASPs are highly variant in their amino
acid sequences, both within and between species, consistent with a role in
immune recognition in the host.

Author Summary

Single-celled Leishmania parasites, transmitted by sand flies, infect humans
and other mammals in many tropical and sub-tropical regions, giving rise to a
spectrum of diseases called the leishmaniases. Species of parasite within the
Leishmania genus can be divided into two groups (referred to as sub-genera)
that are separated by up to 100 million years of evolution yet are highly
related at the genome level. Our research is focused on identifying gene
differences between these sub-genera that may identify proteins that impact on
the transmission and pathogenicity of different Leishmania species. Here we
report the presence of a highly-variant genomic locus (OHL) that was
previously described as absent in parasites of the L. (Viannia) subgenus (on
the basis of lack of key genes) but is present and well-characterised (as the
LmcDNA16 locus) in all members of the a</field></doc>
</add>
