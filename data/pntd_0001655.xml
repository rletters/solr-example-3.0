<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001655</field>
<field name="doi">10.1371/journal.pntd.0001655</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Linda J. Wammes, Firdaus Hamid, Aprilianto E. Wiria, Heri Wibowo, Erliyani Sartono, Rick M. Maizels, Hermelijn H. Smits, Taniawati Supali, Maria Yazdanbakhsh</field>
<field name="title">Regulatory T Cells in Human Lymphatic Filariasis: Stronger Functional Activity in Microfilaremics</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">5</field>
<field name="pages">e1655</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Infection with filarial parasites is associated with T cell
hyporesponsiveness, which is thought to be partly mediated by their ability to
induce regulatory T cells (Tregs) during human infections. This study
investigates the functional capacity of Tregs from different groups of
filarial patients to suppress filaria-specific immune responses during human
filariasis. Microfilaremic (MF), chronic pathology (CP) and uninfected endemic
normal (EN) individuals were selected in an area endemic for Brugia timori in
Flores island, Indonesia. PBMC were isolated, CD4CD25hi cells were
magnetically depleted and in vitro cytokine production and proliferation in
response to B. malayi adult worm antigen (BmA) were determined in total and
Treg-depleted PBMC. In MF subjects BmA-specific T and B lymphocyte
proliferation as well as IFN-gamma, IL-13 and IL-17 responses were lower
compared to EN and CP groups. Depletion of Tregs restored T cell as well as B
cell proliferation in MF-positives, while proliferative responses in the other
groups were not enhanced. BmA-induced IL-13 production was increased after
Treg removal in MF-positives only. Thus, filaria-associated Tregs were
demonstrated to be functional in suppressing proliferation and possibly Th2
cytokine responses to BmA. These suppressive effects were only observed in the
MF group and not in EN or CP. These findings may be important when considering
strategies for filarial treatment and the targeted prevention of filaria-
induced lymphedema.

Author Summary

Lymphatic filariasis is a neglected disease still prominent in low-resource
settings and is very disabling when it progresses to chronic pathology caused
by lymphedema. Until now, studies on the contribution of Tregs to lymphocyte
hyporesponsiveness in human filariasis have focused on frequency and
phenotypic characteristics of these cells. We have looked at the functional
consequence of the presence of Tregs in filaria-specific immune responses
during different stages of human lymphatic filariasis. Proliferation of not
only T cells, but also B cells, was decreased in patients with microfilaremia
compared to uninfected individuals and chronic pathology (lymphedema)
patients. The suppressed lymphocyte proliferative responses were increased
after in vitro removal of Tregs in the microfilaria-positive group only,
indicating the presence of filaria-specific functional Tregs in microfilaremic
patients which are not as active in subjects with chronic pathology or without
infection. Th2 cytokine responses were specifically enhanced in
microfilaremics as well after Treg depletion, suggesting Treg-associated
suppression of filaria-specific Th2 responses. Taken together, filaria-
specific Treg contribute to immune modulation during microfilaremia and might
need to be considered in therapeutic strategies to prevent chronic pathology
induced by filarial infection.

Introduction

Lymphatic filariasis (LF), caused by nematodes Wuchereria bancrofti, Brugia
malayi and B. timori, affects around 120 million people worldwide and
additionally 2 billion people are at risk in endemic areas [1]. Although not
life-threatening, chronic manifestation of disease causes major disabilities
and deformities, especiall</field></doc>
</add>
