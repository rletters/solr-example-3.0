<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001350</field>
<field name="doi">10.1371/journal.pntd.0001350</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Gilda Grard, Jan Felix Drexler, Joseph Fair, Jean-Jacques Muyembe, Nathan D. Wolfe, Christian Drosten, Eric M. Leroy</field>
<field name="title">Re-Emergence of Crimean-Congo Hemorrhagic Fever Virus in Central Africa</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">10</field>
<field name="pages">e1350</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Crimean-Congo hemorrhagic fever (CCHF) is a severe tick-borne disease well
recognized through Europe and Asia where diagnostic tests and medical
surveillance are available. However, it is largely neglected in Africa,
especially in the tropical rainforest of Central Africa where only sporadic
human cases have been reported and date back to more than 30 years. We
describe here an isolated human case that occurred in the Democratic Republic
of the Congo in 2008 and performed phylogenetic analysis to investigate
whether it resulted from a regional re-emergence or from the introduction of a
novel virus in the area.

Methods and Findings

Near complete segment S and partial segment M sequences were characterized.
Bayesian phylogenetic analysis and datation were performed to investigate the
relationship between this new strain and viral strains from Africa, Europe and
Asia. The new strain is phylogenetically close to the previously described
regional genotype (II) that appears to be specific to Central Africa.
Phylogenetic discrepancy between segment S and M suggested genetic exchange
among local sublineages, which was dated back to 130–590 years before present.

Conclusions

The phylogenetic analyses presented here suggest ongoing CCHF virus
circulation in Central Africa for a long time despite the absence of reported
human cases. Many infections have most probably been overlooked, due to the
weakness of healthcare structures and the absence of available diagnostic
procedure. However, despite the lack of accurate ecological data, the sporadic
reporting of human cases could also be partly associated with a specific
sylvatic cycle in Central Africa where deforestation may raise the risks of
re-emergence. For these reasons, together with the high risk of nosocomial
transmission, public health authorities&apos; attention should be drawn to this
etiological agent.

Author Summary

Crimean-Congo hemorrhagic fever virus (CCHFV) is transmitted to humans through
tick-bite or contact with infected blood or tissues from livestock, the main
vertebrate hosts in a peri-domestic natural cycle. With numerous outbreaks, a
high case fatality rate (3%–30%) and a high risk for nosocomial transmission,
CCHFV became a public health concern in Europe and Asia. However virus
surveillance in Africa is difficult due to the limited sanitary facilities.
Especially, CCHFV occurrence in Central Africa is very poorly described and
seems highly in contrast with the temperate to dry environments to which the
virus is usually associated with. We described a single human infection that
occurred in Democratic Republic of the Congo after nearly 50 years of absence.
The phylogenetic analysis suggests that CCHFV enzootic circulation in the area
is still ongoing despite the absence of notification, and thus reinforces the
need for the medical workers and authorities to be aware of the outbreak risk.
The source of infection seemed associated with a forest environment while no
link with the usual agro-pastoral risk factors could be identified. More
accurate ecological data about CCHFV enzootic cycle are required to assess the
risk of emergence in developing countries subjected to deforestation.

Introduction

Crimean-Congo hemorrhagic fever virus (CCHFV, family Bunyaviridae,</field></doc>
</add>
