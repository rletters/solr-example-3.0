<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000925</field>
<field name="doi">10.1371/journal.pntd.0000925</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">John Giles, A. Townsend Peterson, Alzira Almeida</field>
<field name="title">Ecology and Geography of Plague Transmission Areas in Northeastern Brazil</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">1</field>
<field name="pages">e925</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Plague in Brazil is poorly known and now rarely seen, so studies of its
ecology are difficult. We used ecological niche models of historical
(1966-present) records of human plague cases across northeastern Brazil to
assess hypotheses regarding environmental correlates of plague occurrences
across the region. Results indicate that the apparently focal distribution of
plague in northeastern Brazil is indeed discontinuous, and that the causes of
the discontinuity are not necessarily only related to elevation—rather, a
diversity of environmental dimensions correlate to presence of plague foci in
the region. Perhaps most interesting is that suitable areas for plague show
marked seasonal variation in photosynthetic mass, with peaks in April and May,
suggesting links to particular land cover types. Next steps in this line of
research will require more detailed and specific examination of reservoir
ecology and natural history.

Author Summary

We analyzed the spatial and environmental distributions of human plague cases
across northeastern Brazil from 1966-present, where the disease is now only
rarely transmitted to humans, but persists as a zoonosis of native rodent
populations. We elucidated environmental correlates of plague occurrences by
way of ecological niche modeling techniques utilizing advanced satellite
imagery and geospatial datasets to better understand the ecology and geography
of the transmission of plague. Our analysis indicates that plague foci in
Brazil are indeed insular as previously suggested. Furthermore, distribution
of such foci are likely not directly dependent on elevation, and rather are
contigent on climate and vegetation. Seasonality of zoonotic plague
transmission is linked to variations of these ecological parameters-
particularly the increase in precipitation and primary production of the rainy
season. Spatial analysis of transmission events afford a broad view of
potential plague foci distributions across northeastern Brazil and indicate
that the epidemiology of plague is driven by a dynamic array of environmental
factors.

Introduction

Plague arrived in Brazil during the Third Pandemic, in October 1899, imported
by ship traffic to Santos, in São Paulo state, and was rapidly diffused to
other coastal cities. By 1906, it had dispersed by means of land and sea
commerce more broadly, and had become established in native rodent
populations, particularly in the northeastern sector of the country [1], [2].
Nonetheless, records of plague in Brazil are sparse through the 1920s, making
detailed tracking of the pattern of spread of the disease in the region
difficult or impossible [1], [3].

Only by around 1936 were data on plague and its control in Brazil regularly
collated and archived. Based on analyses of these data (for 1936–1966),
Baltazard [1] identified numerous distinct plague foci occurring in different
environmental contexts, a viewpoint that was updated by Vieira &amp; Coelho [4],
based principally on elevation. These foci appear to exist independently of
one another in time and space [1], and overall numbers of human cases varied
from 20 to 100 until the 1970s. Since that time, all of the foci entered a
period of relative inactivity, with few or no human cases [5], [6], [7], [8].
The last significant outbreak in Brazil was in the late 1980s in Paraíba [9].

The purpose of thi</field></doc>
</add>
