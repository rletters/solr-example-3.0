<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000980</field>
<field name="doi">10.1371/journal.pntd.0000980</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Jorge Augusto de Oliveira Guerra, Suzane Ribeiro Prestes, Henrique Silveira, Leila Inês de Aguiar Raposo Câmara Coelho, Pricila Gama, Aristoteles Moura, Valdir Amato, Maria das Graças Vale Barbosa, Luiz Carlos de Lima Ferreira</field>
<field name="title">Mucosal Leishmaniasis Caused by Leishmania (Viannia) braziliensis and Leishmania (Viannia) guyanensis in the Brazilian Amazon</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">3</field>
<field name="pages">e980</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Leishmania (Viannia) braziliensis is a parasite recognized as the most
important etiologic agent of mucosal leishmaniasis (ML) in the New World. In
Amazonia, seven different species of Leishmania, etiologic agents of human
Cutaneous Leishmaniasis, have been described. Isolated cases of ML have been
described for several different species of Leishmania: L. (V.) panamensis, L.
(V.) guyanensis and L. (L.) amazonensis.

Methodology

Leishmania species were characterized by polymerase chain reaction (PCR) of
tissues taken from mucosal biopsies of Amazonian patients who were diagnosed
with ML and treated at the Tropical Medicine Foundation of Amazonas (FMTAM) in
Manaus, Amazonas state, Brazil. Samples were obtained retrospectively from the
pathology laboratory and prospectively from patients attending the
aforementioned tertiary care unit.

Results

This study reports 46 cases of ML along with their geographical origin, 30
cases caused by L. (V.) braziliensis and 16 cases by L. (V.) guyanensis. This
is the first record of ML cases in 16 different municipalities in the state of
Amazonas and of simultaneous detection of both species in 4 municipalities of
this state. It is also the first record of ML caused by L. (V.) guyanensis in
the states of Pará, Acre, and Rondônia and cases of ML caused by L. (V.)
braziliensis in the state of Rondônia.

Conclusions/Significance

L. (V.) braziliensis is the predominant species that causes ML in the Amazon
region. However, contrary to previous studies, L. (V.) guyanensis is also a
significant causative agent of ML within the region. The clinical and
epidemiological expression of ML in the Manaus region is similar to the rest
of the country, although the majority of ML cases are found south of the
Amazon River.

Author Summary

Leishmaniasis is considered a neglected disease with 1.5 million new cases of
cutaneous leishmaniasis (CL) occurring each year. In the Amazon region and in
the Americas in general, ML is caused by Leishmania (Viannia) braziliensis,
though in rare cases it has been related to other species. ML, which is
associated with inadequate treatment of CL, normally manifests itself years
after the occurrence of CL. Clinical features evolve slowly and most often
affect the nasal cavity, in some cases causing perforation, or even
destruction, of the septum. Diagnosis is made using the Montenegro skin test,
serology and histopathology of the patients&apos; mucosal tissues, or by isolation
of the parasites. PCR is the best way to identify the species of leishmaniasis
and is therefore the diagnostic method of choice. This paper describes 46
cases of ML and their geographical origin, 30 cases associated with L. (V.)
braziliensis and 16 with L. (V.) guyanensis. The species of leishmaniasis was
identified using mucosal biopsies taken from Amazonian patients who were
diagnosed and treated for ML in the tertiary care unit, in Manaus, Amazonas
state, Brazil. This is the highest number of ML cases caused by L. (V.)
guyanensis that has ever been reported.

Introduction

Mucosal leishmaniasis (ML) in the Americas is mainly associated with </field></doc>
</add>
