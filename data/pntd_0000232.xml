<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000232</field>
<field name="doi">10.1371/journal.pntd.0000232</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Banchob Sripa</field>
<field name="title">Concerted Action Is Needed to Tackle Liver Fluke Infections in Asia</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">5</field>
<field name="pages">e232</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Human liver fluke infection caused by Clonorchis sinensis, Opisthorchis
viverrini, and O. felineus remains a major public health problem, affecting
the poor in the poorest regions of Asia. An estimated 45 million people are
infected and more than 600 million are at risk of these infections [1],[2].
This Viewpoint highlights the distribution of the infection, clinical
features, neglect of liver fluke infection, and how the global health
community can help to eradicate the infection in Asia.

Distribution of the Infection

Most of those infected with liver fluke live in Asia. C. sinensis is endemic
in south China, northern Vietnam, Taiwan, and Korea, while O. viverrini is
found mainly in Thailand, Lao People&apos;s Democratic Republic (PDR), Cambodia,
and central Vietnam. In Northeast Thailand and Laos, despite widespread
administration of the anthelmintic drug praziquantel, the prevalence of O.
viverrini approaches 85% in endemic areas in Lao PDR [3]. A recent survey in
the Chi River basin in northeast Thailand found a prevalence of O. viverrini
of up to 78% in certain villages (B. Sripa, T. Laha, J. Bethony, P. J.
Brindley, et al., unpublished data). An estimated 6 million people in Thailand
are infected with Opisthorchis [4].

Clinical Features

Liver fluke infections are associated with several hepatobiliary diseases,
including cholangitis, cholecystitis, gallstones, hepatomegaly, and
cholangiocarcinoma (CCA), the primary liver cancer that arises from biliary
epithelial cells [5]. Opisthorchis is one of two helminth parasites (O.
viverrini and Schistosoma hematobium) that are carcinogenic to humans, as
reported by the World Health Organization and the International Agency for
Research on Cancer [6]. In Thailand, CCA is the most prevalent of the fatal
neoplasias, and liver and bile duct cancer ranks at number five in the list of
diseases in Thailand that cause the highest number of disability adjusted life
years (DALYs) [7]. CCA is responsible for about 15%–25% of liver cancers in
the United States but represents 86.5% of all cancers in Thailand&apos;s Khon Kaen
region, the highest incidence in the world (Figure 1) [5].

Figure 1

Incidence of CCA and O. viverrini in Thailand from 1990 to 2001.

(A) Increasing intensity of red represents increasing prevalence of O.
viverrini, while increasing number of dots represents increasing cancer rates.
In general, higher O. viverrini prevalence correlates with a higher CCA
burden, although sporadic anthelmintic therapy has influenced this
relationship. It should be noted that even one spot represents significant
cancer rates anywhere else in the world. *Truncated age-standardized incidence
from 35 to 64 years. **Age-standardized incidence of CCA throughout registered
regions [6]. (B–E) Cyrinoid fishes that represent the intermediate host of the
O. viverrini parasite (B); a dish of koi-pla (minced fish and condiments),
which is thought to represent a common source of infectious metaceraciae of O.
viverrini (C); photomicrograph of an adult O. viverrini worm in bile ducts of
experimentally infected hamster (D); photograph of cholangiocarcinoma in human
liver from a patient from Khon Kaen, Thailand (E). (Modified from Sripa et al.
[5].)

The Neglect of Liver Fluke Infections

Despite the high prevalence of liver flukes in Asia, and the associated
hepatobiliary disease, these infections are relatively neglected, not on</field></doc>
</add>
