<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000612</field>
<field name="doi">10.1371/journal.pntd.0000612</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Christian A. Ganoza, Michael A. Matthias, Mayuko Saito, Manuel Cespedes, Eduardo Gotuzzo, Joseph M. Vinetz</field>
<field name="title">Asymptomatic Renal Colonization of Humans in the Peruvian Amazon by Leptospira</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">2</field>
<field name="pages">e612</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Renal carriage and shedding of leptospires is characteristic of carrier or
maintenance animal hosts. Sporadic reports indicate that after infection,
humans may excrete leptospires for extended periods. We hypothesized that,
like mammalian reservoir hosts, humans develop asymptomatic leptospiruria in
settings of high disease transmission such as the Peruvian Amazon.

Methodology/Principal Findings

Using a cross-sectional study design, we used a combination of epidemiological
data, serology and molecular detection of the leptospiral 16S rRNA gene to
identify asymptomatic urinary shedders of Leptospira. Approximately one-third
of the 314 asymptomatic participants had circulating anti-leptospiral
antibodies. Among enrolled participants, 189/314 (59%) had evidence of recent
infection (microscopic agglutination test (MAT0 ≥1∶800 or ELISA IgM-positive
or both). The proportion of MAT-positive and high MAT-titer (≥1∶800) persons
was higher in men than women (p = 0.006). Among these people, 13/314 (4.1%)
had Leptospira DNA-positive urine samples. Of these, the 16S rRNA gene from 10
samples was able to be sequenced. The urine-derived species clustered within
both pathogenic (n = 6) and intermediate clades of Leptospira (n = 4). All of
the thirteen participants with leptospiral DNA in urine were women. The median
age of the DNA-positive group was older compared to the negative group
(p≤0.05). A group of asymptomatic participants (“long-term asymptomatic
individuals,” 102/341 (32.5%) of enrolled individuals) without serological
evidence of recent infection was identified; within this group, 6/102 (5.9%)
excreted pathogenic and intermediate-pathogenic Leptospira (75–229 bacteria/mL
of urine).

Conclusions/Significance

Asymptomatic renal colonization of leptospires in a region of high disease
transmission is common, including among people without serological or clinical
evidence of recent infection. Both pathogenic and intermediate Leptospira can
persist as renal colonization in humans. The pathogenic significance of this
finding remains to be explored but is of fundamental biological significance.

Author Summary

Leptospirosis is a bacterial disease commonly transmitted from animals to
humans. The more than 200 types of spiral-shaped bacteria (spirochetes) in the
genus Leptospira are classified as pathogenic, intermediately pathogenic, or
saprophytic (meaning not causing infection in any mammal) based on their
ability to cause disease and on genetic information. Unique among the
spirochetes that infect humans, Leptospira live both in the environment (in
surface waters and moist soils), and in mammals, where they cause chronic
infection by colonizing kidney tubules. Infected animals are the source of
human infection, but humans have not been systematically studied as chronic
Leptospira carriers. In our study, we found that more than 5% of people (in
fact, only women) in a rural Amazonian village, without clinical evidence of
infection by Leptospira, were chronically colonized by the bacteria. Chronic
infection was not associated with a detectable immune response against the
spirochete. Pathogenic and intermediately pathogenic Leptospira caused
asymptomatic, chronic kidney infections. Future work is needed to determine
wh</field></doc>
</add>
