<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000115</field>
<field name="doi">10.1371/journal.pntd.0000115</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Anupam Jhingran, Prasad K. Padmanabhan, Sushma Singh, Krishanpal Anamika, Abhijeet A. Bakre, Sudha Bhattacharya, Alok Bhattacharya, Narayanaswamy Srinivasan, Rentala Madhubala</field>
<field name="title">Characterization of the Entamoeba histolytica Ornithine Decarboxylase-Like Enzyme</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">1</field>
<field name="pages">e115</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The polyamines putrescine, spermidine, and spermine are organic cations that
are required for cell growth and differentiation. Ornithine decarboxylase
(ODC), the first and rate-limiting enzyme in the polyamine biosynthetic
pathway, is a highly regulated enzyme.

Methodology and Results

To use this enzyme as a potential drug target, the gene encoding putative
ornithine decarboxylase (ODC)-like sequence was cloned from Entamoeba
histolytica, a protozoan parasite causing amoebiasis. DNA sequence analysis
revealed an open reading frame (ORF) of ∼1,242 bp encoding a putative protein
of 413 amino acids with a calculated molecular mass of 46 kDa and a predicted
isoelectric point of 5.61. The E. histolytica putative ODC-like sequence has
33% sequence identity with human ODC and 36% identity with the Datura
stramonium ODC. The ORF is a single-copy gene located on a 1.9-Mb chromosome.
The recombinant putative ODC protein (48 kDa) from E. histolytica was
heterologously expressed in Escherichia coli. Antiserum against recombinant
putative ODC protein detected a band of anticipated size ∼46 kDa in E.
histolytica whole-cell lysate. Difluoromethylornithine (DFMO), an enzyme-
activated irreversible inhibitor of ODC, had no effect on the recombinant
putative ODC from E. histolytica. Comparative modeling of the three-
dimensional structure of E. histolytica putative ODC shows that the putative
binding site for DFMO is disrupted by the substitution of three amino
acids—aspartate-332, aspartate-361, and tyrosine-323—by histidine-296,
phenylalanine-305, and asparagine-334, through which this inhibitor interacts
with the protein. Amino acid changes in the pocket of the E. histolytica
enzyme resulted in low substrate specificity for ornithine. It is possible
that the enzyme has evolved a novel substrate specificity.

Conclusion

To our knowledge this is the first report on the molecular characterization of
putative ODC-like sequence from E. histolytica. Computer modeling revealed
that three of the critical residues required for binding of DFMO to the ODC
enzyme are substituted in E. histolytica, resulting in the likely loss of
interactions between the enzyme and DFMO.

Author Summary

Entamoeba histolytica is a unicellular protozoan parasite that infects about
50 million people each year and can cause potentially life-threatening
diseases such as hemorrhagic colitis and extraintestinal abscesses. The
infections are primarily treated by antiamoebic therapy. Drugs of choice for
invasive amoebiasis are tissue-active agents, such as metronidazole,
tinidazole, and chloroquine. Although drug resistance to E. histolytica does
not appear to be a serious problem, there are occasional reports of failure
with metronidazole, suggesting that clinical drug resistance may be
developing. When identifying a drug target, it is important that the putative
target be absent in the host, or, if it is present in the host, that the
homologue in the parasite be substantially different from the host homologue
so that it can be exploited as a drug target. Such is the case with the
enzymes involved in polyamine biosynthesis, a pathway that has been exploited
as a target to control disease caused by several </field></doc>
</add>
