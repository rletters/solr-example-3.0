<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001521</field>
<field name="doi">10.1371/journal.pntd.0001521</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Rachana Bhattarai, Christine M. Budke, Hélène Carabin, Jefferson V. Proaño, Jose Flores-Rivera, Teresa Corona, Renata Ivanek, Karen F. Snowden, Ana Flisser</field>
<field name="title">Estimating the Non-Monetary Burden of Neurocysticercosis in Mexico</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">2</field>
<field name="pages">e1521</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Neurocysticercosis (NCC) is a major public health problem in many developing
countries where health education, sanitation, and meat inspection
infrastructure are insufficient. The condition occurs when humans ingest eggs
of the pork tapeworm Taenia solium, which then develop into larvae in the
central nervous system. Although NCC is endemic in many areas of the world and
is associated with considerable socio-economic losses, the burden of NCC
remains largely unknown. This study provides the first estimate of disability
adjusted life years (DALYs) associated with NCC in Mexico.

Methods

DALYs lost for symptomatic cases of NCC in Mexico were estimated by
incorporating morbidity and mortality due to NCC-associated epilepsy, and
morbidity due to NCC-associated severe chronic headaches. Latin hypercube
sampling methods were employed to sample the distributions of uncertain
parameters and to estimate 95% credible regions (95% CRs).

Findings

In Mexico, 144,433 and 98,520 individuals are estimated to suffer from NCC-
associated epilepsy and NCC-associated severe chronic headaches, respectively.
A total of 25,341 (95% CR: 12,569–46,640) DALYs were estimated to be lost due
to these clinical manifestations, with 0.25 (95% CR: 0.12–0.46) DALY lost per
1,000 person-years of which 90% was due to NCC-associated epilepsy.

Conclusion

This is the first estimate of DALYs associated with NCC in Mexico. However,
this value is likely to be underestimated since only the clinical
manifestations of epilepsy and severe chronic headaches were included. In
addition, due to limited country specific data, some parameters used in the
analysis were based on systematic reviews of the literature or primary
research from other geographic locations. Even with these limitations, our
estimates suggest that healthy years of life are being lost due to NCC in
Mexico.

Author Summary

Neurocysticercosis (NCC) is a major public health problem caused by the larvae
of the parasite Taenia solium. The condition occurs when humans ingest eggs of
the pork tapeworm Taenia solium, which then develop into larvae in the central
nervous system. The disease is predominantly found and considered important in
Latin American, Asian, and African countries and is associated with a large
social and economic burden. Very few studies have been conducted to evaluate
the burden of NCC and there are no estimates from Mexico. We estimated the
disability adjusted life years (DALYs) lost due to NCC in Mexico incorporating
morbidity and mortality due to NCC-associated epilepsy, and morbidity due to
NCC-associated severe chronic headaches. NCC-associated epilepsy and severe
chronic headaches were estimated to cause a loss of approximately 0.25 healthy
year of life per 1,000 persons annually in Mexico. This is the first estimate
of DALYs associated with NCC in Mexico. However, this value is likely to be
underestimated since only the clinical manifestations of epilepsy and severe
chronic headaches were included.

Introduction

Neurocysticercosis (NCC) is a major public health problem caused by the larvae
of the zoonotic cestode Taenia solium. Humans are the definitive hosts of T.
solium and become infected with the intestinal adult tapeworm (taeniasis) by
ing</field></doc>
</add>
