<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000219</field>
<field name="doi">10.1371/journal.pntd.0000219</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Ibne Karim M. Ali, Shahram Solaymani-Mohammadi, Jasmine Akhter, Shantanu Roy, Chiara Gorrini, Adriana Calderaro, Sarah K. Parker, Rashidul Haque, William A. Petri Jr., C. Graham Clark</field>
<field name="title">Tissue Invasion by Entamoeba histolytica: Evidence of Genetic Selection and/or DNA Reorganization Events in Organ Tropism</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">4</field>
<field name="pages">e219</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Entamoeba histolytica infection may have various clinical manifestations. Nine
out of ten E. histolytica infections remain asymptomatic, while the remainder
become invasive and cause disease. The most common form of invasive infection
is amebic diarrhea and colitis, whereas the most common extra-intestinal
disease is amebic liver abscess. The underlying reasons for the different
outcomes are unclear, but a recent study has shown that the parasite genotype
is a contributor. To investigate this link further we have examined the
genotypes of E. histolytica in stool- and liver abscess-derived samples from
the same patients. Analysis of all 18 paired samples (16 from Bangladesh, one
from the United States of America, and one from Italy) revealed that the
intestinal and liver abscess amebae are genetically distinct. The results
suggest either that E. histolytica subpopulations in the same infection show
varying organ tropism, or that a DNA reorganization event takes place prior to
or during metastasis from intestine to liver.

Author Summary

The parasite Entamoeba histolytica can cause serious disease by invading the
lining of the large intestine and spreading to other organs of the body.
However, most infected individuals never develop symptoms, and it is not clear
what determines the different outcomes of infection. Factors that might be
having an effect range from the immune response of the infected individual, to
concurrent infections with other organisms or genetic differences among the
parasites. In the present study we investigated the role of the latter by
comparing parasites in the intestine with those that have invaded the liver of
the same patient. In all 18 pairs of samples we could detect genetic
differences among the parasites. Interpreting the findings is difficult, as we
cannot distinguish at present between mutations that have occurred during
tissue invasion and genetic diversity that was already present in the
population of parasites in the intestine. However, our results strongly
support a role for the parasite in determining the outcome of infection with
E. histolytica.

Introduction

Entamoeba histolytica, the causative agent of human amebiasis, is endemic in
many tropical countries. It is well established that E. histolytica infections
result in variable clinical outcomes. Most infections remain asymptomatic,
some develop diarrhea and dysentery, and only a few develop extra-intestinal
complications such as liver abscess. The parasite is estimated to be
responsible for millions of cases of dysentery and liver abscess world-wide
annually, resulting in up to 100,000 deaths [1],[2] Additionally, E.
histolytica-associated dysentery is associated with malnutrition and lower
cognitive test scores in infected children in Bangladesh, and thus may impair
their proper development [3],[4].

What determines the outcome of an E. histolytica infection remains largely
unknown although host genetics and parasite genotype likely play important
roles [5],[6]. We recently investigated the association between the genotypes
of parasites and the clinical outcome of infection (6) using Bangladeshi
clinical samples and a 6-locus genotypin</field></doc>
</add>
