<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001676</field>
<field name="doi">10.1371/journal.pntd.0001676</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Linda K. Lee, Victor C. Gan, Vernon J. Lee, Adriana S. Tan, Yee Sin Leo, David C. Lye</field>
<field name="title">Clinical Relevance and Discriminatory Value of Elevated Liver Aminotransferase Levels for Dengue Severity</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">6</field>
<field name="pages">e1676</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Elevation of aspartate aminotransferase (AST) and alanine aminotransferase
(ALT) is prominent in acute dengue illness. The World Health Organization
(WHO) 2009 dengue guidelines defined AST or ALT≥1000 units/liter (U/L) as a
criterion for severe dengue. We aimed to assess the clinical relevance and
discriminatory value of AST or ALT for dengue hemorrhagic fever (DHF) and
severe dengue.

Methodology/Principal Findings

We retrospectively studied and classified polymerase chain reaction positive
dengue patients from 2006 to 2008 treated at Tan Tock Seng Hospital, Singapore
according to WHO 1997 and 2009 criteria for dengue severity. Of 690 dengue
patients, 31% had DHF and 24% severe dengue. Elevated AST and ALT occurred in
86% and 46%, respectively. Seven had AST or ALT≥1000 U/L. None had acute liver
failure but one patient died. Median AST and ALT values were significantly
higher with increasing dengue severity by both WHO 1997 and 2009 criteria.
However, they were poorly discriminatory between non-severe and severe dengue
(e.g., AST area under the receiver operating characteristic [ROC] curve =
0.62; 95% confidence interval [CI]: 0.57–0.67) and between dengue fever (DF)
and DHF (AST area under the ROC curve = 0.56; 95% CI: 0.52–0.61). There was
significant overlap in AST and ALT values among patients with dengue with or
without warning signs and severe dengue, and between those with DF and DHF.

Conclusions

Although aminotransferase levels increased in conjunction with dengue
severity, AST or ALT values did not discriminate between DF and DHF or non-
severe and severe dengue.

Author Summary

Dengue is a global public health problem, as the incidence of the disease has
reached hyperendemic proportions in recent decades. Infection with dengue can
cause acute, febrile illness or severe disease, which can lead to plasma
leakage, bleeding, and organ impairment. One of the most prominent clinical
characteristics of dengue patients is increased aspartate and alanine
aminotransferase liver enzyme levels. The significance of this is uncertain,
as it is transient in the majority of cases, and most patients recover
uneventfully without liver damage. In this study, we characterized this
phenomenon in the context of dengue severity and found that, although liver
enzyme levels increased concurrently with dengue severity, they could not
sufficiently discriminate between dengue fever and dengue hemorrhagic fever or
between non-severe and severe dengue. Therefore clinicians may need to use
other parameters to distinguish dengue severity in patients during early
illness.

Introduction

Dengue is a mosquito-borne arboviral infection endemic to most tropical and
subtropical countries [1]. Elevation of the liver enzymes aspartate
aminotransferase (AST) and alanine aminotransferase (ALT) is common in acute
dengue illness, occurring in 65–97% [2], [3], [4], [5] of dengue patients,
peaking during the convalescent period of illness (days 7–10) [2], [4], [6].
In dengue-endemic countries, dengue is an important cause of acute viral
hepatitis [7].

Elevated AST and ALT levels have been associated with bleeding [2], [4], [6]
and dengue hemorrhagic fever (DHF) [3], [8]. Liver failure has been recognized
as a complication and unusual manife</field></doc>
</add>
