<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001545</field>
<field name="doi">10.1371/journal.pntd.0001545</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Piero Olliaro, Michel T. Vaillant, Shyam Sundar, Manica Balasegaram</field>
<field name="title">More Efficient Ways of Assessing Treatments for Neglected Tropical Diseases Are Required: Innovative Study Designs, New Endpoints, and Markers of Effects</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">5</field>
<field name="pages">e1545</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
The Issue: Getting an Answer Sooner and Cheaper

Shortening trial time means reaching a decision earlier as to whether a
treatment is effective—and saving money in the process. With old, invasive,
inefficient tests of cure like those we have now for several neglected
tropical diseases, follow-up (and total trial) times remain inefficiently and
uneconomically long. While the need to licence new drugs is urgent for many of
the neglected tropical diseases, it frequently takes 8–10 years from Phase 1
to licensure, and sometimes even longer. Consider visceral leishmaniasis: it
has taken nearly 20 years and at least three different organisations for
paromomycin to find its way to registration in India, and even longer to take
a final decision to terminate the development of sitamaquine.

The need for efficiency is particularly acute in drug development, and
specifically in Phase 2 clinical trials, when one will select the drug
dose/schedule to be tested at a larger scale in the Phase 3 (pivotal) trials.
Here, one wants to find out what works and what doesn&apos;t as quickly and
economically as possible. Transposing results from non-clinical studies (in
vitro and in vivo experiments) in terms of pharmacokinetic/dynamic correlation
is not easy, so one is often left with a variety of potential doses and
regimens to choose from.

What Would Alternative Study Designs Add?

Adaptive trials designs are increasingly used by pharmaceutical companies to
improve efficiencies in the R&amp;D process [1]. This approach allows the
possibility to redesign the trial (sample size, number of arms, etc.) based on
the information acquired through interim analyses. Sequential and group
sequential [1] trials are a special case of adaptive trials whereby several
interim analyses are done in order to complete the trial earlier (interrupt
enrolment) based on the accumulated information.

However, these methods work best for diseases for which treatment response
becomes obvious shortly after treatment rather than having to wait for 6
months (visceral leishmaniasis), 18 months (onchocerciasis [river blindness];
human African trypanosomiasis [HAT; sleeping sickness]), or a yet-to-be-
defined period for chronic Chagas disease. Tuberculosis is in the same league
(18 months from treatment start for the initial assessment and another 12
months for final cure), while with “only” 28–63 days of follow-up, malaria is
comparatively much better in this sense. The reason for such long follow-up
times is that patients who initially respond favourably may relapse later, and
such cases cannot yet be predicted by the current tests of cure.

There are several ways to specify early termination procedures (for futility),
allow repeated analyses to be performed on accumulated data, maintain pre-
specified α and β error, or stop the trial as soon as the information is
sufficient to reach a conclusion [2]. These methods can be grouped as: (i)
sequential methods (sequential probability ratio test and triangular test [2],
[3]) and (ii) group sequential designs (Peto [4], Pocock [5], and O&apos;Brien-
Fleming [6] methods; α [7], [8] and β [9] spending function; etc.). This is a
domain of ongoing statistical research with existing methods being improved
and new ones </field></doc>
</add>
