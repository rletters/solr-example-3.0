<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000379</field>
<field name="doi">10.1371/journal.pntd.0000379</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Peter J. Hotez, Gavin Yamey</field>
<field name="title">The Evolving Scope of PLoS Neglected Tropical Diseases</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">2</field>
<field name="pages">e379</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
PLoS Neglected Tropical Diseases is an open-access community journal that
serves the needs of a small but active and robust community of neglected
tropical disease (NTD) scientists, clinicians, and public health and policy
experts. As stewards of that community, our editorial staff has very much
molded the journal according to what we have learned from you in terms of
mission, priorities, and scope. In the eighteen months since the launch of
PLoS Neglected Tropical Diseases, some of our most interesting editorial
discussions and queries from authors are in regards to the journal&apos;s scope,
particularly the specific conditions defined as NTDs
(http://www.plosntds.org/static/scope.action).

What has emerged from these discussions is a journal focused on a fairly
specific group of important infections endemic in developing countries with
seven common features [1],[2]: (1) the NTDs occur in the setting of poverty
where they are the most prevalent infections afflicting “the bottom billion,”
i.e., people living on less than US$1 per day [3]; (2) they are chronic
conditions—people can harbor their NTDs for years or decades; (3) they
generally disable, rather than kill; (4) they are frequently disfiguring and
stigmatizing [4]; (5) they are not typically emerging in character; instead,
the NTDs have plagued humankind for centuries and many are described in
ancient texts [5]; (6) they are not just diseases of poverty, but they also
promote poverty because of their effects on child development, cognition, and
education, adult agricultural worker productivity, and pregnancy outcome [3];
and (7) the NTDs are a critical component of the “other diseases” mentioned in
the sixth Millennium Development Goal (“Combat HIV/AIDS, malaria, and other
diseases”) [1],[2],[6].

Helminths

There are certain diseases that easily meet these criteria. A good example are
the helminth infections, especially the most common ones, such as hookworm
infection, ascariasis, trichuriasis, strongyloidiasis, schistosomiasis, and
lymphatic filariasis—each affecting more than 100 million people in sub-
Saharan Africa or tropical regions of Asia and the Americas. We have included
many of the less common helminthiases in our scope, including food-borne
trematode infections, onchocerciasis, loiasis and other filarial infections,
cysticercosis and other cestodiases, and other intestinal nematode infections.
We also include helminth infections that are not exclusively tropical, such as
enterobiasis, toxocariasis, and trichinellosis. In North America (and
elsewhere in temperate regions) helminth infections such as ascariasis and
strongyloidiasis are still considered diseases that primarily affect the poor,
and they have been designated by some as “neglected infections of poverty”
[7],[8]. In summary, we would consider almost any human helminth infections as
an NTD and thus within the scope of the journal.

Protozoa

Similarly, most of the protozoan infections, including Chagas disease,
leishmaniasis, human African trypanosomiasis, and many of the intestinal
protozoan infections, are also considered NTDs. Conspicuous by its absence on
the protozoan infection list is malaria. Certainly, no one would question the
devastating global health impact of this disease, nor its predilection to
affect the poor. Moreover, as with the NTDs, several investigators, including
Jeffrey Sachs and others, </field></doc>
</add>
