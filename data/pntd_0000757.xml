<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000757</field>
<field name="doi">10.1371/journal.pntd.0000757</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Vu Thi Ty Hang, Edward C. Holmes, Duong Veasna, Nguyen Thien Quy, Tran Tinh Hien, Michael Quail, Carol Churcher, Julian Parkhill, Jane Cardosa, Jeremy Farrar, Bridget Wills, Niall J. Lennon, Bruce W. Birren, Philippe Buchy, Matthew R. Henn, Cameron P. Simmons</field>
<field name="title">Emergence of the Asian 1 Genotype of Dengue Virus Serotype 2 in Viet Nam: In Vivo Fitness Advantage and Lineage Replacement in South-East Asia</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">7</field>
<field name="pages">e757</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

A better description of the extent and structure of genetic diversity in
dengue virus (DENV) in endemic settings is central to its eventual control. To
this end we determined the complete coding region sequence of 187 DENV-2
genomes and 68 E genes from viruses sampled from Vietnamese patients between
1995 and 2009. Strikingly, an episode of genotype replacement was observed,
with Asian 1 lineage viruses entirely displacing the previously dominant
Asian/American lineage viruses. This genotype replacement event also seems to
have occurred within DENV-2 in Thailand and Cambodia, suggestive of a major
difference in viral fitness. To determine the cause of this major evolutionary
event we compared both the infectivity of the Asian 1 and Asian/American
genotypes in mosquitoes and their viraemia levels in humans. Although there
was little difference in infectivity in mosquitoes, we observed significantly
higher plasma viraemia levels in paediatric patients infected with Asian 1
lineage viruses relative to Asian/American viruses, a phenotype that is
predicted to result in a higher probability of human-to-mosquito transmission.
These results provide a mechanistic basis to a marked change in the genetic
structure of DENV-2 and more broadly underscore that an understanding of DENV
evolutionary dynamics can inform the development of vaccines and anti-viral
drugs.

Author Summary

Dengue virus (DENV) is a mosquito transmitted RNA virus. One of the most
characteristic patterns of DENV evolution is that viral lineages, including
whole genotypes, are born and die-out on a regular basis. However, the precise
cause of such lineage turnover is unclear. To address this issue we explored
the genome-scale genetic diversity and evolution of dengue serotype 2 virus
(DENV-2) in Viet Nam, a country with a high burden of dengue disease. We
observed that DENV viruses assigned to the Asian 1 lineage were likely
introduced into southern Viet Nam in the late 1990&apos;s (perhaps from Thailand)
and subsequently displaced the resident Asian/American lineage serotype 2
viruses. A similar scenario seems to have occurred in Thailand and Cambodia,
such that there appears to have been a region-wide proliferation of Asian 1
lineage viruses. Investigation of Vietnamese patients experiencing DENV-2
infection revealed that Asian 1 viruses also attain higher virus levels in the
blood than viruses of the Asian/American lineage. This difference in virus
titre is likely to have a profound impact on viral fitness by increasing the
probability of mosquito transmission, and therein providing Asian 1 lineage
viruses with a selective advantage.

Introduction

Dengue viruses (DENV) are vector-borne RNA viruses of the family Flaviviridae
and are classified as either four distinct viruses or different serotypes
(DENV-1 to DENV-4), each of which can infect humans. Infection with DENV can
cause a spectrum of outcomes, ranging from asymptomatic infection through to
clinically significant disease. Severe disease in children is commonly
characterised by increased vascular permeability, thrombo</field></doc>
</add>
