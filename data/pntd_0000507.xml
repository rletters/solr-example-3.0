<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000507</field>
<field name="doi">10.1371/journal.pntd.0000507</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Takele Lakew, Wondu Alemayehu, Muluken Melese, Elizabeth Yi, Jenafir I. House, Kevin C. Hong, Zhaoxia Zhou, Kathryn J. Ray, Travis C. Porco, Bruce D. Gaynor, Thomas M. Lietman, Jeremy D. Keenan</field>
<field name="title">Importance of Coverage and Endemicity on the Return of Infectious Trachoma after a Single Mass Antibiotic Distribution</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">8</field>
<field name="pages">e507</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

As part of the SAFE strategy, mass antibiotic treatments are useful in
controlling the ocular strains of chlamydia that cause trachoma. The World
Health Organization recommends treating at least 80% of individuals per
community. However, the role of antibiotic coverage for trachoma control has
been poorly characterized.

Methodology/Principal Findings

In a collection of cluster-randomized clinical trials, mass oral azithromycin
was administered to 40 villages in Ethiopia. The village prevalence of ocular
chlamydia was determined before treatment, and at two and six months post-
treatment. The mean prevalence of ocular chlamydia was 48.9% (95% CI 42.8 to
55.0%) before mass treatments, decreased to 5.4% (95% CI 3.9 to 7.0%) at two
months after treatments (p&lt;0.0001), and returned to 7.9% (95% CI 5.4 to 10.4%)
by six months after treatment (p = 0.03). Antibiotic coverage ranged from
73.9% to 100%, with a mean of 90.6%. In multivariate regression models,
chlamydial prevalence two months after treatment was associated with baseline
infection (p&lt;0.0001) and antibiotic coverage (p = 0.007). However, by six
months after treatment, chlamydial prevalence was associated only with
baseline infection (p&lt;0.0001), but not coverage (p = 0.31).

Conclusions/Significance

In post-hoc analyses of a large clinical trial, the amount of endemic
chlamydial infection was a strong predictor of chlamydial infection after mass
antibiotic treatments. Antibiotic coverage was an important short-term
predictor of chlamydial infection, but no longer predicted infection by six
months after mass antibiotic treatments. A wider range of antibiotic coverage
than found in this study might allow an assessment of a more subtle
association.

Author Summary

Trachoma, caused by ocular chlamydia infection, is the most common infectious
cause of blindness in the world. The World Health Organization (WHO)
recommends the SAFE strategy (eyelid surgery, antibiotics, facial hygiene,
environmental improvements) for trachoma control. Oral antibiotics reduce the
transmission of ocular chlamydia, but re-infection of treated individuals is
common. Therefore, the WHO recommends annual mass antibiotic treatments to the
entire village. The success of treatment is likely based on many factors,
including the antibiotic coverage, or percentage of villagers who receive
antibiotics. However, no studies have analyzed the importance of antibiotic
coverage for the reduction of ocular chlamydia. Here, we performed
multivariate regression analyses on data from a clinical trial of mass oral
antibiotics for trachoma in a severely affected area of Ethiopia. At the
relatively high levels of antibiotic coverage in our study, coverage was
associated with post-treatment infection at two months, but not at six months.
The amount of infection at baseline was strongly correlated with post-
treatment infection at both two and six months. These results suggest that in
areas with severe trachoma treated with relatively high antibiotic coverage,
increasing coverage even further may have only a short-term benefit.

Introduction

The World Health Organization (WHO) recomme</field></doc>
</add>
