<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000128</field>
<field name="doi">10.1371/journal.pntd.0000128</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Myrtle Perera, Margaret Whitehead, David Molyneux, Mirani Weerasooriya, Godfrey Gunatilleke</field>
<field name="title">Neglected Patients with a Neglected Disease? A Qualitative Study of Lymphatic Filariasis</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2007</field>
<field name="volume">1</field>
<field name="number">2</field>
<field name="pages">e128</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Lymphatic filariasis (LF) is a so-called neglected tropical disease, currently
overshadowed by higher-profile efforts to address malaria, tuberculosis, and
HIV/AIDS. Despite recent successes in arresting transmission, some 40 million
people who already have the disease have been largely neglected. This study
aims to increase understanding of how this vulnerable, neglected group can be
helped.

Methods

We used purposive sampling to select 60 men and women with filarial
lymphoedema (45 with filarial elephantiasis and 15 men with filarial
hydrocoele) from the south of Sri Lanka in 2004–2005. Participants were
selected to give a balance of men and women and poor and nonpoor, and a range
of stages of the disease. Participants&apos; experiences and the consequences of
their disease for the household were explored with in-depth qualitative,
semistructured interviews.

Findings

LF was extremely debilitating to participants over long periods of time. The
stigma attached to the condition caused social isolation and emotional
distress, and delayed diagnosis and treatment, resulting in undue advancement
of the disease. Free treatment services at government clinics were avoided
because the participants&apos; condition would be identifiable in public. Loss of
income due to the condition was reported by all households in the sample, not
just the poorest. Households that were already on low incomes were pushed into
near destitution, from which it was almost impossible to escape. Affected
members of low-income households also had less opportunity to obtain
appropriate treatment from distant clinics, and had living and working
conditions that made hygiene and compliance difficult.

Significance

This highly vulnerable category of patients has low visibility, thus becoming
marginalized and forgotten. With an estimated 300,000 total cases of
elephantiasis and/or oedema in Sri Lanka, and around 300,000 men with filarial
hydrocoele, the affected households will need help and support for many years
to come. These individuals should be specially targeted for identification,
outreach, and care. The global strategy for elimination is aimed at the
cessation of transmission, but there will remain some 40 million individuals
with clinical manifestations whose needs and problems are illustrated in this
study.

Author Summary

Lymphatic filariasis (LF) is a tropical disease causing extreme swelling of
the limbs and male genitals. Despite recent successes in preventing
transmission of the disease, some 40 million people worldwide who already have
the disease have been largely neglected. We aimed to increase understanding of
how this vulnerable, neglected group can be helped, by asking people with LF
in Sri Lanka to recount their own experiences.

Study participants reported that LF was extremely debilitating over a long
period of time. The social isolation from stigma caused emotional distress and
delayed diagnosis and treatment. Free treatment services at government clinics
were avoided because the participants&apos; condition would be identifiable in
public. Loss of income due to the condition was reported by all households,
not only those of the poorest. Households that were already on low incomes
were pushed into near destitution by LF. Low-income households also h</field></doc>
</add>
