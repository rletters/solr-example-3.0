<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000481</field>
<field name="doi">10.1371/journal.pntd.0000481</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Steven T. Stoddard, Amy C. Morrison, Gonzalo M. Vazquez-Prokopec, Valerie Paz Soldan, Tadeusz J. Kochel, Uriel Kitron, John P. Elder, Thomas W. Scott</field>
<field name="title">The Role of Human Movement in the Transmission of Vector-Borne Pathogens</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">7</field>
<field name="pages">e481</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Human movement is a key behavioral factor in many vector-borne disease systems
because it influences exposure to vectors and thus the transmission of
pathogens. Human movement transcends spatial and temporal scales with
different influences on disease dynamics. Here we develop a conceptual model
to evaluate the importance of variation in exposure due to individual human
movements for pathogen transmission, focusing on mosquito-borne dengue virus.

Methodology and Principal Findings

We develop a model showing that the relevance of human movement at a
particular scale depends on vector behavior. Focusing on the day-biting Aedes
aegypti, we illustrate how vector biting behavior combined with fine-scale
movements of individual humans engaged in their regular daily routine can
influence transmission. Using a simple example, we estimate a transmission
rate (R0) of 1.3 when exposure is assumed to occur only in the home versus
3.75 when exposure at multiple locations—e.g., market, friend&apos;s—due to
movement is considered. Movement also influences for which sites and
individuals risk is greatest. For the example considered, intriguingly, our
model predicts little correspondence between vector abundance in a site and
estimated R0 for that site when movement is considered. This illustrates the
importance of human movement for understanding and predicting the dynamics of
a disease like dengue. To encourage investigation of human movement and
disease, we review methods currently available to study human movement and,
based on our experience studying dengue in Peru, discuss several important
questions to address when designing a study.

Conclusions/Significance

Human movement is a critical, understudied behavioral component underlying the
transmission dynamics of many vector-borne pathogens. Understanding movement
will facilitate identification of key individuals and sites in the
transmission of pathogens such as dengue, which then may provide targets for
surveillance, intervention, and improved disease prevention.

Author Summary

Vector-borne diseases constitute a largely neglected and enormous burden on
public health in many resource-challenged environments, demanding efficient
control strategies that could be developed through improved understanding of
pathogen transmission. Human movement—which determines exposure to vectors—is
a key behavioral component of vector-borne disease epidemiology that is poorly
understood. We develop a conceptual framework to organize past studies by the
scale of movement and then examine movements at fine-scale—i.e., people going
through their regular, daily routine—that determine exposure to insect vectors
for their role in the dynamics of pathogen transmission. We develop a model to
quantify risk of vector contact across locations people visit, with emphasis
on mosquito-borne dengue virus in the Amazonian city of Iquitos, Peru. An
example scenario illustrates how movement generates variation in exposure risk
across individuals, how transmission rates within sites can be increased, and
that risk within sites is not solely determined by vector density, as is
commonly assumed. Our analysis illustrates the importance of human movement
for pathogen transmission, yet </field></doc>
</add>
