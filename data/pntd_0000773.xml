<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000773</field>
<field name="doi">10.1371/journal.pntd.0000773</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Sheila B. Ogoma, Dickson W. Lweitoijera, Hassan Ngonyani, Benjamin Furer, Tanya L. Russell, Wolfgang R. Mukabana, Gerry F. Killeen, Sarah J. Moore</field>
<field name="title">Screening Mosquito House Entry Points as a Potential Method for Integrated Control of Endophagic Filariasis, Arbovirus and Malaria Vectors</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">8</field>
<field name="pages">e773</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Partial mosquito-proofing of houses with screens and ceilings has the
potential to reduce indoor densities of malaria mosquitoes. We wish to measure
whether it will also reduce indoor densities of vectors of neglected tropical
diseases.

Methodology

The main house entry points preferred by anopheline and culicine vectors were
determined through controlled experiments using specially designed
experimental huts and village houses in Lupiro village, southern Tanzania. The
benefit of screening different entry points (eaves, windows and doors) using
PVC-coated fibre glass netting material in terms of reduced indoor densities
of mosquitoes was evaluated compared to the control.

Findings

23,027 mosquitoes were caught with CDC light traps; 77.9% (17,929) were
Anopheles gambiae sensu lato, of which 66.2% were An. arabiensis and 33.8% An.
gambiae sensu stricto. The remainder comprised 0.2% (50) An. funestus, 10.2%
(2359) Culex spp. and 11.6% (2664) Mansonia spp. Screening eaves reduced
densities of Anopheles gambiae s. l. (Relative ratio (RR)  = 0.91; 95% CI =
0.84, 0.98; P = 0.01); Mansonia africana (RR = 0.43; 95% CI = 0.26, 0.76;
P&lt;0.001) and Mansonia uniformis (RR = 0.37; 95% CI = 0.25, 0.56; P&lt;0.001) but
not Culex quinquefasciatus, Cx. univittatus or Cx. theileri. Numbers of these
species were reduced by screening windows and doors but this was not
significant.

Significance

This study confirms that across Africa, screening eaves protects households
against important mosquito vectors of filariasis, Rift Valley Fever and
O&apos;Nyong nyong as well as malaria. While full house screening is required to
exclude Culex species mosquitoes, screening of eaves alone or fitting ceilings
has considerable potential for integrated control of other vectors of
filariasis, arbovirus and malaria.

Author Summary

Mosquito vectors that transmit filariasis and several arboviruses such as Rift
Valley Fever, Chikungunya and O&apos;Nyong nyong as well as malaria co-occur across
tropical Africa. These diseases are co-endemic in most rural African countries
where they are transmitted by the same mosquito vectors. The only control
measure currently in widespread use is mass drug administration for
filariasis. In this study, we used controlled experiments to evaluate the
benefit of screening the main mosquito entry points into houses, namely,
eaves, windows and doors. This study aims to illustrate the potential of
screening specific house openings with the intention of preventing endophagic
mosquitoes from entering houses and thus reducing contact between humans and
vectors of neglected tropical diseases. This study confirms that while full
house screening is effective for reducing indoor densities of Culex spp.
mosquitoes, screening of eaves alone has a great potential for integrated
control of neglected tropical diseases and malaria.

Introduction

Houses are the main site for contact between humans and night biting mosquito
vectors [1], [2]. The impact of improved housing on indoor malaria vector
densities [3]–[6] and transmission [7] is well established. In Africa, the
primary malaria vectors are nocturnal, endophilic and endop</field></doc>
</add>
