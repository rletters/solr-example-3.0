<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001248</field>
<field name="doi">10.1371/journal.pntd.0001248</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Gemma L. Chaloner, Palmira Ventosilla, Richard J. Birtles</field>
<field name="title">Multi-Locus Sequence Analysis Reveals Profound Genetic Diversity among Isolates of the Human Pathogen Bartonella bacilliformis</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">7</field>
<field name="pages">e1248</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Bartonella bacilliformis is the aetiological agent of human bartonellosis, a
potentially life threatening infection of significant public health concern in
the Andean region of South America. Human bartonellosis has long been
recognised in the region but a recent upsurge in the number of cases of the
disease and an apparent expansion of its geographical distribution have re-
emphasized its contemporary medical importance. Here, we describe the
development of a multi-locus sequence typing (MLST) scheme for B.
bacilliformis and its application to an archive of 43 isolates collected from
patients across Peru. MLST identified eight sequence types among these
isolates and the delineation of these was generally congruent with those of
the previously described typing scheme. Phylogenetic analysis based on
concatenated sequence data derived from MLST loci revealed that seven of the
eight sequence types were closely related to one another; however, one
sequence type, ST8, exhibited profound evolutionary divergence from the
others. The extent of this divergence was akin to that observed between other
members of the Bartonella genus, suggesting that ST8 strains may be better
considered as members of a novel Bartonella genospecies.

Author Summary

Bartonellosis (Carrion&apos;s disease) is caused by the bacterium Bartonella
bacilliformis and is encountered across the Andean cordillera, particularly in
Peru. B. bacilliformis is transmitted by arthropods, probably sandflies, and
exploits humans as a reservoir host, establishing chronic infections
characterized by intra-erythrocytic bacteremia. It this thought that
asymptomatic infections are common among the residents of bartonellosis-
endemic regions. However, several thousand cases of B. bacilliformis-induced
severe haemolytic anaemia are reported each year, frequently among visitors to
endemic regions but also among inhabitants of an increasing number of new foci
of disease. To better understand the epidemiology of bartonellosis, a clearer
understanding of how B. bacilliformis strains circulating in endemic regions
and in new foci are related to one another is required. Here, we describe the
development and application of a new genotyping tool for addressing this
shortfall, namely multilocus sequence typing (MLST). We applied MLST to 43 B.
bacilliformis isolates of varying provenance, delineating eight distinct
genotypes, some of which had obvious epidemiological correlates. Surprisingly,
one genotype exhibited profound divergence from the other seven, and the
extent of this divergence suggested that some bartonellosis may be caused by a
Bartonella species specifically related to, yet distinct from, B.
bacilliformis.

Introduction

Bartonellosis, or Carrion&apos;s disease, caused by the bacterium Bartonella
bacilliformis, has long been recognised in the Andean region of South America,
particularly in the high valleys lining the western side of the cordillera in
central Peru [1]. Bartonellosis may present in two markedly different clinical
manifestations. Firstly, Oroya fever is characterised by fever, headache,
pallor and myalgia, which progresses to a severe haemolytic anaemia. Mortality
rates as high as 88% have been described in untreated patients with this
manifestation. Alternatively, infection may provo</field></doc>
</add>
