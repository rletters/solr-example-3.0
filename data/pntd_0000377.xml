<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000377</field>
<field name="doi">10.1371/journal.pntd.0000377</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Louise Ford, Jun Zhang, Jing Liu, Sarwar Hashmi, Juliet A. Fuhrman, Yelena Oksov, Sara Lustigman</field>
<field name="title">Functional Analysis of the Cathepsin-Like Cysteine Protease Genes in Adult Brugia malayi Using RNA Interference</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">2</field>
<field name="pages">e377</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Cathepsin-like enzymes have been identified as potential targets for drug or
vaccine development in many parasites, as their functions appear to be
essential in a variety of important biological processes within the host, such
as molting, cuticle remodeling, embryogenesis, feeding and immune evasion.
Functional analysis of Caenorhabditis elegans cathepsin L (Ce-cpl-1) and
cathepsin Z (Ce-cpz-1) has established that both genes are required for early
embryogenesis, with Ce-cpl-1 having a role in regulating in part the
processing of yolk proteins. Ce-cpz-1 also has an important role during
molting.

Methods and Findings

RNA interference assays have allowed us to verify whether the functions of the
orthologous filarial genes in Brugia malayi adult female worms are similar.
Treatment of B. malayi adult female worms with Bm-cpl-1, Bm-cpl-5, which
belong to group Ia of the filarial cpl gene family, or Bm-cpz-1 dsRNA resulted
in decreased numbers of secreted microfilariae in vitro. In addition, analysis
of the intrauterine progeny of the Bm-cpl-5 or Bm-cpl Pro dsRNA- and siRNA-
treated worms revealed a clear disruption in the process of embryogenesis
resulting in structural abnormalities in embryos and a varied differential
development of embryonic stages.

Conclusions

Our studies suggest that these filarial cathepsin-like cysteine proteases are
likely to be functional orthologs of the C. elegans genes. This functional
conservation may thus allow for a more thorough investigation of their
distinct functions and their development as potential drug targets.

Author Summary

Filarial nematodes are an important group of human pathogens, causing
lymphatic filariasis and onchocerciasis, and infecting around 150 million
people throughout the tropics with more than 1.5 billion at risk of infection.
Control of filariasis currently relies on mass drug administration (MDA)
programs using drugs which principally target the microfilarial life-cycle
stage. These control programs are facing major challenges, including the
absence of a drug with macrofilaricidal or permanent sterilizing activity, and
the possibility of the development of drug-resistance against the drugs
available. Cysteine proteases are essential enzymes which play important roles
in a wide range of cellular processes, and the cathepsin-like cysteine
proteases have been identified as potential targets for drug or vaccine
development in many parasites. Here we have studied the function of several of
the cathepsin-like enzymes in the filarial nematode, B. malayi, and
demonstrate that these cysteine proteases are involved in the development of
embryos, show similar functions to their counterparts in C. elegans, and
therefore, provide an important target for future drug development targeted to
eliminate filariasis.

Introduction

Human lymphatic filariasis (LF), caused by the filarial parasites Brugia
malayi, Brugia timori and Wuchereria bancrofti, infects 120 million people
worldwide, of which 40 million people show chronic disease symptoms
(www.globalnetwork.org) [1]. The disease is estimated to be responsible for
5.5 million DALYs, and is the second leading cause of permanent and long-term
disability worldwide [2]. A further one billion people (18% of the world&apos;s
</field></doc>
</add>
