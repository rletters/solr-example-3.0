<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000804</field>
<field name="doi">10.1371/journal.pntd.0000804</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Gregory J. Crowther, Dhanasekaran Shanmugam, Santiago J. Carmona, Maria A. Doyle, Christiane Hertz-Fowler, Matthew Berriman, Solomon Nwaka, Stuart A. Ralph, David S. Roos, Wesley C. Van Voorhis, Fernán Agüero</field>
<field name="title">Identification of Attractive Drug Targets in Neglected-Disease Pathogens Using an In Silico Approach</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">8</field>
<field name="pages">e804</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The increased sequencing of pathogen genomes and the subsequent availability
of genome-scale functional datasets are expected to guide the experimental
work necessary for target-based drug discovery. However, a major bottleneck in
this has been the difficulty of capturing and integrating relevant information
in an easily accessible format for identifying and prioritizing potential
targets. The open-access resource TDRtargets.org facilitates drug target
prioritization for major tropical disease pathogens such as the mycobacteria
Mycobacterium leprae and Mycobacterium tuberculosis; the kinetoplastid
protozoans Leishmania major, Trypanosoma brucei, and Trypanosoma cruzi; the
apicomplexan protozoans Plasmodium falciparum, Plasmodium vivax, and
Toxoplasma gondii; and the helminths Brugia malayi and Schistosoma mansoni.

Methodology/Principal Findings

Here we present strategies to prioritize pathogen proteins based on whether
their properties meet criteria considered desirable in a drug target. These
criteria are based upon both sequence-derived information (e.g., molecular
mass) and functional data on expression, essentiality, phenotypes, metabolic
pathways, assayability, and druggability. This approach also highlights the
fact that data for many relevant criteria are lacking in less-studied
pathogens (e.g., helminths), and we demonstrate how this can be partially
overcome by mapping data from homologous genes in well-studied organisms. We
also show how individual users can easily upload external datasets and
integrate them with existing data in TDRtargets.org to generate highly
customized ranked lists of potential targets.

Conclusions/Significance

Using the datasets and the tools available in TDRtargets.org, we have
generated illustrative lists of potential drug targets in seven tropical
disease pathogens. While these lists are broadly consistent with the research
community&apos;s current interest in certain specific proteins, and suggest novel
target candidates that may merit further study, the lists can easily be
modified in a user-specific manner, either by adjusting the weights for chosen
criteria or by changing the criteria that are included.

Author Summary

In cell-based drug development, researchers attempt to create drugs that kill
a pathogen without necessarily understanding the details of how the drugs
work. In contrast, target-based drug development entails the search for
compounds that act on a specific intracellular target—often a protein known or
suspected to be required for survival of the pathogen. The latter approach to
drug development has been facilitated greatly by the sequencing of many
pathogen genomes and the incorporation of genome data into user-friendly
databases. The present paper shows how the database TDRtargets.org can
identify proteins that might be considered good drug targets for diseases such
as African sleeping sickness, Chagas disease, parasitic worm infections,
tuberculosis, and malaria. These proteins may score highly in searches of the
database because they are dissimilar to human proteins, are structurally
similar to other “druggable” proteins, have functions that are</field></doc>
</add>
