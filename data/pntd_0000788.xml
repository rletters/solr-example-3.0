<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000788</field>
<field name="doi">10.1371/journal.pntd.0000788</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Geneviève M. C. Labbé, Derric D. Nimmo, Luke Alphey</field>
<field name="title">piggybac- and PhiC31-Mediated Genetic Transformation of the Asian Tiger Mosquito, Aedes albopictus (Skuse)</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">8</field>
<field name="pages">e788</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The Asian tiger mosquito, Aedes albopictus (Skuse), is a vector of several
arboviruses including dengue and chikungunya. This highly invasive species
originating from Southeast Asia has travelled the world in the last 30 years
and is now established in Europe, North and South America, Africa, the Middle
East and the Caribbean. In the absence of vaccine or antiviral drugs,
efficient mosquito control strategies are crucial. Conventional control
methods have so far failed to control Ae. albopictus adequately.

Methodology/Principal Findings

Germline transformation of Aedes albopictus was achieved by micro-injection of
embryos with a piggyBac-based transgene carrying a 3xP3-ECFP marker and an
attP site, combined with piggyBac transposase mRNA and piggyBac helper
plasmid. Five independent transgenic lines were established, corresponding to
an estimated transformation efficiency of 2–3%. Three lines were re-injected
with a second-phase plasmid carrying an attB site and a 3xP3-DsRed2 marker,
combined with PhiC31 integrase mRNA. Successful site-specific integration was
observed in all three lines with an estimated transformation efficiency of
2–6%.

Conclusions/Significance

Both piggybac- and site-specific PhiC31-mediated germline transformation of
Aedes albopictus were successfully achieved. This is the first report of Ae.
albopictus germline transformation and engineering, a key step towards
studying and controlling this species using novel molecular techniques and
genetic control strategies.

Author Summary

The Asian tiger mosquito, Aedes albopictus, is a highly invasive mosquito and
has spread from South East Asia to Europe, the United States and northern
areas of Asia in the past 30 years. Aedes mosquitoes transmit a range of viral
diseases, including dengue and chikungunya. Aedes albopictus is generally
considered to be somewhat less of a concern in this regard than Aedes aegypti.
However a recent mutation in the chikungunya virus dramatically increased its
transmission by Aedes albopictus, causing an important outbreak in the Indian
Ocean in 2006 that eventually reached Italy in 2007. This highlights the
potential importance of this mosquito, which can thrive much further from the
Equator than can Aedes aegypti. This paper describes the first genetic
engineering of the Asian tiger mosquito. This is an essential step towards the
development of genetics-based control methods against this mosquito, and also
an invaluable tool for basic research. We describe both transposon-based and
site-specific integration methods.

Introduction

Aedes mosquitoes are responsible for an estimated 50 to 100 million dengue
cases worldwide every year, with nearly half the world&apos;s population at risk of
being infected [1], [2]. The two main vector species, Aedes aegypti (L.) and
Aedes albopictus (Skuse) are also the main vectors of the chikungunya virus,
which can cause severely debilitating syndromes lasting up to several months.

In the last 30 years, Ae. albopictus has travelled the world via human travel
and commerce, e.g. the trade of used tyres [3] and “lucky bamboo” [4]. It
spread from Southeast Asia and Pacific Islands to Europe, North and South
America, Africa, the Middle East and the Caribbean. Its strong ecological
plasticity, including the ability of some st</field></doc>
</add>
