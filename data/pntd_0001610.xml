<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001610</field>
<field name="doi">10.1371/journal.pntd.0001610</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Regis Gomes, Clarissa Teixeira, Fabiano Oliveira, Phillip G. Lawyer, Dia-Eldin Elnaiem, Claudio Meneses, Yasuyuki Goto, Ajay Bhatia, Randall F. Howard, Steven G. Reed, Jesus G. Valenzuela, Shaden Kamhawi</field>
<field name="title">KSAC, a Defined Leishmania Antigen, plus Adjuvant Protects against the Virulence of L. major Transmitted by Its Natural Vector Phlebotomus duboscqi</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">4</field>
<field name="pages">e1610</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Recombinant KSAC and L110f are promising Leishmania vaccine candidates. Both
antigens formulated in stable emulsions (SE) with the natural TLR4 agonist
MPL® and L110f with the synthetic TLR4 agonist GLA in SE protected BALB/c mice
against L. major infection following needle challenge. Considering the
virulence of vector-transmitted Leishmania infections, we vaccinated BALB/c
mice with either KSAC+GLA-SE or L110f+GLA-SE to assess protection against L.
major transmitted via its vector Phlebotomus duboscqi.

Methods

Mice receiving the KSAC or L110f vaccines were challenged by needle or L.
major-infected sand flies. Weekly disease progression and terminal parasite
loads were determined. Immunological responses to KSAC, L110f, or soluble
Leishmania antigen (SLA) were assessed throughout vaccination, three and
twelve weeks after immunization, and one week post-challenge.

Results

Following sand fly challenge, KSAC-vaccinated mice were protected while L110f-
vaccinated animals showed partial protection. Protection correlated with the
ability of SLA to induce IFN-γ-producing CD4+CD62LlowCCR7low effector memory T
cells pre- and post-sand fly challenge.

Conclusions

This study demonstrates the protective efficacy of KSAC+GLA-SE against sand
fly challenge; the importance of vector-transmitted challenge in evaluating
vaccine candidates against Leishmania infection; and the necessity of a rapid
potent Th1 response against Leishmania to attain true protection.

Author Summary

Leishmaniasis is a neglected disease caused by the Leishmania parasite and
transmitted by the bite of an infective sand fly. Despite the importance of
this disease there is no vaccine available for humans. Studies have shown that
vector-transmitted infections are more virulent, promoting parasite
establishment and abrogating protection observed against needle-injected
parasites in vaccinated mice. KSAC and L110f, derived from Leishmania-based
polyproteins, protected mice against the needle-injected parasites. Here, we
tested the two molecules for their capacity to protect mice against cutaneous
leishmaniasis transmitted by an infective sand fly. Our results show that
KSAC, but not L110f, confers protection against Leishmania transmitted by sand
fly bites where protection was correlated to a strong immune response to
Leishmania antigens by memory T cells before and after sand fly transmission
of the parasite. This is the first report of a Leishmania-based vaccine that
confers protection against a virulent sand fly challenge. Our results support
the importance of screening Leishmania vaccine candidates using infective sand
flies before moving forward with the costly steps of vaccine development.

Introduction

Leishmaniasis is a neglected disease endemic in 98 countries with an estimated
350 million people at risk and an estimated burden of 2,357,000 disability-
adjusted life years [1]. Visceral leishmaniasis is fatal if left untreated,
and the morbidity and stigma caused by cutaneous leishmaniasis is significant
[2]. Current treatment is dependent on long-term therapy with toxic drugs,
most requiring parentera</field></doc>
</add>
