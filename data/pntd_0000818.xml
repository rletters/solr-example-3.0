<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000818</field>
<field name="doi">10.1371/journal.pntd.0000818</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Filip Meheus, Manica Balasegaram, Piero Olliaro, Shyam Sundar, Suman Rijal, Md. Abul Faiz, Marleen Boelaert</field>
<field name="title">Cost-Effectiveness Analysis of Combination Therapies for Visceral Leishmaniasis in the Indian Subcontinent</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">9</field>
<field name="pages">e818</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Visceral leishmaniasis is a systemic parasitic disease that is fatal unless
treated. We assessed the cost and cost-effectiveness of alternative strategies
for the treatment of visceral leishmaniasis in the Indian subcontinent. In
particular we examined whether combination therapies are a cost-effective
alternative compared to monotherapies.

Methods and Findings

We assessed the cost-effectiveness of all possible mono- and combination
therapies for the treatment of visceral leishmaniasis in the Indian
subcontinent (India, Nepal and Bangladesh) from a societal perspective using a
decision analytical model based on a decision tree. Primary data collected in
each country was combined with data from the literature and an expert poll
(Delphi method). The cost per patient treated and average and incremental
cost-effectiveness ratios expressed as cost per death averted were calculated.
Extensive sensitivity analysis was done to evaluate the robustness of our
estimations and conclusions. With a cost of US$92 per death averted, the
combination miltefosine-paromomycin was the most cost-effective treatment
strategy. The next best alternative was a combination of liposomal
amphotericin B with paromomycin with an incremental cost-effectiveness of $652
per death averted. All other strategies were dominated with the exception of a
single dose of 10mg per kg of liposomal amphotericin B. While strategies based
on liposomal amphotericin B (AmBisome) were found to be the most effective,
its current drug cost of US$20 per vial resulted in a higher average cost-
effectiveness. Sensitivity analysis showed the conclusion to be robust to
variations in the input parameters over their plausible range.

Conclusions

Combination treatments are a cost-effective alternative to current monotherapy
for VL. Given their expected impact on the emergence of drug resistance, a
switch to combination therapy should be considered once final results from
clinical trials are available.

Author Summary

Visceral leishmaniasis (VL) is a serious health problem in the Indian
subcontinent affecting the rural poor. It has a significant economic impact on
concerned households. The development of drug resistance is a major problem
and threatens control efforts under the VL elimination initiative. With an
unprecedented choice of antileishmanial drugs (but no newer compound in
clinical development), policies that protect these drugs against the emergence
of resistance are required. A possible strategy that has been successfully
used for malaria and tuberculosis is the use of combination therapies. This
study is the first comprehensive assessment of the cost-effectiveness of all
possible mono- and combination therapies for the treatment of visceral
leishmaniasis in the Indian subcontinent. The analysis was done from the
societal perspective, including both health provider and household costs. The
present work shows that combination treatments are a cost-effective
alternative to current monotherapy for VL. Given their expected impact on
emergence of drug resistance, the use of combination therapy should be
considered in the context of the VL elimination programme in the Indian
subcontinent.

Introduction

Despite their toxicity, pentavalent antimonials are stil</field></doc>
</add>
