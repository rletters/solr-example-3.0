<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000580</field>
<field name="doi">10.1371/journal.pntd.0000580</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Anna Svárovská, Thomas H. Ant, Veronika Seblová, Lucie Jecná, Stephen M. Beverley, Petr Volf</field>
<field name="title">Leishmania major Glycosylation Mutants Require Phosphoglycans (lpg2−) but Not Lipophosphoglycan (lpg1−) for Survival in Permissive Sand Fly Vectors</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">1</field>
<field name="pages">e580</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Sand fly species able to support the survival of the protozoan parasite
Leishmania have been classified as permissive or specific, based upon their
ability to support a wide or limited range of strains and/or species. Studies
of a limited number of fly/parasite species combinations have implicated
parasite surface molecules in this process and here we provide further
evidence in support of this proposal. We investigated the role of
lipophosphoglycan (LPG) and other phosphoglycans (PGs) in sand fly survival,
using Leishmania major mutants deficient in LPG (lpg1−), and the phosphoglycan
(PG)-deficient mutant lpg2−. The sand fly species used were the permissive
species Phlebotomus perniciosus and P. argentipes, and the specific vector P.
duboscqi, a species resistant to L. infantum development.

Principal Findings

The lpg2− mutants did not survive well in any of the three sand fly species,
suggesting that phosphoglycans and/or other LPG2-dependent molecules are
required for parasite development. In vitro, all three L. major lines were
equally resistant to proteolytic activity of bovine trypsin, suggesting that
sand fly-specific hydrolytic proteases or other factors are the reason for the
early lpg2− parasite killing. The lpg1− mutants developed late-stage
infections in two permissive species, P. perniciosus and P. argentipes, where
their infection rates and intensities of infections were comparable to the
wild type (WT) parasites. In contrast, in P. duboscqi the lpg1− mutants
developed significantly worse than the WT parasites.

Conclusions

In combination with previous studies, the data establish clearly that LPG is
not required for Leishmania survival in permissive species P. perniciosus and
P. argentipes but plays an important role in the specific vector P. duboscqi.
With regard to PGs other than LPG, the data prove the importance of
LPG2-related molecules for survival of L. major in the three sand fly species
tested.

Author Summary

Phlebotomine sand flies are small blood-feeding insects, medically important
as vectors of protozoan parasites of the genus Leishmania. Sand flies species
can be divided roughly into two groups, termed specific or permissive,
depending on their ability to support development of one or a few strains vs.
a broad spectrum of these parasites. In this study, we explored the ability of
two Leishmania major glycocalyx mutants to survive within these different
types of vectors. The lpg1− mutant, which specifically lacks lipophosphoglycan
(LPG), was able to survive normally in two permissive species, Phlebotomus
argentipes and P. perniciosus, but was only able to survive within the
specific species P. duboscqi for a limited time prior to dissolution of the
peritrophic matrix. Consistent with its classification as a specific sand fly
vector, P. duboscqi was not able to support development of L. infantum. The
lpg2− L. major mutant, which is a broader mutant and lacks all phosphoglycans
including LPG and proteophosphoglycans, was unable to survive in all the three
vector species tested. This study extends the knowledge on the role of
Leishmania major surface glycoconjugates to development in three important
vector species and gives support</field></doc>
</add>
