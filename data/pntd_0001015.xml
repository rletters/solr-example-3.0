<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001015</field>
<field name="doi">10.1371/journal.pntd.0001015</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Jacklyn Wong, Steven T. Stoddard, Helvio Astete, Amy C. Morrison, Thomas W. Scott</field>
<field name="title">Oviposition Site Selection by the Dengue Vector Aedes aegypti and Its Implications for Dengue Control</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">4</field>
<field name="pages">e1015</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Because no dengue vaccine or antiviral therapy is commercially available,
controlling the primary mosquito vector, Aedes aegypti, is currently the only
means to prevent dengue outbreaks. Traditional models of Ae. aegypti assume
that population dynamics are regulated by density-dependent larval competition
for food and little affected by oviposition behavior. Due to direct impacts on
offspring survival and development, however, mosquito choice in oviposition
site can have important consequences for population regulation that should be
taken into account when designing vector control programs.

Methodology/Principal Findings

We examined oviposition patterns by Ae. aegypti among 591 naturally occurring
containers and a set of experimental containers in Iquitos, Peru. Using larval
starvation bioassays as an indirect measure of container food content, we
assessed whether females select containers with the most food for their
offspring. Our data indicate that choice of egg-laying site is influenced by
conspecific larvae and pupae, container fill method, container size, lid, and
sun exposure. Although larval food positively influenced oviposition, our
results did not support the hypothesis that females act primarily to maximize
food for larvae. Females were most strongly attracted to sites containing
immature conspecifics, even when potential competitors for their progeny were
present in abundance.

Conclusion/Significance

Due to strong conspecific attraction, egg-laying behavior may contribute more
to regulating Ae. aegypti populations than previously thought. If highly
infested containers are targeted for removal or larvicide application, females
that would have preferentially oviposited in those sites may instead
distribute their eggs among other suitable, previously unoccupied containers.
Strategies that kill mosquitoes late in their development (i.e., insect growth
regulators that kill pupae rather than larvae) will enhance vector control by
creating “egg sinks,” treated sites that exploit conspecific attraction of
ovipositing females, but reduce emergence of adult mosquitoes via density-
dependent larval competition and late acting insecticide.

Author Summary

Controlling the mosquito Aedes aegypti is of public health importance because,
at present, it is the only means to stop dengue virus transmission.
Implementing successful mosquito control programs requires understanding what
factors regulate population abundance, as well as anticipating how mosquitoes
may adapt to control measures. In some species of mosquitoes, females choose
egg-laying sites to improve the survival and growth of their offspring, a
behavior that ultimately influences population distribution and abundance. In
the current study, we tested whether Ae. aegypti actively choose the
containers in which they lay their eggs and determined what cues are most
relevant to that process. We also explored whether females select containers
that provide the most food for their larval progeny. Surprisingly, egg-laying
females were most attracted to sites containing other immature Ae. aegypti,
rather than to sites containing the most food. We propose that this behavior
may contribute to density-dependent competition for food among larvae and play
a larger role than previously </field></doc>
</add>
