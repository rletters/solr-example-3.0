<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001678</field>
<field name="doi">10.1371/journal.pntd.0001678</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Robin H. Miller, Penny Masuoka, Terry A. Klein, Heung-Chul Kim, Todd Somer, John Grieco</field>
<field name="title">Ecological Niche Modeling to Estimate the Distribution of Japanese Encephalitis Virus in Asia</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">6</field>
<field name="pages">e1678</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Culex tritaeniorhynchus is the primary vector of Japanese encephalitis virus
(JEV), a leading cause of encephalitis in Asia. JEV is transmitted in an
enzootic cycle involving large wading birds as the reservoirs and swine as
amplifying hosts. The development of a JEV vaccine reduced the number of JE
cases in regions with comprehensive childhood vaccination programs, such as in
Japan and the Republic of Korea. However, the lack of vaccine programs or
insufficient coverage of populations in other endemic countries leaves many
people susceptible to JEV. The aim of this study was to predict the
distribution of Culex tritaeniorhynchus using ecological niche modeling.

Methods/Principal Findings

An ecological niche model was constructed using the Maxent program to map the
areas with suitable environmental conditions for the Cx. tritaeniorhynchus
vector. Program input consisted of environmental data (temperature, elevation,
rainfall) and known locations of vector presence resulting from an extensive
literature search and records from MosquitoMap. The statistically significant
Maxent model of the estimated probability of Cx. tritaeniorhynchus presence
showed that the mean temperatures of the wettest quarter had the greatest
impact on the model. Further, the majority of human Japanese encephalitis (JE)
cases were located in regions with higher estimated probability of Cx.
tritaeniorhynchus presence.

Conclusions/Significance

Our ecological niche model of the estimated probability of Cx.
tritaeniorhynchus presence provides a framework for better allocation of
vector control resources, particularly in locations where JEV vaccinations are
unavailable. Furthermore, this model provides estimates of vector probability
that could improve vector surveillance programs and JE control efforts.

Author Summary

Japanese encephalitis virus (JEV) is transmitted predominately by the
mosquito, Culex tritaeniorhynchus. The primary reservoirs of the virus are
wading birds, with swine serving as amplifying hosts. Despite the development
of a JEV vaccine, people remain unvaccinated in endemic countries and are
susceptible to JEV infection. The distribution of the JEV vector(s) provides
essential information for preventive measures. This study used an ecological
niche modeling program to predict the distribution of Cx. tritaeniorhynchus
based on collection records and environmental maps (climate, land cover, and
elevation). The model showed that the mean temperatures of the wettest quarter
had the greatest impact on the model. Of the 25 countries endemic for Japanese
encephalitis (JE) endemic countries, seven possessed greater than 50% land
area with an estimated high probability of Cx. tritaeniorhynchus presence. Our
model provides a useful tool for JEV surveillance programs that focus on
vector control strategies.

Introduction

Japanese encephalitis virus (JEV), the causative agent of Japanese
encephalitis (JE), is an arbovirus that belongs to the family Flaviviridae and
is endemic to Southeast and Northeast Asia, the Pacific Islands, and northern
Australia (Figure 1) [1]. The primary vector of JEV is Culex tritaeniorhynchus
Giles, but other Culex species (e.g., Culex annulirostris, Culex vishnui
Theobald, Culex bitaeniorhynchus Giles, and Culex pipiens Linnaeus) </field></doc>
</add>
