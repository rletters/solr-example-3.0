<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000775</field>
<field name="doi">10.1371/journal.pntd.0000775</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Chuin-Shee Shang, Chi-Tai Fang, Chung-Ming Liu, Tzai-Hung Wen, Kun-Hsien Tsai, Chwan-Chuen King</field>
<field name="title">The Role of Imported Cases and Favorable Meteorological Conditions in the Onset of Dengue Epidemics</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">8</field>
<field name="pages">e775</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Travelers who acquire dengue infection are often routes for virus transmission
to other regions. Nevertheless, the interplay between infected travelers,
climate, vectors, and indigenous dengue incidence remains unclear. The role of
foreign-origin cases on local dengue epidemics thus has been largely neglected
by research. This study investigated the effect of both imported dengue and
local meteorological factors on the occurrence of indigenous dengue in Taiwan.

Methods and Principal Findings

Using logistic and Poisson regression models, we analyzed bi-weekly,
laboratory-confirmed dengue cases at their onset dates of illness from 1998 to
2007 to identify correlations between indigenous dengue and imported dengue
cases (in the context of local meteorological factors) across different time
lags. Our results revealed that the occurrence of indigenous dengue was
significantly correlated with temporally-lagged cases of imported dengue (2–14
weeks), higher temperatures (6–14 weeks), and lower relative humidity (6–20
weeks). In addition, imported and indigenous dengue cases had a significant
quantitative relationship in the onset of local epidemics. However, this
relationship became less significant once indigenous epidemics progressed past
the initial stage.

Conclusions

These findings imply that imported dengue cases are able to initiate
indigenous epidemics when appropriate weather conditions are present. Early
detection and case management of imported cases through rapid diagnosis may
avert large-scale epidemics of dengue/dengue hemorrhagic fever. The deployment
of an early-warning surveillance system, with the capacity to integrate
meteorological data, will be an invaluable tool for successful prevention and
control of dengue, particularly in non-endemic countries.

Author Summary

Dengue/dengue hemorrhagic fever is the world&apos;s most widely spread mosquito-
borne arboviral disease and threatens more than two-thirds of the world&apos;s
population. Cases are mainly distributed in tropical and subtropical areas in
accordance with vector habitats for Aedes aegypti and Ae. albopictus. However,
the role of imported cases and favorable meteorological conditions has not yet
been quantitatively assessed. This study verified the correlation between the
occurrence of indigenous dengue and imported cases in the context of weather
variables (temperature, rainfall, relative humidity, etc.) for different time
lags in southern Taiwan. Our findings imply that imported cases have a role in
igniting indigenous outbreaks, in non-endemics areas, when favorable weather
conditions are present. This relationship becomes insignificant in the late
phase of local dengue epidemics. Therefore, early detection and case
management of imported cases through timely surveillance and rapid laboratory-
diagnosis may avert large scale epidemics of dengue/dengue hemorrhagic fever.
An early-warning surveillance system integrating meteorological data will be
an invaluable tool for successful prevention and control of dengue,
particularly in non-endemic countries.

Introduction

Dengue outbreaks initiated by international tourists, immigrants, and foreign
workers have been reported in numerous developed areas and countries [1], [2],
[3]. Nevertheless, the i</field></doc>
</add>
