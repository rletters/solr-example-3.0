<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001436</field>
<field name="doi">10.1371/journal.pntd.0001436</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Thomas P. C. Dorlo, Pieter P. A. M. van Thiel, Gerard J. Schoone, Ymkje Stienstra, Michèle van Vugt, Jos H. Beijnen, Peter J. de Vries</field>
<field name="title">Dynamics of Parasite Clearance in Cutaneous Leishmaniasis Patients Treated with Miltefosine</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">12</field>
<field name="pages">e1436</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Parasite loads were quantified in repeated skin biopsies from lesions of 2
patients with Old-World cutaneous leishmaniasis (CL) caused by Leishmania
major and L. infantum during and after treatment with miltefosine. Miltefosine
induced a rapid therapeutic effect on both infections with an initial decline
of parasites of ∼1 log/week for the L. major infection. These observations
illustrate the usability of quantifying parasite loads in skin lesions as a
pharmacodynamic measure and quantitative descriptor of drug effect for CL
supporting clinical assessment.

Author Summary

The clinical evaluation of the ulcerated lesions in cutaneous leishmaniasis
(CL) is both difficult and subjective. As a result, the evaluation of
therapeutic efficacy of drugs for CL remains complicated. The relationship
between dose and effect of antileishmanial drugs in CL is unclear and a good
quantitative descriptor of drug effect has not yet been established. This
report describes the use of quantifying the parasite load in repeated full-
thickness skin biopsies from lesions of two patients with extensive Old-World
CL (Leishmania major and L. infantum) who both were treated with miltefosine
to demonstrate the dynamics of parasite clearance within CL lesions.
Therapeutic effect of miltefosine was already noticeable directly after start
of treatment, with a rapid, log-linear decline in parasite load in the skin
biopsies of approximately 1 log/week for the L. major infection. These
observations illustrate the applicability of quantifying parasite loads as a
pharmacodynamic measure for CL supporting clinical assessment. The methodology
described here might enable better evaluation and comparison of standard and
new therapeutics in future randomized clinical trials for CL.

Introduction

Miltefosine is an oral antileishmanial drug widely used in the management of
visceral leishmaniasis (VL) in the Indian subcontinent [1]. There is
increasing evidence on the efficacy of miltefosine in New-World cutaneous
leishmaniasis (CL) [2]. In Old-World CL the applicability of miltefosine is
documented in a few reports with Leishmania major as causative species
[3]–[6]. The miltefosine dosage regimens used in CL are based on those
established in Indian VL patients and lack a rational background [7].

Treatment can be given to speed up spontaneous healing of CL. Systemic
treatment is indicated in ‘complex’ disease in patients with multiple lesions
(&gt;5), with lesions in cosmetically or functionally delicate areas, with
lesions which are non-responsive to intralesional treatment or when
mucocutaneous leishmaniasis may develop. Clinical assessment of the progress
and healing of CL lesions remains difficult, certainly in complex CL cases,
where definitive cure from a clinical perspective is determined up to 6 months
post-treatment.

Nothing is known about the dose-effect relationship of miltefosine in CL,
mainly because a good quantitative descriptor of drug effect has not yet been
established. In an attempt to rationalize the treatment of CL and to support
the clinical examination and follow-up, two patients with Old-World CL for
whom systemic miltefosine treatment was indicated were followed-up by
measuring the Leishmania parasite load in rep</field></doc>
</add>
