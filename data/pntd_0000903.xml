<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000903</field>
<field name="doi">10.1371/journal.pntd.0000903</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Sirenda Vong, Virak Khieu, Olivier Glass, Sowath Ly, Veasna Duong, Rekol Huy, Chantha Ngan, Ole Wichmann, G. William Letson, Harold S. Margolis, Philippe Buchy</field>
<field name="title">Dengue Incidence in Urban and Rural Cambodia: Results from Population-Based Active Fever Surveillance, 2006–2008</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">11</field>
<field name="pages">e903</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Dengue vaccines are now in late-stage development, and evaluation and robust
estimates of dengue disease burden are needed to facilitate further
development and introduction. In Cambodia, the national dengue case-definition
only allows reporting of children less than 16 years of age, and little is
known about dengue burden in rural areas and among older persons. To estimate
the true burden of dengue in the largest province of Cambodia, Kampong Cham,
we conducted community-based active dengue fever surveillance among the
0-to-19–year age group in rural villages and urban areas during 2006–2008.

Methods and Findings

Active surveillance for febrile illness was conducted in 32 villages and 10
urban areas by mothers trained to use digital thermometers combined with
weekly home visits to identify persons with fever. An investigation team
visited families with febrile persons to obtain informed consent for
participation in the follow-up study, which included collection of personal
data and blood specimens. Dengue-related febrile illness was defined using
molecular and serological testing of paired acute and convalescent blood
samples. Over the three years of surveillance, 6,121 fever episodes were
identified with 736 laboratory-confirmed dengue virus (DENV) infections for
incidences of 13.4–57.8/1,000 person-seasons. Average incidence was highest
among children less than 7 years of age (41.1/1,000 person-seasons) and lowest
among the 16-to-19–year age group (11.3/1,000 person-seasons). The
distribution of dengue was highly focal, with incidence rates in villages and
urban areas ranging from 1.5–211.5/1,000 person-seasons (median 36.5). During
a DENV-3 outbreak in 2007, rural areas were affected more than urban areas
(incidence 71 vs. 17/1,000 person-seasons, p&lt;0.001).

Conclusion

The large-scale active surveillance study for dengue fever in Cambodia found a
higher disease incidence than reported to the national surveillance system,
particularly in preschool children and that disease incidence was high in both
rural and urban areas. It also confirmed the previously observed focal nature
of dengue virus transmission.

Author Summary

Dengue is a major public health problem in South-East Asia. Several dengue
vaccine candidates are now in late-stage development and are being evaluated
in clinical trials. Accurate estimates of true dengue disease burden will
become an important factor in the public-health decision-making process for
endemic countries once safe and effective vaccines become available. However,
estimates of the true disease incidence are difficult to make, because
national surveillance systems suffer from disease under-recognition and
reporting. Dengue is mainly reported among children, and in some countries,
such as Cambodia, the national case definition only includes hospitalized
children. This study used active, community-based surveillance of febrile
illness coupled with laboratory testing for DENV infection to identify cases
of dengue fever in rural and urban populations. We found a high burden of
dengue in young children and late adolescents in both rural and urban
communities at a magnitude greater than previously describe</field></doc>
</add>
