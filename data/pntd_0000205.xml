<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000205</field>
<field name="doi">10.1371/journal.pntd.0000205</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Heather R. Williamson, Mark E. Benbow, Khoa D. Nguyen, Dia C. Beachboard, Ryan K. Kimbirauskas, Mollie D. McIntosh, Charles Quaye, Edwin O. Ampadu, Daniel Boakye, Richard W. Merritt, Pamela L. C. Small</field>
<field name="title">Distribution of Mycobacterium ulcerans in Buruli Ulcer Endemic and Non-Endemic Aquatic Sites in Ghana</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">3</field>
<field name="pages">e205</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Mycobacterium ulcerans, the causative agent of Buruli ulcer, is an emerging
environmental bacterium in Australia and West Africa. The primary risk factor
associated with Buruli ulcer is proximity to slow moving water. Environmental
constraints for disease are shown by the absence of infection in arid regions
of infected countries. A particularly mysterious aspect of Buruli ulcer is the
fact that endemic and non-endemic villages may be only a few kilometers apart
within the same watershed. Recent studies suggest that aquatic invertebrate
species may serve as reservoirs for M. ulcerans, although transmission
pathways remain unknown. Systematic studies of the distribution of M. ulcerans
in the environment using standard ecological methods have not been reported.
Here we present results from the first study based on random sampling of
endemic and non-endemic sites. In this study PCR-based methods, along with
biofilm collections, have been used to map the presence of M. ulcerans within
26 aquatic sites in Ghana. Results suggest that M. ulcerans is present in both
endemic and non-endemic sites and that variable number tandem repeat (VNTR)
profiling can be used to follow chains of transmission from the environment to
humans. Our results suggesting that the distribution of M. ulcerans is far
broader than the distribution of human disease is characteristic of
environmental pathogens. These findings imply that focal demography, along
with patterns of human water contact, may play a major role in transmission of
Buruli ulcer.

Author Summary

Buruli ulcer is an ulcerative skin disease caused by Mycobacterium ulcerans.
Though usually not fatal, ulceration can cover up to 15% of the body, with
treatment being costly and sometimes painful. Primary risk for Buruli ulcer in
Africa is exposure to stagnant water, but the route of transmission is
unknown. Detection of M. ulcerans in aquatic insects in endemic sites suggests
the presence of aquatic reservoirs. This article reports results from the
first investigation into the ecology of M. ulcerans based on random sampling
of both endemic and non-endemic aquatic sites. Development of a method for
discriminating M. ulcerans from closely related mycobacterial pathogens made
it possible to determine the distribution of M. ulcerans in aquatic
environments. This article demonstrates the presence of M. ulcerans DNA in
both endemic and non-endemic sites within aquatic insects, water filtrate, and
glass-slide biofilm communities. This article provides data suggesting that M.
ulcerans is more broadly distributed than the human disease it causes. This
study provides an initial step for future work on whether certain M. ulcerans
strains are particularly successful human pathogens and suggests that research
on specific human water contact factors may provide insight into the
transmission of M. ulcerans.

Introduction

Mycobacterium ulcerans is the cause of Buruli ulcer, a severe necrotizing skin
infection (Figure 1). Although Buruli ulcer is globally distributed, it is an
emerging infection primarily in Australia and West Africa [1]. The disease
begins as a painless nodule or papule that, if left untreated, can le</field></doc>
</add>
