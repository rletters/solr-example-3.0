<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000656</field>
<field name="doi">10.1371/journal.pntd.0000656</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Christopher Kribs-Zaleta</field>
<field name="title">Estimating Contact Process Saturation in Sylvatic Transmission of Trypanosoma cruzi in the United States</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">4</field>
<field name="pages">e656</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Although it has been known for nearly a century that strains of Trypanosoma
cruzi, the etiological agent for Chagas&apos; disease, are enzootic in the southern
U.S., much remains unknown about the dynamics of its transmission in the
sylvatic cycles that maintain it, including the relative importance of
different transmission routes. Mathematical models can fill in gaps where
field and lab data are difficult to collect, but they need as inputs the
values of certain key demographic and epidemiological quantities which
parametrize the models. In particular, they determine whether saturation
occurs in the contact processes that communicate the infection between the two
populations. Concentrating on raccoons, opossums, and woodrats as hosts in
Texas and the southeastern U.S., and the vectors Triatoma sanguisuga and
Triatoma gerstaeckeri, we use an exhaustive literature review to derive
estimates for fundamental parameters, and use simple mathematical models to
illustrate a method for estimating infection rates indirectly based on
prevalence data. Results are used to draw conclusions about saturation and
which population density drives each of the two contact-based infection
processes (stercorarian/bloodborne and oral). Analysis suggests that the
vector feeding process associated with stercorarian transmission to hosts and
bloodborne transmission to vectors is limited by the population density of
vectors when dealing with woodrats, but by that of hosts when dealing with
raccoons and opossums, while the predation of hosts on vectors which drives
oral transmission to hosts is limited by the population density of hosts.
Confidence in these conclusions is limited by a severe paucity of data
underlying associated parameter estimates, but the approaches developed here
can also be applied to the study of other vector-borne infections.

Author Summary

The parasite Trypanosoma cruzi, transmitted by insect vectors, causes Chagas&apos;
disease, which affects millions of people throughout the Americas and over 100
other mammalian species. In the United States, infection in humans is believed
rare, but prevalence is high in hosts like raccoons and opossums in the
southeast and woodrats in Texas and northern Mexico. The principal U.S. vector
species appear inefficient, however, so hosts may be primarily infected by
congenital transmission and oral transmission caused by eating infected
vectors. Mathematical models can evaluate the importance of each transmission
route but require as inputs estimates for basic contact rates and demographic
information. We estimate basic quantities via an exhaustive review of T. cruzi
transmission in the southern and southeastern U.S., and use properties of
mathematical models to estimate infection rates and the threshold (saturation)
population-density ratios that govern whether each infection process depends
on host or vector density. Results (based on extremely limited data) suggest
that oral transmission is always driven by host density, while transmission to
vectors depends upon host density in cycles involving raccoons and opossums,
but upon vector density in cycles involving woodrats, which live in higher
concentrations.

Introduction

Since the Brazilian physician Carlos Chagas discovered the parasite
Trypanosoma cruzi in 1909, much research has been devoted throughout the
Americas </field></doc>
</add>
