<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001311</field>
<field name="doi">10.1371/journal.pntd.0001311</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Kathryn B. Anderson, Robert V. Gibbons, Stephen J. Thomas, Alan L. Rothman, Ananda Nisalak, Ruth L. Berkelman, Daniel H. Libraty, Timothy P. Endy</field>
<field name="title">Preexisting Japanese Encephalitis Virus Neutralizing Antibodies and Increased Symptomatic Dengue Illness in a School-Based Cohort in Thailand</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">10</field>
<field name="pages">e1311</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Dengue viruses (DENVs) and Japanese encephalitis virus (JEV) have significant
cross-reactivity in serological assays; the clinical implications of this
remain undefined. An improved understanding of whether and how JEV immunity
modulates the clinical outcome of DENV infection is important as large-scale
DENV vaccine trials will commence in areas where JEV is co-endemic and/or JEV
immunization is routine.

Methods and Findings

The association between preexisting JEV neutralizing antibodies (NAbs) and the
clinical severity of DENV infection was evaluated in a prospective school-
based cohort in Thailand that captured asymptomatic, non-hospitalized, and
hospitalized DENV infections. Covariates considered included age, baseline
DENV antibody status, school of attendance, epidemic year, and infecting DENV
serotype. 942 children experienced at least one DENV infection between 1998
and 2002, out of 3,687 children who were enrolled for at least one full year.
In crude analysis, the presence of JEV NAbs was associated with an increased
occurrence of symptomatic versus asymptomatic infection (odds ratio [OR] =
1.55, 95% CI: 1.08–2.23) but not hospitalized illness or dengue hemorrhagic
fever (DHF). The association was strongest in children with negative DENV
serology (DENV-naive) (OR = 2.75, 95% CI: 1.12–6.72), for whom the presence of
JEV NAbs was also associated with a symptomatic illness of longer duration
(5.4 days for JEV NAb+ versus 2.6 days for JEV NAb-, p = 0.048). JEV NAbs were
associated with increased DHF in younger children with multitypic DENV NAb
profiles (OR = 4.05, 95% CI: 1.18 to 13.87). Among those with JEV NAbs, the
association with symptomatic illness did not vary by antibody titer.

Interpretation

The prior existence of JEV NAbs was associated with an increased probability
of symptomatic as compared to asymptomatic DENV illness. These findings are in
contrast to previous studies suggesting an attenuating effect of heterologous
flavivirus immunity on DENV disease severity.

Author Summary

Dengue viruses (DENVs) and Japanese encephalitis virus (JEV) have significant
cross-reactivity in serological assays, but the possible clinical implications
of this remain poorly understood. Interactions between these flaviviruses are
potentially important for public health because wild-type JEV continues to co-
circulate with DENV in Southeast Asia, the area with the highest burden of
DENV illness, and JEV vaccination coverage in this region is high. In this
study, we examined how preexisting JEV neutralizing antibodies (NAbs)
influenced the clinical severity of subsequent DENV infection using data from
a prospective school-based cohort study in Thailand that captured a wide range
of clinical severities, including asymptomatic, non-hospitalized, and
hospitalized DENV infections. We found that the prior existence of JEV NAbs
was associated with an increased occurrence of symptomatic versus asymptomatic
DENV infection. This association was most notable in DENV-naives, in whom the
presence of JEV NAbs was also associated with an illness of longer duration.
These findings suggest that the issue of heterologous flavivirus im</field></doc>
</add>
