<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001178</field>
<field name="doi">10.1371/journal.pntd.0001178</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Carly R. Perry, Melissa L. Burke, Deborah J. Stenzel, Donald P. McManus, Grant A. Ramm, Geoffrey N. Gobert</field>
<field name="title">Differential Expression of Chemokine and Matrix Re-Modelling Genes Is Associated with Contrasting Schistosome-Induced Hepatopathology in Murine Models</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">6</field>
<field name="pages">e1178</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

The pathological outcomes of schistosomiasis are largely dependent on the
molecular and cellular mechanisms of the host immune response. In this study,
we investigated the contribution of variations in host gene expression to the
contrasting hepatic pathology observed between two inbred mouse strains
following Schistosoma japonicum infection. Whole genome microarray analysis
was employed in conjunction with histological and immunohistochemical analysis
to define and compare the hepatic gene expression profiles and cellular
composition associated with the hepatopathology observed in S. japonicum-
infected BALB/c and CBA mice. We show that the transcriptional profiles differ
significantly between the two mouse strains with high statistical confidence.
We identified specific genes correlating with the more severe pathology
associated with CBA mice, as well as genes which may confer the milder degree
of pathology associated with BALB/c mice. In BALB/c mice, neutrophil genes
exhibited striking increases in expression, which coincided with the
significantly greater accumulation of neutrophils at granulomatous regions
seen in histological sections of hepatic tissue. In contrast, up-regulated
expression of the eosinophil chemokine CCL24 in CBA mice paralleled the
cellular influx of eosinophils to the hepatic granulomas. Additionally, there
was greater down-regulation of genes involved in metabolic processes in CBA
mice, reflecting the more pronounced hepatic damage in these mice. Profibrotic
genes showed similar levels of expression in both mouse strains, as did genes
associated with Th1 and Th2 responses. However, imbalances in expression of
matrix metalloproteinases (e.g. MMP12, MMP13) and tissue inhibitors of
metalloproteinases (TIMP1) may contribute to the contrasting pathology
observed in the two strains. Overall, these results provide a more complete
picture of the molecular and cellular mechanisms which govern the pathological
outcome of hepatic schistosomiasis. This improved understanding of the
immunopathogenesis in the murine model schistosomiasis provides the basis for
a better appreciation of the complexities associated with chronic human
schistosomiasis.

Author Summary

Schistosomiasis is a significant cause of morbidity and mortality in the
tropical world although its true burden has been historically underestimated.
Millions of people currently endure severe pathology as a result of
schistosome infections, although some individuals appear to be less
susceptible to infection despite constant parasite exposure. A similar range
of disease susceptibility is evident in different strains of inbred mice
infected with schistosomes, thereby mirroring the clinical situation.
Granuloma formation in the liver of both humans and mice is a characteristic
manifestation of chronic schistosomiasis, and is largely controlled by gene
signalling pathways. Certain genes expressed in particular cohorts of mice and
humans may be associated with the development of severe pathology, or may
confer a protective phenotype. This murine study highlights some key molecular
aspects of chronic schistosomiasis which may be responsible for the
development of both mild and severe pathology, and provides </field></doc>
</add>
