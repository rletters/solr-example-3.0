<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001541</field>
<field name="doi">10.1371/journal.pntd.0001541</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">José A. Ruiz-Postigo, José R. Franco, Mounir Lado, Pere P. Simarro</field>
<field name="title">Human African Trypanosomiasis in South Sudan: How Can We Prevent a New Epidemic?</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">5</field>
<field name="pages">e1541</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Human African trypanosomiasis (HAT) has been a major public health problem in
South Sudan for the last century. Recurrent outbreaks with a repetitive
pattern of responding-scaling down activities have been observed. Control
measures for outbreak response were reduced when the prevalence decreased
and/or socio-political crisis erupted, leading to a new increase in the number
of cases. This paper aims to raise international awareness of the threat of
another outbreak of sleeping sickness in South Sudan. It is a review of the
available data, interventions over time, and current reports on the status of
HAT in South Sudan. Since 2006, control interventions and treatments providing
services for sleeping sickness have been reduced. Access to HAT diagnosis and
treatment has been considerably diminished. The current status of control
activities for HAT in South Sudan could lead to a new outbreak of the disease
unless 1) the remaining competent personnel are used to train younger staff to
resume surveillance and treatment in the centers where HAT activities have
stopped, and 2) control of HAT continues to be given priority even when the
number of cases has been substantially reduced. Failure to implement an
effective and sustainable system for HAT control and surveillance will
increase the risk of a new epidemic. That would cause considerable suffering
for the affected population and would be an impediment to the socioeconomic
development of South Sudan.

Introduction

Human African trypanosomiasis (HAT), also known as sleeping sickness, is a
deadly disease caused by subspecies of Trypanosoma brucei (Protozoa,
Kinetoplastida)—T.b. gambiense and T.b. rhodesiense—transmitted to humans
through the bite of insect vectors of the genus Glossina (tsetse flies)
[1]–[5].

The disease has been a major public health problem in South Sudan for the last
century [6]. Foci due to T.b. gambiense have been described in the Greater
Equatoria Region bordering the Central African Republic [7], Democratic
Republic of the Congo [8], [9], and Uganda [10], [11]. HAT caused by T.b.
rhodesiense has been reported from areas of Jonglei state (Akobo County)
bordering Gambella in Ethiopia [12], [13], although since 1984 no HAT cases
have been reported from either Gambella or Jonglei [14].

In South Sudan, the main vector of the disease is Glossina fuscipes, but G.
tachinoides, G. pallidipes, and G. morsitans have also been found in the
Greater Equatoria Region [15]–[18].

Since the disease was first described in South Sudan, recurrent outbreaks with
a repetitive pattern of response-scaling-down activities have been observed.
Control measures for outbreak response were reduced when the prevalence
decreased and/or socio-political crisis erupted, leading to a resurgence in
the number of cases. That pattern may now reoccur due to difficulties in
maintaining a high level of HAT control activities following the recent
decrease in prevalence. Decision makers should call for urgent action to
continue surveillance in order to avoid repeating that pattern. This is
essential to achieve sustainable control of the disease.

South Sudan became a new nation in July 2011. The health services are still in
the building phase and so far, insecurity is posing difficulties for health
authorities and other implementers to easily acces</field></doc>
</add>
