<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001335</field>
<field name="doi">10.1371/journal.pntd.0001335</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Maria V. Sharakhova, Vladimir A. Timoshevskiy, Fan Yang, Sergei Iu. Demin, David W. Severson, Igor V. Sharakhov</field>
<field name="title">Imaginal Discs – A New Source of Chromosomes for Genome Mapping of the Yellow Fever Mosquito Aedes aegypti</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">10</field>
<field name="pages">e1335</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The mosquito Aedes aegypti is the primary global vector for dengue and yellow
fever viruses. Sequencing of the Ae. aegypti genome has stimulated research in
vector biology and insect genomics. However, the current genome assembly is
highly fragmented with only ∼31% of the genome being assigned to chromosomes.
A lack of a reliable source of chromosomes for physical mapping has been a
major impediment to improving the genome assembly of Ae. aegypti.

Methodology/Principal Findings

In this study we demonstrate the utility of mitotic chromosomes from imaginal
discs of 4th instar larva for cytogenetic studies of Ae. aegypti. High numbers
of mitotic divisions on each slide preparation, large sizes, and reproducible
banding patterns of the individual chromosomes simplify cytogenetic
procedures. Based on the banding structure of the chromosomes, we have
developed idiograms for each of the three Ae. aegypti chromosomes and placed
10 BAC clones and a 18S rDNA probe to precise chromosomal positions.

Conclusion

The study identified imaginal discs of 4th instar larva as a superior source
of mitotic chromosomes for Ae. aegypti. The proposed approach allows precise
mapping of DNA probes to the chromosomal positions and can be utilized for
obtaining a high-quality genome assembly of the yellow fever mosquito.

Author Summary

Dengue fever is an emerging health threat to as much as half of the human
population around the world. No vaccines or drug treatments are currently
available. Thus, disease prevention is largely based on efforts to control its
major mosquito vector Ae. aegypti. Novel vector control strategies, such as
population replacement with pathogen-incompetent transgenic mosquitoes, rely
on detailed knowledge of the genome organization for the mosquito. However,
the current genome assembly of Ae. aegypti is highly fragmented and requires
additional physical mapping onto chromosomes. The absence of readable polytene
chromosomes makes genome mapping for this mosquito extremely challenging. In
this study, we discovered and investigated a new source of chromosomes useful
for the cytogenetic analysis in Ae. aegypti – mitotic chromosomes from
imaginal discs of 4th instar larvae. Using natural banding patterns of these
chromosomes, we developed a new band-based approach for physical mapping of
DNA probes to the precise chromosomal positions. Further application of this
approach for genome mapping will greatly enhance the utility of the existing
draft genome sequence assembly for Ae. aegypti and thereby facilitate
application of advanced genome technologies for investigating and developing
novel genetic control strategies for dengue transmission.

Introduction

Ae. aegypti is a principal vector for yellow fever, dengue and chikungunya
viruses [1], [2]. These diseases have a significant worldwide impact on human
health. Yellow fever affects up to 600 million lives and is responsible for
about 30,000 deaths annually [3]. Dengue fever is a threat to &gt;2.5 billion
people in tropical and subtropical regions, where between 50 to 100 million
infections occur each year [2], [4], [5]. The incidence of dengue fever is
increasing globally [6], for example in developed areas like Singapore where
dengue was thought t</field></doc>
</add>
