<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000464</field>
<field name="doi">10.1371/journal.pntd.0000464</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Taisaku Nogi, Dan Zhang, John D. Chan, Jonathan S. Marchant</field>
<field name="title">A Novel Biological Activity of Praziquantel Requiring Voltage-Operated Ca2+ Channel β Subunits: Subversion of Flatworm Regenerative Polarity</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">6</field>
<field name="pages">e464</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Approximately 200 million people worldwide harbour parasitic flatworm
infections that cause schistosomiasis. A single drug—praziquantel (PZQ)—has
served as the mainstay pharmacotherapy for schistosome infections since the
1980s. However, the relevant in vivo target(s) of praziquantel remain
undefined.

Methods and Findings

Here, we provide fresh perspective on the molecular basis of praziquantel
efficacy in vivo consequent to the discovery of a remarkable action of PZQ on
regeneration in a species of free-living flatworm (Dugesia japonica).
Specifically, PZQ caused a robust (100% penetrance) and complete duplication
of the entire anterior-posterior axis during flatworm regeneration to yield
two-headed organisms with duplicated, integrated central nervous and organ
systems. Exploiting this phenotype as a readout for proteins impacting
praziquantel efficacy, we demonstrate that PZQ-evoked bipolarity was
selectively ablated by in vivo RNAi of voltage-operated calcium channel (VOCC)
β subunits, but not by knockdown of a VOCC α subunit. At higher doses of PZQ,
knockdown of VOCC β subunits also conferred resistance to PZQ in lethality
assays.

Conclusions

This study identifies a new biological activity of the antischistosomal drug
praziquantel on regenerative polarity in a species of free-living flatworm.
Ablation of the bipolar regenerative phenotype evoked by PZQ via in vivo RNAi
of VOCC β subunits provides the first genetic evidence implicating a molecular
target crucial for in vivo PZQ activity and supports the ‘VOCC hypothesis’ of
PZQ efficacy. Further, in terms of regenerative biology and Ca2+ signaling,
these data highlight a novel role for voltage-operated Ca2+ entry in
regulating in vivo stem cell differentiation and regenerative patterning.

Author Summary

Praziquantel is the major drug used to treat people infected with parasitic
worms that cause the neglected tropical disease schistosomiasis. Despite being
in widespread clinical use, it is surprising that scientists have not
identified how praziquantel works to kill pathogenic schistosomes. This lack
of pathobiological insight is a major roadblock to the directed design of new
drugs to treat schistosomiasis, as the relevant in vivo target
molecule/pathway of praziquantel remains undefined. In this report, we have
discovered a new biological activity of praziquantel that enables us to bring
a unique chemical genetic perspective to the problem of identifying molecules
needed for in vivo praziquantel efficacy. Specifically, we show that
praziquantel miscues regenerative patterning in a species of free-living
flatworm to yield bipolar (two-headed) organisms. By using this phenotype to
screen for molecules underpinning this activity, we provide in vivo support
for the ‘Ca2+ channel hypothesis’ of PZQ efficacy, and show that manipulation
of specific subunits of voltage-gated Ca2+ channels prevent this effect, and
lessen praziquantel-mediated toxicity. These data provide further impetus to
studying the role of these proteins in schistosome pharmacotherapy.

Introduction

Flatworms (‘platyhelminths’) comprise a diverse grouping of ∼25,000 species
representing some of the simplest organisms that are triploblastic and
bilaterally symmetric. The major</field></doc>
</add>
