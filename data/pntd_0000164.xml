<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000164</field>
<field name="doi">10.1371/journal.pntd.0000164</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Philip Bejon, Tabitha W. Mwangi, Brett Lowe, Norbert Peshu, Adrian V. S. Hill, Kevin Marsh</field>
<field name="title">Helminth Infection and Eosinophilia and the Risk of Plasmodium falciparum Malaria in 1- to 6-Year-Old Children in a Malaria Endemic Area</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">2</field>
<field name="pages">e164</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Helminth infection is common in malaria endemic areas, and an interaction
between the two would be of considerable public health importance. Animal
models suggest that helminth infections may increase susceptibility to
malaria, but epidemiological data has been limited and contradictory.

Methodology/Principal Findings

In a vaccine trial, we studied 387 one- to six-year-old children for the
effect of helminth infections on febrile Plasmodium falciparum malaria
episodes. Gastrointestinal helminth infection and eosinophilia were prevalent
(25% and 50% respectively), but did not influence susceptibility to malaria.
Hazard ratios were 1 for gastrointestinal helminth infection (95% CI 0.6–1.6)
and 0.85 and 0.85 for mild and marked eosinophilia, respectively (95% CI
0.56–1.76 and 0.69–1.96). Incident rate ratios for multiple episodes were 0.83
for gastro-intestinal helminth infection (95% CI 0.5–1.33) and 0.86 and 0.98
for mild and marked eosinophilia (95% CI 0.5–1.4 and 0.6–1.5).

Conclusions/Significance

There was no evidence that infection with gastrointestinal helminths or
urinary schistosomiasis increased susceptibility to Plasmodium falciparum
malaria in this study. Larger studies including populations with a greater
prevalence of helminth infection should be undertaken.

Author Summary

Malaria infection and other parasitic infections are widespread in developing
countries. There is evidence from some studies that intestinal worm infections
may increase the risk of developing febrile malaria. However, the evidence is
mixed, and some studies have found no effect or even protective effects. A
vaccine trial was recently conducted to assess the efficacy of a candidate
malaria vaccine. Episodes of malaria were monitored. The vaccine was not
protective, but data was also recorded on the prevalence of worm infections.
The rates of febrile malaria did not seem to vary according to worm infection
in this study. However, because of the relatively low prevalence of worm
infection, the study did not have high power. Given the conflicting findings
in the literature, and the potential for the effect of worm infection to vary
geographically, it is important that larger, definitive studies are conducted,
since even quite small effects might be important for global public health.

Introduction

Helminth and malaria endemic areas frequently coincide, and an interaction
between the two would be of considerable public health importance [1],[2].
Animal models suggest that helminth infections may alter susceptibility to
malaria, although the results are conflicting [3],[4]. In studies conducted
among malaria endemic populations in Africa, helminths have been reported to
increase susceptibility to clinical malaria [5],[6], reduce the risk [7] or
make no difference [8]. Studies in Thailand suggest helminths might increase
the risk of non-severe malaria, but reduce the risk of cerebral malaria
[9],[10].

The Th2 cytokine milieu induced by helminth infection is thought to drive the
antibody response of malaria co-infected individuals towards the production of
non-cytophilic subclasses (IgG2, IgG4, and IgM), whereas protection against
malaria is associated with the presence of the IgG1 and IgG3 cytophilic
s</field></doc>
</add>
