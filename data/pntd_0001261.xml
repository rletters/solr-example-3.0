<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001261</field>
<field name="doi">10.1371/journal.pntd.0001261</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Michelle L. Michalski, Kathryn G. Griffiths, Steven A. Williams, Ray M. Kaplan, Andrew R. Moorhead</field>
<field name="title">The NIH-NIAID Filariasis Research Reagent Resource Center</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">11</field>
<field name="pages">e1261</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Filarial worms cause a variety of tropical diseases in humans; however, they
are difficult to study because they have complex life cycles that require
arthropod intermediate hosts and mammalian definitive hosts. Research efforts
in industrialized countries are further complicated by the fact that some
filarial nematodes that cause disease in humans are restricted in host
specificity to humans alone. This potentially makes the commitment to research
difficult, expensive, and restrictive. Over 40 years ago, the United States
National Institutes of Health–National Institute of Allergy and Infectious
Diseases (NIH-NIAID) established a resource from which investigators could
obtain various filarial parasite species and life cycle stages without having
to expend the effort and funds necessary to maintain the entire life cycles in
their own laboratories. This centralized resource (The Filariasis Research
Reagent Resource Center, or FR3) translated into cost savings to both NIH-
NIAID and to principal investigators by freeing up personnel costs on grants
and allowing investigators to divert more funds to targeted research goals.
Many investigators, especially those new to the field of tropical medicine,
are unaware of the scope of materials and support provided by the FR3. This
review is intended to provide a short history of the contract, brief
descriptions of the fiilarial species and molecular resources provided, and an
estimate of the impact the resource has had on the research community, and
describes some new additions and potential benefits the resource center might
have for the ever-changing research interests of investigators.

History of the FR3 and Its Parasite Strains

The United States National Institutes of Health–National Institute of Allergy
and Infectious Diseases (NIH-NIAID) Filariasis Research Reagent Resource
Center (FR3) started in 1969 when Dr. Paul Thompson, professor and later head
of the Department of Parasitology at the University of Georgia College of
Veterinary Medicine (Athens, Georgia), obtained an NIH contract to establish
the Filariasis Chemotherapy and Repository Research Services. Thompson, the
former head of the Antiparasitic Drug Division at Parke Davis Corporation in
Ann Arbor, Michigan, specialized in antimalarial drug testing using Plasmodium
berghei as a model. He initially obtained a US Army contract to perform
antimalarial drug screening, and concurrently procured an NIH grant to study
immune mechanisms and immunizing agents in filariasis and NIH contract funds
to establish a filariasis repository that would function to supply worms for
his filariasis chemotherapy studies and for other filariasis researchers. In
1969, Drs. Hyong-Sun Ah and John Hibbard joined Thompson&apos;s filariasis
immunology and repository projects. The following year, he hired entomologist
Dr. John W. McCall to run the repository, because McCall had extensive
experience in maintaining various mosquito species for malaria studies. Later
that year, Dr. Tom Klei joined the filariasis program as an NIH postdoctoral
fellow. At that time, the repository maintained two filarial species:
Litomosoides sigmodontis (then L. carinii), vectored by the tropical rat mite
Ornithonyssus bacoti; and Acanthocheilonema (then Dipetalonema) viteae,
vectored by the argasid tick Ornithodo</field></doc>
</add>
