<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001656</field>
<field name="doi">10.1371/journal.pntd.0001656</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Cristian Valencia, Jorge Arévalo, Jean Claude Dujardin, Alejandro Llanos-Cuentas, François Chappuis, Mirko Zimic</field>
<field name="title">Prediction Score for Antimony Treatment Failure in Patients with Ulcerative Leishmaniasis Lesions</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">6</field>
<field name="pages">e1656</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Increased rates for failure in leishmaniasis antimony treatment have been
recently recognized worldwide. Although several risk factors have been
identified there is no clinical score to predict antimony therapy failure of
cutaneous leishmaniasis.

Methods

A case control study was conducted in Peru from 2001 to 2004. 171 patients
were treated with pentavalent antimony and followed up to at least 6 months to
determine cure or failure. Only patients with ulcerative cutaneous
leishmaniasis (N = 87) were considered for data analysis. Epidemiological,
demographical, clinical and laboratory data were analyzed to identify risk
factors for treatment failure. Two prognostic scores for antimonial treatment
failure were tested for sensitivity and specificity to predict antimony
therapy failure by comparison with treatment outcome.

Results

Among 87 antimony-treated patients, 18 (21%) failed the treatment and 69 (79%)
were cured. A novel risk factor for treatment failure was identified: presence
of concomitant distant lesions. Patients presenting concomitant-distant
lesions showed a 30.5-fold increase in the risk of treatment failure compared
to other patients. The best prognostic score for antimonial treatment failure
showed a sensitivity of 77.78% and specificity of 95.52% to predict antimony
therapy failure.

Conclusions

A prognostic score including a novel risk factor was able to predict
antimonial treatment failure in cutaneous leishmaniasis with high specificity
and sensitivity. This prognostic score presents practical advantages as it
relies on clinical and epidemiological characteristics, easily obtained by
physicians or health workers, and makes it a promising clinical tool that
needs to be validated before their use for developing countries.

Author Summary

The manuscript is relevant because of the finding of a new risk factor for
chemotherapy failure and the development of a prognosis score for cutaneous
leishmaniasis. The proportion of patients that have multiple lesions in
American Tegumentary Leishmaniasis (ATL) is considerable. Publications and our
experience permit to estimate that they represent around 20% of the affected
population from the Amazon basin with cutaneous lesions. In addition, about
1/3 of them would correspond to the concomitant distant lesions category, the
novel risk factor identified with a very high odds ratio (20–30) associated.
Such numbers merit study of concomitant distant ulcers category on its own,
not only because of clinical management implications, but also to search for
factors that are contributing to chemotherapy failure. Finally, the simple
equation proposed in the manuscript can be easily adapted to smart phone
technologies. Similar prognosis equations are scarce for other pathologies and
do not exist for Cutaneous Leishmaniasis at all. The simplicity of this tool
should be followed by subsequent epidemiologic studies in other ATL endemic
regions.

Introduction

Leishmaniasis is caused by protozoan parasites of the genus Leishmania sp.
There is an estimated 2 million new cases and almost 70 000 attributable
deaths worldwide every year [1]. Leishmaniasis includes a cluster of diseases
with widely diverse clinical manifestations. These include three major groups
of</field></doc>
</add>
