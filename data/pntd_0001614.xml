<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001614</field>
<field name="doi">10.1371/journal.pntd.0001614</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Kay M. Tomashek, Christopher J. Gregory, Aidsa Rivera Sánchez, Matthew A. Bartek, Enid J. Garcia Rivera, Elizabeth Hunsperger, Jorge L. Muñoz-Jordán, Wellington Sun</field>
<field name="title">Dengue Deaths in Puerto Rico: Lessons Learned from the 2007 Epidemic</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">4</field>
<field name="pages">e1614</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The incidence and severity of dengue in Latin America has increased
substantially in recent decades and data from Puerto Rico suggests an increase
in severe cases. Successful clinical management of severe dengue requires
early recognition and supportive care.

Methods

Fatal cases were identified among suspected dengue cases reported to two
disease surveillance systems and from death certificates. To be included,
fatal cases had to have specimen submitted for dengue diagnostic testing
including nucleic acid amplification for dengue virus (DENV) in serum or
tissue, immunohistochemical testing of tissue, and immunoassay detection of
anti-DENV IgM from serum. Medical records from laboratory-positive dengue
fatal case-patients were reviewed to identify possible determinants for death.

Results

Among 10,576 reported dengue cases, 40 suspect fatal cases were identified, of
which 11 were laboratory-positive, 14 were laboratory-negative, and 15
laboratory-indeterminate. The median age of laboratory-positive case-patients
was 26 years (range 5 months to 78 years), including five children aged &lt;15
years; 7 sought medical care at least once prior to hospital admission, 9 were
admitted to hospital and 2 died upon arrival. The nine hospitalized case-
patients stayed a mean of 15 hours (range: 3–48 hours) in the emergency
department (ED) before inpatient admission. Five of the nine case-patients
received intravenous methylprednisolone and four received non-isotonic saline
while in shock. Eight case-patients died in the hospital; five had their
terminal event on the inpatient ward and six died during a weekend. Dengue was
listed on the death certificate in only 5 instances.

Conclusions

During a dengue epidemic in an endemic area, none of the 11 laboratory-
positive case-patients who died were managed according to current WHO
Guidelines. Management issues identified in this case-series included failure
to recognize warning signs for severe dengue and shock, prolonged ED stays,
and infrequent patient monitoring.

Author Summary

Dengue is a major public health problem in the tropics and subtropics; an
estimated 50 million cases occur annually and 40 percent of the world&apos;s
population lives in areas with dengue virus (DENV) transmission. Dengue has a
wide range of clinical presentations from an undifferentiated acute febrile
illness, classic dengue fever, to severe dengue (i.e., dengue hemorrhagic
fever or dengue shock syndrome). About 5% of patients develop severe dengue,
which is more common with second or subsequent infections. No vaccines are
available to prevent dengue, and there are no specific antiviral treatments
for patients with dengue. However, early recognition of shock and intensive
supportive therapy can reduce risk of death from ∼10% to less than 1% among
severe dengue cases. Reviewing dengue deaths is one means to identify issues
in clinical management. These findings can be used to develop healthcare
provider education to minimize dengue morbidity and mortality.

Introduction

Dengue is a major public health problem throughout the tropics and subtropics
[1]. During the last decade, both the incidence and severity of dengue in
Central and South America, Mexico, and the Cari</field></doc>
</add>
