<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001585</field>
<field name="doi">10.1371/journal.pntd.0001585</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Tansy Edwards, Jennifer Smith, Hugh J. W. Sturrock, Lucia W. Kur, Anthony Sabasio, Timothy P. Finn, Mounir Lado, Danny Haddad, Jan H. Kolaczinski</field>
<field name="title">Prevalence of Trachoma in Unity State, South Sudan: Results from a Large-Scale Population-Based Survey and Potential Implications for Further Surveys</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">4</field>
<field name="pages">e1585</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Large parts of South Sudan are thought to be trachoma-endemic but baseline
data are limited. This study aimed to estimate prevalence for planning
trachoma interventions in Unity State, to identify risk factors and to
investigate the effect of different sampling approaches on study conclusions.

Methods and Findings

The survey area was defined as one domain of eight counties in Unity State.
Across the area, 40 clusters (villages) were randomly selected proportional to
the county population size in a population-based prevalence survey. The
simplified grading scheme was used to classify clinical signs of trachoma. The
unadjusted prevalence of trachoma inflammation-follicular (TF) in children
aged 1–9 years was 70.5% (95% CI: 68.6–72.3). After adjusting for age, sex,
county and clustering of cases at household and village level the prevalence
was 71.0% (95% CI: 69.9–72.1). The prevalence of trachomatous trichiasis (TT)
in adults was 15.1% (95% CI: 13.4–17.0) and 13.5% (95% CI: 12.0–15.1) before
and after adjustment, respectively. We estimate that 700,000 people (the
entire population of Unity State) require antibiotic treatment and
approximately 54,178 people require TT surgery. Risk factor analyses confirmed
child-level associations with TF and highlighted that older adults living in
poverty are at higher risk of TT. Conditional simulations, testing the
alternatives of sampling 20 or 60 villages over the same area, indicated that
sampling of only 20 villages would have provided an acceptable level of
precision for state-level prevalence estimation to inform intervention
decisions in this hyperendemic setting.

Conclusion

Trachoma poses an enormous burden on the population of Unity State.
Comprehensive control is urgently required to avoid preventable blindness and
should be initiated across the state now. In other parts of South Sudan
suspected to be highly trachoma endemic, counties should be combined into
larger survey areas to generate the baseline data required to initiate
interventions.

Author Summary

Large parts of South Sudan are thought to be trachoma endemic but baseline
data, required to initiate interventions, are few. District-by-district
surveys, currently recommended by the World Health Organization (WHO), are
often not financially or logistically viable. We therefore adapted existing
WHO guidelines and combined eight counties (equivalent to districts) of Unity
State into one survey area, randomly sampling 40 villages using a population-
based survey design. This decision was based on a trachoma risk map and a
trachoma rapid assessment, both identifying the state as likely to be highly
endemic. The survey confirmed trachoma as being hyperendemic throughout Unity
State, meaning that large-scale intervention should be initiated now.
Simulation studies were conducted to determine the likely outcome if fewer (n
= 20) or more (n = 60) villages had been sampled, confirming that precision
decreased or increased, respectively. Importantly, simulation results also
showed that all three sample sizes would have led to the same conclusion,
namely the need for large-scale intervention. This finding suggests th</field></doc>
</add>
