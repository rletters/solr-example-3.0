<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000583</field>
<field name="doi">10.1371/journal.pntd.0000583</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Mathieu Hägi, Jean-François Schémann, Frédéric Mauny, Germain Momo, Doulaye Sacko, Lamine Traoré, Denis Malvy, Jean-François Viel</field>
<field name="title">Active Trachoma among Children in Mali: Clustering and Environmental Risk Factors</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">1</field>
<field name="pages">e583</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Active trachoma is not uniformly distributed in endemic areas, and local
environmental factors influencing its prevalence are not yet adequately
understood. Determining whether clustering is a consistent phenomenon may help
predict likely modes of transmission and help to determine the appropriate
level at which to target control interventions. The aims of this study were,
therefore, to disentangle the relative importance of clustering at different
levels and to assess the respective role of individual, socio-demographic, and
environmental factors on active trachoma prevalence among children in Mali.

Methodology/Principal Findings

We used anonymous data collected during the Mali national trachoma survey
(1996–1997) at different levels of the traditional social structure (14,627
children under 10 years of age, 6,251 caretakers, 2,269 households, 203
villages). Besides field-collected data, environmental variables were
retrieved later from various databases at the village level. Bayesian
hierarchical logistic models were fit to these prevalence and exposure data.
Clustering revealed significant results at four hierarchical levels. The
higher proportion of the variation in the occurrence of active trachoma was
attributable to the village level (36.7%), followed by household (25.3%), and
child (24.7%) levels. Beyond some well-established individual risk factors
(age between 3 and 5, dirty face, and flies on the face), we showed that
caretaker-level (wiping after body washing), household-level (common ownership
of radio, and motorbike), and village-level (presence of a women&apos;s
association, average monthly maximal temperature and sunshine fraction,
average annual mean temperature, presence of rainy days) features were
associated with reduced active trachoma prevalence.

Conclusions/Significance

This study clearly indicates the importance of directing control efforts both
at children with active trachoma as well as those with close contact, and at
communities. The results support facial cleanliness and environmental
improvements (the SAFE strategy) as population-health initiatives to combat
blinding trachoma.

Author Summary

Active trachoma is not uniformly distributed in endemic areas, and local
environmental factors influencing its prevalence are not yet adequately
understood. Determining whether clustering is a consistent phenomenon may help
predict likely modes of transmission and help to determine the appropriate
level at which to target control interventions. In this work, we estimated the
magnitude of clustering at different levels and investigated the influence of
socio-economic factors and environmental features on active trachoma
prevalence among children in Mali (1996–1997 nationwide survey). Clustering
revealed significant results at the child, caretaker, household, and village
levels. Moreover, beyond some well-established individual risk factors (age
between 3 and 5, dirty face, and flies on the face), we found that
temperature, sunshine fraction, and presence of rainy days were negatively
associated with active trachoma prevalence. This study clearly indicates the
importance of directing control efforts both at children with active trachoma
as well as those with close contact, and at</field></doc>
</add>
