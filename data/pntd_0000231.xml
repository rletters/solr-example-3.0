<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000231</field>
<field name="doi">10.1371/journal.pntd.0000231</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Elizabeth A. Misch, Murdo Macdonald, Chaman Ranjit, Bishwa R. Sapkota, Richard D. Wells, M. Ruby Siddiqui, Gilla Kaplan, Thomas R. Hawn</field>
<field name="title">Human TLR1 Deficiency Is Associated with Impaired Mycobacterial Signaling and Protection from Leprosy Reversal Reaction</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">5</field>
<field name="pages">e231</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Toll-like receptors (TLRs) are important regulators of the innate immune
response to pathogens, including Mycobacterium leprae, which is recognized by
TLR1/2 heterodimers. We previously identified a transmembrane domain
polymorphism, TLR1_T1805G, that encodes an isoleucine to serine substitution
and is associated with impaired signaling. We hypothesized that this TLR1 SNP
regulates the innate immune response and susceptibility to leprosy. In HEK293
cells transfected with the 1805T or 1805G variant and stimulated with extracts
of M. leprae, NF-κB activity was impaired in cells with the 1805G
polymorphism. We next stimulated PBMCs from individuals with different
genotypes for this SNP and found that 1805GG individuals had significantly
reduced cytokine responses to both whole irradiated M. leprae and cell wall
extracts. To investigate whether TLR1 variation is associated with clinical
presentations of leprosy or leprosy immune reactions, we examined 933 Nepalese
leprosy patients, including 238 with reversal reaction (RR), an immune
reaction characterized by a Th1 T cell cytokine response. We found that the
1805G allele was associated with protection from RR with an odds ratio (OR) of
0.51 (95% CI 0.29–0.87, p = 0.01). Individuals with 1805 genotypes GG or TG
also had a reduced risk of RR in comparison to genotype TT with an OR of 0.55
(95% CI 0.31–0.97, p = 0.04). To our knowledge, this is the first association
of TLR1 with a Th1-mediated immune response. Our findings suggest that TLR1
deficiency influences adaptive immunity during leprosy infection to affect
clinical manifestations such as nerve damage and disability.

Author Summary

Mycobacterium leprae (ML) causes a disabling and stigmatizing disease that is
characterized by distinct immune responses. ML produces a spectrum of illness
in humans, and several lines of evidence indicate that host genetic factors
influence susceptibility and clinical manifestations. Leprosy can occur as the
lepromatous or tuberculoid forms, which are associated with different clinical
manifestations, histopathology, T cell cytokine profiles, and bacterial burden
in affected sites. Leprosy is also associated with unique immunologic
reactions, such as reversal reaction, which is characterized by the rapid
development of a Th1 T cell cytokine response that can cause substantial
morbidity. We and others recently discovered a common human polymorphism in
TLR1 (T1805G, I602S) that regulates cytokine production in response to
lipopeptide stimulation, influences the cellular innate immune response to
Mycobacteria, is associated with altered localization, and is present in 50%
of individuals worldwide. Here, we show that in humans the 1805G variant does
not mediate an inflammatory response to ML in vitro and that this polymorphism
is associated with protection from reversal reaction. These data suggest that
a common variant of TLR1 is associated with altered adaptive immune responses
to ML as well as clinical outcome.

Introduction

Mycobacterium tuberculosis (MTb) has established latent infection in one-third
of the world&apos;s population and, among those with progressive disease, causes
about 2 million deaths per year [1]. Mycobacterium lepr</field></doc>
</add>
