<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000425</field>
<field name="doi">10.1371/journal.pntd.0000425</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Carlos Franco-Paredes, Jesse T. Jacob, Barbara Stryjewska, Leo Yoder</field>
<field name="title">Two Patients with Leprosy and the Sudden Appearance of Inflammation in the Skin and New Sensory Loss</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">9</field>
<field name="pages">e425</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Leprosy is a chronic infection caused by Mycobacterium leprae that affects the
peripheral nerves, skin, and potentially other organs [1]–[5]. Although the
worldwide prevalence of leprosy has decreased in the era of multi-drug therapy
(MDT), the global detection of new cases of leprosy remains a concern, with
more than 250,000 new cases of leprosy reported in 2007 [3]. The precise
mechanism of transmission of leprosy has not been conclusively defined;
however, it is likely that this occurs through respiratory secretions by
untreated borderline lepromatous and polar lepromatous cases [1],[5]. Target
cells of infection are macrophages, histiocytes in the skin, and the
nonmyelinating and myelinating Schwann cells in the peripheral nerve, leading
to axonal dysfunction and demyelination [6]. Nerve injury plays a central role
in the pathogenesis of leprosy, leading to functional impairment and deformity
of hands and feet and the eyes [1],[6]. Leprosy is diagnosed by definite loss
of sensation in a hypopigmented or reddish skin patch, a thickened peripheral
nerve with loss of sensation and muscle weakness in the affected nerve, and
presence of acid-fast bacilli on skin smear or biopsy [1],[4].

The immunological response to M. leprae mounted by the host will determine the
different potential clinical states. The Ridley-Joplin system uses clinical
and histopathological features and the bacteriologic index and includes the
polar categories (lepromatous [LL] and tuberculoid [TT]) and the borderline
states (borderline tuberculoid [BT], borderline borderline [BB], and
borderline lepromatous [BL]) [1] (Table 1). In the polar tuberculoid category,
a Th1 type cell–mediated immune response with a low bacterial load is seen.
Lepromatous states are characterized by low cell-mediated immunity and a
higher bacterial load [5] (Table 1). Clinically, patients with tuberculoid
leprosy have a single or very few hypopigmented macules or plaques with a
raised edge; they are dry, scaly, hairless, and have reduced sensation; and
only a few peripheral nerves are commonly enlarged [1]. Lepromatous leprosy is
characterized by widely and symmetrically distributed skin macules, nodules,
erythematous papules, and diffuse skin infiltration; thickened peripheral
nerves are more frequently identified. Borderline states represent a mixture
of signs and symptoms of the polar categories [1].

Table 1

World Health Organization System and Ridley-Joplin Classification and Type of
Reaction.

WHO

Paucibacillary

Multibacillary

Ridley-Joplina

TT

BT

BB

BLb

LL

Type 1 Reaction

No

Yes

Yes

Yes

No

Type 2 Reaction

No

No

No

Yes

Yes

{ label (or @symbol) needed for fn[@id=&apos;nt101&apos;] } WHO classification is used
for operational purposes in the field and it is based on the number of skin
lesions; Ridley-Joplin classification is an immunopathological and clinical
classification.

{ label (or @symbol) needed for fn[@id=&apos;nt102&apos;] } Adapted from [5].

a Ridley-Joplin classification: tuberculoid (TT); borderline tuberculoid (BT);
borderline borderline (BB); borderline lepromatous (BL); and lepromatous (LL).

b Patients with BL can develop both types of reactions.

Description of Case A

A 31-year-old Brazilian male living in the United States for the previous four
years presented with progressiv</field></doc>
</add>
