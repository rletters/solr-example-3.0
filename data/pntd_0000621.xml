<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000621</field>
<field name="doi">10.1371/journal.pntd.0000621</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Sharon M. Tennant, Souleymane Diallo, Haim Levy, Sofie Livio, Samba O. Sow, Milagritos Tapia, Patricia I. Fields, Matthew Mikoleit, Boubou Tamboura, Karen L. Kotloff, James P. Nataro, James E. Galen, Myron M. Levine</field>
<field name="title">Identification by PCR of Non-typhoidal Salmonella enterica Serovars Associated with Invasive Infections among Febrile Patients in Mali</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">3</field>
<field name="pages">e621</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

In sub-Saharan Africa, non-typhoidal Salmonella (NTS) are emerging as a
prominent cause of invasive disease (bacteremia and focal infections such as
meningitis) in infants and young children. Importantly, including data from
Mali, three serovars, Salmonella enterica serovar Typhimurium, Salmonella
Enteritidis and Salmonella Dublin, account for the majority of non-typhoidal
Salmonella isolated from these patients.

Methods

We have extended a previously developed series of polymerase chain reactions
(PCRs) based on O serogrouping and H typing to identify Salmonella Typhimurium
and variants (mostly I 4,[5],12:i:-), Salmonella Enteritidis and Salmonella
Dublin. We also designed primers to detect Salmonella Stanleyville, a serovar
found in West Africa. Another PCR was used to differentiate diphasic
Salmonella Typhimurium and monophasic Salmonella Typhimurium from other O
serogroup B, H:i serovars. We used these PCRs to blind-test 327 Salmonella
serogroup B and D isolates that were obtained from the blood cultures of
febrile patients in Bamako, Mali.

Principal Findings

We have shown that when used in conjunction with our previously described
O-serogrouping PCR, our PCRs are 100% sensitive and specific in identifying
Salmonella Typhimurium and variants, Salmonella Enteritidis, Salmonella Dublin
and Salmonella Stanleyville. When we attempted to differentiate 171 Salmonella
Typhimurium (I 4,[ 5],12:i:1,2) strains from 52 monophasic Salmonella
Typhimurium (I 4,[5],12:i:-) strains, we were able to correctly identify 170
of the Salmonella Typhimurium and 51 of the Salmonella I 4,[5],12:i:- strains.

Conclusion

We have described a simple yet effective PCR method to support surveillance of
the incidence of invasive disease caused by NTS in developing countries.

Author Summary

The genus Salmonella has more than 2500 serological variants (serovars), such
as Salmonella enterica serovar Typhi and Salmonella Paratyphi A and B, that
cause, respectively, typhoid and paratyphoid fevers (enteric fevers), and a
large number of non-typhoidal Salmonella (NTS) serovars that cause
gastroenteritis in healthy hosts. In young infants, the elderly and
immunocompromised hosts, NTS can cause severe, fatal invasive disease.
Multiple studies of pediatric patients in sub-Saharan Africa have documented
the important role of NTS, in particular Salmonella Typhimurium and Salmonella
Enteritidis (and to a lesser degree Salmonella Dublin), as invasive bacterial
pathogens. Salmonella spp. are isolated from blood and identified by standard
microbiological techniques and the serovar is ascertained by agglutination
with commercial antisera. PCR-based typing techniques are becoming
increasingly popular in developing countries, in part because high quality
typing sera are difficult to obtain and expensive and H serotyping is
technically difficult. We have developed a series of polymerase chain
reactions (PCRs) to identify Salmonella Typhimurium and variants, Salmonella
Enteritidis and Salmonella Dublin. We successfully identified 327 Salmonella
isolates using our multiplex PCR. We also designed primers to detect
Salm</field></doc>
</add>
