<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001277</field>
<field name="doi">10.1371/journal.pntd.0001277</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Vera Lúcia Teixeira de Freitas, Sheila Cristina Vicente da Silva, Ana Marli Sartori, Rita Cristina Bezerra, Elizabeth Visone Nunes Westphalen, Tatiane Decaris Molina, Antonio R. L. Teixeira, Karim Yaqub Ibrahim, Maria Aparecida Shikanai-Yasuda</field>
<field name="title">Real-Time PCR in HIV/Trypanosoma cruzi Coinfection with and without Chagas Disease Reactivation: Association with HIV Viral Load and CD4+ Level</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">8</field>
<field name="pages">e1277</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Reactivation of chronic Chagas disease, which occurs in approximately 20% of
patients coinfected with HIV/Trypanosoma cruzi (T. cruzi), is commonly
characterized by severe meningoencephalitis and myocarditis. The use of
quantitative molecular tests to monitor Chagas disease reactivation was
analyzed.

Methodology

Polymerase chain reaction (PCR) of kDNA sequences, competitive (C-) PCR and
real-time quantitative (q) PCR were compared with blood cultures and
xenodiagnosis in samples from 91 patients (57 patients with chronic Chagas
disease and 34 with HIV/T. cruzi coinfection), of whom 5 had reactivation of
Chagas disease and 29 did not.

Principal Findings

qRT-PCR showed significant differences between groups; the highest parasitemia
was observed in patients infected with HIV/T. cruzi with Chagas disease
reactivation (median 1428.90 T. cruzi/mL), followed by patients with HIV/T.
cruzi infection without reactivation (median 1.57 T. cruzi/mL) and patients
with Chagas disease without HIV (median 0.00 T. cruzi/mL). Spearman&apos;s
correlation coefficient showed that xenodiagnosis was correlated with blood
culture, C-PCR and qRT-PCR. A stronger Spearman correlation index was found
between C-PCR and qRT-PCR, the number of parasites and the HIV viral load,
expressed as the number of CD4+ cells or the CD4+/CD8+ ratio.

Conclusions

qRT-PCR distinguished the groups of HIV/T. cruzi coinfected patients with and
without reactivation. Therefore, this new method of qRT-PCR is proposed as a
tool for prospective studies to analyze the importance of parasitemia
(persistent and/or increased) as a criterion for recommending pre-emptive
therapy in patients with chronic Chagas disease with HIV infection or
immunosuppression. As seen in this study, an increase in HIV viral load and
decreases in the number of CD4+ cells/mm3 and the CD4+/CD8+ ratio were
identified as cofactors for increased parasitemia that can be used to target
the introduction of early, pre-emptive therapy.

Author Summary

Chagas disease is endemic in Latin America and is caused by the flagellate
protozoan T. cruzi. The acute phase is asymptomatic in the majority of the
cases and rarely causes inflammation of the heart or the central nervous
system. Most infected patients progress to a chronic phase, characterized by
cardiac or digestive involvement when not asymptomatic. However, when patients
are also exposed to an immunosuppressant (such as chemotherapy), neoplasia, or
other infections such as HIV, T. cruzi infection may develop into a severe
disease (Chagas disease reactivation) involving the heart and central nervous
system. The current microscopic methods for diagnosing Chagas disease
reactivation are not sensitive enough to prevent the high rate of death
observed in these cases. Therefore, we propose a quantitative method to
monitor blood levels of the parasite, which will allow therapy to be
administered as early as possible, even if the patient has not yet presented
symptoms.

Introduction

Chagas disease is endemic in Latin America, where fewer than 8 million people,
many of whom </field></doc>
</add>
