<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001380</field>
<field name="doi">10.1371/journal.pntd.0001380</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Sonia Pelletreau, Mawuli Nyaku, Massitan Dembele, Boubacar Sarr, Philip Budge, Rachael Ross, Els Mathieu</field>
<field name="title">The Field-Testing of a Novel Integrated Mapping Protocol for Neglected Tropical Diseases</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">11</field>
<field name="pages">e1380</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Vertical control and elimination programs focused on specific neglected
tropical diseases (NTDs) can achieve notable success by reducing the
prevalence and intensity of infection. However, many NTD-endemic countries
have not been able to launch or scale-up programs because they lack the
necessary baseline data for planning and advocacy. Each NTD program has its
own mapping guidelines to collect missing data. Where geographic overlap among
NTDs exists, an integrated mapping approach could result in significant
resource savings. We developed and field-tested an innovative integrated NTD
mapping protocol (Integrated Threshold Mapping (ITM) Methodology) for
lymphatic filariasis (LF), trachoma, schistosomiasis and soil-transmitted
helminths (STH).

Methodology/Principal Findings

The protocol is designed to be resource-efficient, and its specific purpose is
to determine whether a threshold to trigger public health interventions in an
implementation unit has been attained. The protocol relies on World Health
Organization (WHO) recommended indicators in the disease-specific age groups.
For each disease, the sampling frame was the district, but for
schistosomiasis, the sub-district rather than the ecological zone was used. We
tested the protocol by comparing it to current WHO mapping methodologies for
each of the targeted diseases in one district each in Mali and Senegal.
Results were compared in terms of public health intervention, and feasibility,
including cost. In this study, the ITM methodology reached the same
conclusions as the WHO methodologies regarding the initiation of public health
interventions for trachoma, LF and STH, but resulted in more targeted
intervention recommendations for schistosomiasis. ITM was practical, feasible
and demonstrated an overall cost saving compared with the standard, non-
integrated, WHO methodologies.

Conclusions/Significance

This integrated mapping tool could facilitate the implementation of much-
needed programs in endemic countries.

Author Summary

Neglected tropical diseases (NTDs) cause significant physical debilitation,
lowered economic productivity, and social ostracism for afflicted individuals.
Five NTDs with available preventive chemotherapy: lymphatic filariasis (LF),
trachoma, schistosomiasis, onchocerciasis and the three soil-transmitted
helminths (STH); have been targeted for control or elimination, but resource
constraints in endemic countries have impeded progress toward these goals. We
have developed an integrated mapping protocol, Integrated Threshold Mapping
(ITM) for use by Ministries of Health to decide where public health
interventions for NTDs are needed. We compared this protocol to the World
Health Organizations disease-specific mapping protocols in Mali and Senegal.
Results from both methodologies indicated the same public health interventions
for trachoma, LF and STH, while the ITM methodology resulted in a more
targeted intervention for schistosomiasis. Our study suggests that the
integrated methodology, which is also less expensive and logistically more
feasible to implement, could replace disease-specific mapping protocols in
resource-poor NTD-endemic countries.

Introduction

Neglected tropical diseases (NTDs) are parasitic and bacterial diseases that
affect an </field></doc>
</add>
