<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000442</field>
<field name="doi">10.1371/journal.pntd.0000442</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">A. Desiree LaBeaud, Indu Malhotra, Maria J. King, Christopher L. King, Charles H. King</field>
<field name="title">Do Antenatal Parasite Infections Devalue Childhood Vaccination?</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">5</field>
<field name="pages">e442</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

On a global basis, both potent vaccine efficacy and high vaccine coverage are
necessary to control and eliminate vaccine-preventable diseases. Emerging
evidence from animal and human studies suggest that neglected tropical
diseases (NTDs) significantly impair response to standard childhood
immunizations. A review of efficacy and effectiveness studies of vaccination
among individuals with chronic parasitic infections was conducted, using
PUBMED database searches and analysis of data from the authors&apos; published and
unpublished studies. Both animal models and human studies suggest that chronic
trematode, nematode, and protozoan infections can result in decreased vaccine
efficacy. Among pregnant women, who in developing countries are often infected
with multiple parasites, soluble parasite antigens have been shown to cross
the placenta and prime or tolerize fetal immune responses. As a result,
antenatal infections can have a significant impact on later vaccine responses.
Acquired childhood parasitic infections, most commonly malaria, can also
affect subsequent immune response to vaccination. Additional data suggest that
antiparasite therapy can improve the effectiveness of several human vaccines.
Emerging evidence demonstrates that both antenatal and childhood parasitic
infections alter levels of protective immune response to routine vaccinations.
Successful antiparasite treatment may prevent immunomodulation caused by
parasitic antigens during pregnancy and early childhood and may improve
vaccine efficacy. Future research should highlight the varied effects that
different parasites (alone and in combination) can have on human vaccine-
related immunity. To optimize vaccine effectiveness in developing countries,
better control of chronic NTDs may prove imperative.

Introduction

Since the inception of the Expanded Program on Immunization (EPI) in 1974,
many global partners, including the World Health Organization, United Nations
Children&apos;s Fund, and the Gates Foundation, have joined to support mass global
immunization projects that have resulted in a significant drop in child
mortality worldwide [1]. Programmatic effectiveness has been primarily
measured as the operational improvement in vaccine coverage, with the tacit
assumption that average individual vaccine response (i.e., average vaccine
efficacy) remains the same for all populations [2],[3]. For example, the
Global Alliance for Vaccines has improved the percentage of children receiving
diphtheria–tetanus–pertussis (DTP) vaccinations from 71% in 1999 to 78% in
2004 [4]. Despite these impressive attempts at mass vaccination coverage,
vaccine-preventable diseases still kill an estimated 1 to 2 million African
children each year [5]. Whereas efficacy is the measure of the impact of
treatment in an ideal (study) environment, effectiveness is the measure of
impact in “real-world” settings [6]. These preventable deaths contribute to
the high infant and childhood mortality rates experienced by these countries
and, by definition, highlight the lapse in vaccine effectiveness in resource-
poor areas.

Vaccines are among the most cost-effective health interventions available for
the prevention of life-threatening and disabling infectious diseases. Even so,
the overall effectiveness of vaccine strategies requires both adequat</field></doc>
</add>
