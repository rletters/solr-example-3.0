<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000996</field>
<field name="doi">10.1371/journal.pntd.0000996</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Ole Wichmann, In-Kyu Yoon, Sirenda Vong, Kriengsak Limkittikul, Robert V. Gibbons, Mammen P. Mammen, Sowath Ly, Philippe Buchy, Chukiat Sirivichayakul, Rome Buathong, Rekol Huy, G. William Letson, Arunee Sabchareon</field>
<field name="title">Dengue in Thailand and Cambodia: An Assessment of the Degree of Underrecognized Disease Burden Based on Reported Cases</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">3</field>
<field name="pages">e996</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Disease incidence data are needed to guide decision-making for public health
interventions. Although dengue is a reportable disease in Thailand and
Cambodia, the degree that reported incidence underrecognizes true disease
burden is unknown. We utilized dengue incidence calculated from laboratory-
confirmed outpatient and inpatient cases in prospective cohort studies to
estimate the magnitude of dengue underrecognition and to establish more
accurate disease burden estimates for these countries.

Methods and Findings

Cohort studies were conducted among children aged &lt;15 years by members of a
dengue field site consortium over at least 2 dengue seasons. Age-group
specific multiplication factors (MFs) were computed by comparing data from
three cohort studies to national surveillance data in the same province and
year. In Thailand, 14,627 person-years of prospective cohort data were
obtained in two provinces and 14,493 person-years from one province in
Cambodia. Average annual incidence of laboratory-confirmed dengue was 23/1,000
and 25/1,000 in Thailand, and 41/1,000 in Cambodia. Calculated MFs in these
provinces varied by age-group and year (range 0.4–29). Average age-group
specific MFs were then applied to country-level reporting data and indicated
that in Thailand a median 229,886 (range 210,612–331,236) dengue cases
occurred annually during 2003–2007 and a median 111,178 (range 80,452–357,135)
cases occurred in Cambodia in children &lt;15 years of age. Average
underrecognition of total and inpatient dengue cases was 8.7 and 2.6-fold in
Thailand, and 9.1 and 1.4-fold in Cambodia, respectively. During the high-
incidence year 2007, &gt;95,000 children in Thailand and &gt;58,000 children in
Cambodia were estimated to be hospitalized due to dengue.

Conclusion

Calculating MFs by comparing prospective cohort study data to locally-reported
national surveillance data is one approach to more accurately assess disease
burden. These data indicate that although dengue is regularly reported in many
countries, national surveillance data significantly underrecognize the true
burden of disease.

Author Summary

Dengue is a major public health problem especially in tropical and subtropical
countries of Asia and Latin-America. An effective dengue vaccine is not yet
available, but several vaccine candidates are currently being evaluated in
clinical trials. Accurate country-level incidence data are crucial to assess
the cost-effectiveness of such vaccines and will assist policy-makers in
making vaccine introduction decisions. Existing national surveillance systems
are often passive and are designed to monitor trends and to detect disease
outbreaks. Our analyses of data from prospectively followed cohorts with
laboratory confirmation of dengue cases show that, in Thailand and Cambodia,
dengue incidence is underrecognized by more than 8-fold. The magnitude of the
outpatient burden caused by dengue is not assessed or reflected by the
national surveillance data. We estimate that a median of more than 340,000
symptomatic dengue virus infections occurred annually in children less than 15
years of age in </field></doc>
</add>
