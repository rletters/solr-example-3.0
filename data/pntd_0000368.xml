<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000368</field>
<field name="doi">10.1371/journal.pntd.0000368</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Shan Lv, Yi Zhang, He-Xiang Liu, Ling Hu, Kun Yang, Peter Steinmann, Zhao Chen, Li-Ying Wang, Jürg Utzinger, Xiao-Nong Zhou</field>
<field name="title">Invasive Snails and an Emerging Infectious Disease: Results from the First National Survey on Angiostrongylus cantonensis in China</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">2</field>
<field name="pages">e368</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Eosinophilic meningitis (angiostrongyliasis) caused by Angiostrongylus
cantonensis is emerging in mainland China. However, the distribution of A.
cantonensis and its intermediate host snails, and the role of two invasive
snail species in the emergence of angiostrongyliasis, are not well understood.

Methodology/Principal Findings

A national survey pertaining to A. cantonensis was carried out using a grid
sampling approach (spatial resolution: 40×40 km). One village per grid cell
was randomly selected from a 5% random sample of grid cells located in areas
where the presence of the intermediate host snail Pomacea canaliculata had
been predicted based on a degree-day model. Potential intermediate hosts of A.
cantonensis were collected in the field, restaurants, markets and snail farms,
and examined for infection. The infection prevalence among intermediate host
snails was estimated, and the prevalence of A. cantonensis within P.
canaliculata was displayed on a map, and predicted for non-sampled locations.
It was confirmed that P. canaliculata and Achatina fulica were the predominant
intermediate hosts of A. cantonensis in China, and these snails were found to
be well established in 11 and six provinces, respectively. Infected snails of
either species were found in seven provinces, closely matching the endemic
area of A. cantonensis. Infected snails were also found in markets and
restaurants. Two clusters of A. cantonensis–infected P. canaliculata were
predicted in Fujian and Guangxi provinces.

Conclusions/Significance

The first national survey in China revealed a wide distribution of A.
cantonensis and two invasive snail species, indicating that a considerable
number of people are at risk of angiostrongyliasis. Health education, rigorous
food inspection and surveillance are all needed to prevent recurrent
angiostrongyliasis outbreaks.

Author Summary

Eosinophilic meningitis is caused by the rat lungworm (Angiostrongylus
cantonensis). This parasite is endemic in Southeast Asia, Australia, the
Caribbean, and on Pacific Islands. Moreover, the disease is emerging in
mainland China, which might be related to the spread of two invasive snail
species (Achatina fulica and Pomacea canaliculata). Thus far, the biggest
angiostrongyliasis outbreak in China occurred in 2006 in Beijing, involving
160 patients. However, detailed information about the national distribution of
A. cantonensis and its intermediate hosts is still lacking, and the importance
of the two invasive snail species for disease transmission is not well
understood. Therefore, a national survey on the distribution of A. cantonensis
and its intermediate hosts in China was carried out in 2006/2007. It was found
that A. fulica and P. canaliculata were implicated in most angiostrongyliasis
outbreaks, and that the distribution of A. cantonensis closely matched that of
these snails. The two invasive snail species facilitated the expansion of the
parasite, thus probably leading to the emergence of angiostrongyliasis, a
previously rare disease, in mainland China.

Introduction

Eosinophilic meningitis, a potentially fatal disease caused by Angiostrongylus
cantonensis, is considered an emerging infectious disease in mai</field></doc>
</add>
