<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000451</field>
<field name="doi">10.1371/journal.pntd.0000451</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Charles E. Rupprecht</field>
<field name="title">Bats, Emerging Diseases, and the Human Interface</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">7</field>
<field name="pages">e451</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Background

The tragic death of a Dutch patient due to Duvenhage virus infection acquired
after bat exposure in Kenya during 2007 emphasizes the potential dangers
associated with ecotourism, underscores the role of Chiroptera as reservoirs
of emerging infectious diseases, and highlights modern attempts to prevent and
treat these zoonotic diseases.

The patient in this incident, a physician from The Netherlands, was a visitor
to a game park in eastern Kenya, and had not been previously vaccinated
against rabies. Often, many travelers abroad may not be well versed in local
environmental conditions. Well before expected departure, travel medicine
consultants should discuss both generic and country-specific risks with their
clients. For maximum benefit, several biologics require administration a month
in advance of departure, such as rabies pre-exposure immunization. Besides
primary vaccination suggestions, and health insurance considerations for
emergency care abroad or medical evacuation as needed, basic education is
necessary concerning realistic public health concerns, especially in
developing countries. Selective positive social behaviors should be promoted,
especially as related to personal interactions with animals, both domestic and
wild. Greater appreciation of animals from a distance is ideal, rather than
any personal provocations. If animal bites or scratches do occur, immediate
thorough washing of wounds with soap and clean water is valuable, followed by
careful biomedical evaluation. While the latter was attempted in this
particular Kenyan incident, the global implications of bat rabies were not
appreciated.

Unlike the epidemiological situation in Europe and the developed world, rabid
dogs remain responsible for the vast majority of human rabies cases in other
parts of the world. For this reason, less attention is paid to infections
acquired from other mammals, such as wildlife. With the exception of
Antarctica, bat rabies occurs on all continents. The existence of lyssaviruses
associated with infected Chiroptera in Africa has been documented for several
decades [1]. Although surveillance is often limited or lacking, multiple
studies to date suggest that bat rabies is much more widespread throughout
Africa, and other continents, than is commonly communicated. In contrast to
bites from mammalian carnivores, such as dogs, resulting injuries after even
superficial direct contact with bats may appear rather trivial by comparison,
as illustrated in this patient, with only superficial wounds noticed on her
nose. In another typical example from Texas in 2006, a teenager died from
rabies 1 month after a report of a bat that had awakened him, after landing
upon his face [2]. Clearly, based upon both experimental and epidemiological
criteria, any probable likelihood for bona fide transdermal or mucosal
exposures to a bat is a reasonable consideration for rabies prophylaxis,
regardless of geographical locality. Confusion in the use of specific viral
taxonomy related to the presence or absence of “rabies virus” per se, versus
other “lyssaviruses”, or bio-political considerations over so-called “rabies-
free” areas, are largely irrelevant to the health provider and traveler alike,
and pale in the face of obvious public health concerns when ignored or
misunderstood, in the wake of volant reservoirs. To simplify: rabies is an
acute progressive encephalitis; the cle</field></doc>
</add>
