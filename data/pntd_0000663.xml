<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000663</field>
<field name="doi">10.1371/journal.pntd.0000663</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Sacha J. Pidot, Kingsley Asiedu, Michael Käser, Janet A. M. Fyfe, Timothy P. Stinear</field>
<field name="title">Mycobacterium ulcerans and Other Mycolactone-Producing Mycobacteria Should Be Considered a Single Species</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">7</field>
<field name="pages">e663</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
The nomenclature of Mycobacterium ulcerans has become confused with the
discovery that other mycobacteria that are not necessarily associated with
Buruli ulcer also produce the lipid toxin mycolactone. These
mycobacteria—collectively known as mycolactone-producing mycobacteria
(MPM)—have been given a variety of species names, including Mycobacterium
shinshuense, Mycobacterium pseudoshottsii, Mycobacterium marinum, and
Mycobacterium “liflandii”. Here we highlight the fact that all MPM share
sufficient phenotypic and genotypic characteristics such that they should all
be formally recognised as M. ulcerans and not separate species. Renaming all
MPM as M. ulcerans is taxonomically correct and will resolve the confusion
that is prevalent in the field and will assist political and financial
advocacy for Buruli ulcer.

Defining a bacterial species has become an increasingly difficult task,
particularly when bacteria exhibit different phenotypes but are genetically
very closely related. Genomics has shown us very clearly that subtle genetic
differences between bacteria can result in impressive phenotypic differences.
It is not surprising that the expansion of bacterial genomics has led to a
reassessment of the taxonomy of many bacterial species.

Such is the case with M. ulcerans, M. marinum, and other closely associated
mycobacteria. M. ulcerans and M. marinum are genetically related species that
cause quite different human skin diseases. M. ulcerans causes Buruli ulcer, a
disease characterised by chronic and severe skin ulcers. The bacterium
produces a lipid toxin called mycolactone, replicates slowly (doubling time
&gt;48 h) [1], and is apigmented. In contrast, M. marinum causes relatively minor
granulomatous skin lesions, often referred to as “fish tank granulomas”, has a
doubling time of 6–11 h, and produces bright yellow pigments when exposed to
light. Despite their widely different phenotypes, genome comparisons have
shown that these species share over 4,000 genes with 98.3% average DNA
sequence identity [2]. However, there are also some important genetic
differences between them. DNA–DNA hybridisation (DDH) analysis confirmed their
status as distinct species, as inter-species relative hybridisation ratios
(RBR) were less than 40% [3], [4]. The low RBR is explained by a number of
features unique to M. ulcerans, such as the presence of a large virulence
plasmid (pMUM) required for mycolactone production, and multiple copies of the
insertion sequence element IS2404 that itself accounts for 6% of the M.
ulcerans genome [2], [5].

Mycobacteria isolated recently from humans, fish, and frogs around the world
(including Japan, the Mediterranean Sea, the Red Sea, Belgium, and the United
States) have been variously called M. shinshuense, M. marinum, M.
pseudoshottsii, or given unofficial names such as M. “liflandii” [6]–[10].
Subsequent studies have used the collective term MPM when describing M.
ulcerans and these bacteria, as they all produce a form of mycolactone [5], .
Phylogenetic studies of more than 50 M. ulcerans, other MPM, and M. marinum
strains, based on multi-locus sequence analysis (MLSA) of chromosomal and pMUM
sequences and studies of large DNA InDel polymorphisms, indicate that all MPM
have likely evolved from a common M. ma</field></doc>
</add>
