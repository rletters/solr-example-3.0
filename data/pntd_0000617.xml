<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000617</field>
<field name="doi">10.1371/journal.pntd.0000617</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Jessica R. Fried, Robert V. Gibbons, Siripen Kalayanarooj, Stephen J. Thomas, Anon Srikiatkhachorn, In-Kyu Yoon, Richard G. Jarman, Sharone Green, Alan L. Rothman, Derek A. T. Cummings</field>
<field name="title">Serotype-Specific Differences in the Risk of Dengue Hemorrhagic Fever: An Analysis of Data Collected in Bangkok, Thailand from 1994 to 2006</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">3</field>
<field name="pages">e617</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

It is unclear whether dengue serotypes differ in their propensity to cause
severe disease. We analyzed differences in serotype-specific disease severity
in children presenting for medical attention in Bangkok, Thailand.

Methodology/Principal Findings

Prospective studies were conducted from 1994 to 2006. Univariate and
multivariate logistic and multinomial logistic regressions were used to
determine if dengue hemorrhagic fever (DHF) and signs of severe clinical
disease (pleural effusion, ascites, thrombocytopenia, hemoconcentration) were
associated with serotype. Crude and adjusted odds ratios were calculated.
There were 162 (36%) cases with DENV-1, 102 (23%) with DENV-2, 123 (27%) with
DENV-3, and 64 (14%) with DENV-4. There was no significant difference in the
rates of DHF by serotype: DENV-2 (43%), DENV-3 (39%), DENV-1 (34%), DENV-4
(31%). DENV-2 was significantly associated with increased odds of DHF grade I
compared to DF (OR 2.9 95% CI 1.1, 8.0), when using DENV-1 as the reference.
Though not statistically significant, DENV-2 had an increased odds of total
DHF and DHF grades II, III, and IV. Secondary serologic response was
significantly associated with DHF (OR 6.2) and increased when considering more
severe grades of DHF. DENV-2 (9%) and -4 (3%) were significantly less often
associated with primary disease than DENV-1 (28%) and -3 (33%). Restricting
analysis to secondary cases, we found DENV-2 and DENV-3 to be twice as likely
to result in DHF as DEN-4 (p = 0.05). Comparing study years, we found the rate
of DHF to be significantly less in 1999, 2000, 2004, and 2005 than in 1994,
the study year with the highest percentage of DHF cases, even when controlling
for other variables.

Conclusions/Significance

As in other studies, we find secondary disease to be strongly associated with
DHF and with more severe grades of DHF. DENV-2 appears to be marginally
associated with more severe dengue disease as evidenced by a significant
association with DHF grade I when compared to DENV-1. In addition, we found
non-significant trends with other grades of DHF. Restricting the analysis to
secondary disease we found DENV-2 and -3 to be twice as likely to result in
DHF as DEN-4. Differences in severity by study year may suggest that other
factors besides serotype play a role in disease severity.

Author Summary

The four dengue viruses (DENV) represent the most common human arbovirus
infections in the world and are currently a challenging problem, particularly
in the tropical and subtropical regions of Asia and the Americas. Infection
with DENV may produce symptoms of varying severity. While access to care,
appropriate interventions, host genetic factors, and previous exposure to DENV
are all known to affect the outcome of the infection, it is not entirely
understood why some individuals develop more severe disease. It has been
hypothesized that the four dengue serotypes differ in disease severity and
clinical manifestations. This analysis assessed whether there were significant
differences in severity of disease caused by the dengue serotypes in a
pediatric population in Thailand. We found significant and n</field></doc>
</add>
