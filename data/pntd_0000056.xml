<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000056</field>
<field name="doi">10.1371/journal.pntd.0000056</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Janjira Thaipadungpanit, Vanaporn Wuthiekanun, Wirongrong Chierakul, Lee D. Smythe, Wimol Petkanchanapong, Roongrueng Limpaiboon, Apichat Apiwatanaporn, Andrew T. Slack, Yupin Suputtamongkol, Nicholas J. White, Edward J. Feil, Nicholas P. J. Day, Sharon J. Peacock</field>
<field name="title">A Dominant Clone of Leptospira interrogans Associated with an Outbreak of Human Leptospirosis in Thailand</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2007</field>
<field name="volume">1</field>
<field name="number">1</field>
<field name="pages">e56</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

A sustained outbreak of leptospirosis occurred in northeast Thailand between
1999 and 2003, the basis for which was unknown.

Methods and Findings

A prospective study was conducted between 2000 and 2005 to identify patients
with leptospirosis presenting to Udon Thani Hospital in northeast Thailand,
and to isolate the causative organisms from blood. A multilocus sequence
typing scheme was developed to genotype these pathogenic Leptospira.
Additional typing was performed for Leptospira isolated from human cases in
other Thai provinces over the same period, and from rodents captured in the
northeast during 2004. Sequence types (STs) were compared with those of
Leptospira drawn from a reference collection. Twelve STs were identified among
101 isolates from patients in Udon Thani. One of these (ST34) accounted for 77
(76%) of isolates. ST34 was Leptospira interrogans, serovar Autumnalis. 86% of
human Leptospira isolates from Udon Thani corresponded to ST34 in 2000/2001,
but this figure fell to 56% by 2005 as the outbreak waned (p = 0.01). ST34
represented 17/24 (71%) of human isolates from other Thai provinces, and 7/8
(88%) rodent isolates. By contrast, 59 STs were found among 76 reference
strains, indicating a much more diverse population genetic structure; ST34 was
not identified in this collection.

Conclusions

Development of an MLST scheme for Leptospira interrogans revealed that a
single ecologically successful pathogenic clone of L. interrogans predominated
in the rodent population, and was associated with a sustained outbreak of
human leptospirosis in Thailand.

Author Summary

A sustained outbreak of human leptospirosis occurred in northeast Thailand
between 1999 and 2003, the basis for which was unknown. Leptospirosis is a
potentially serious infection cause by bacteria known as Leptospira; infection
usually occurs following environmental exposure to pathogenic Leptospira shed
in the urine of an infected animal. The purpose of this study was to obtain
bacterial isolates from humans with leptospirosis around the time of the Thai
outbreak for genotyping, and to relate these to the maintenance host animal.
To achieve this, a bacterial typing scheme (multilocus sequence typing, MLST)
was developed for L. interrogans, the major cause of human disease. This
approach has the advantage over existing typing schemes in that the data
generated are amenable to detailed evolutionary analysis, and are readily
comparable via the internet. Our results demonstrated the emergence of a
dominant clone of L. interrogans serovar Autumnalis; this was the major cause
of human disease during the outbreak, and was found in a maintenance host
which was defined as the bandicoot rat.

Introduction

Leptospirosis is a zoonotic infection caused by pathogenic members of the
genus Leptospira. Human disease is usually acquired following environmental
exposure to Leptospira shed in the urine of an infected animal [1],[2].
Infection is acquired during occupational or recreational exposure to
contaminated soil and water, organisms gaining entry to the accidental human
host via abra</field></doc>
</add>
