<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000068</field>
<field name="doi">10.1371/journal.pntd.0000068</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Rommie E. Amaro, Robert V. Swift, J. Andrew McCammon</field>
<field name="title">Functional and Structural Insights Revealed by Molecular Dynamics Simulations of an Essential RNA Editing Ligase in Trypanosoma brucei</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2007</field>
<field name="volume">1</field>
<field name="number">2</field>
<field name="pages">e68</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

RNA editing ligase 1 (TbREL1) is required for the survival of both the insect
and bloodstream forms of Trypanosoma brucei, the parasite responsible for the
devastating tropical disease African sleeping sickness. The type of RNA
editing that TbREL1 is involved in is unique to the trypanosomes, and no close
human homolog is known to exist. In addition, the high-resolution crystal
structure revealed several unique features of the active site, making this
enzyme a promising target for structure-based drug design. In this work, two
20 ns atomistic molecular dynamics (MD) simulations are employed to
investigate the dynamics of TbREL1, both with and without the ATP substrate
present. The flexibility of the active site, dynamics of conserved residues
and crystallized water molecules, and the interactions between TbREL1 and the
ATP substrate are investigated and discussed in the context of TbREL1&apos;s
function. Differences in local and global motion upon ATP binding suggest that
two peripheral loops, unique to the trypanosomes, may be involved in
interdomain signaling events. Notably, a significant structural rearrangement
of the enzyme&apos;s active site occurs during the apo simulations, opening an
additional cavity adjacent to the ATP binding site that could be exploited in
the development of effective inhibitors directed against this protozoan
parasite. Finally, ensemble averaged electrostatics calculations over the MD
simulations reveal a novel putative RNA binding site, a discovery that has
previously eluded scientists. Ultimately, we use the insights gained through
the MD simulations to make several predictions and recommendations, which we
anticipate will help direct future experimental studies and structure-based
drug discovery efforts against this vital enzyme.

Author Summary

RNA editing ligase 1 (TbREL1) is required for the survival of both the insect
and bloodstream forms of Trypanosoma brucei, the parasite responsible for the
devastating tropical disease African sleeping sickness. The type of RNA
editing that TbREL1 is involved in is unique to the trypanosomes, and no close
human homolog is known to exist. Here we use molecular dynamics simulations to
investigate the dynamics of TbREL1, both with and without the ATP substrate
present. The flexibility of the active site, dynamics of conserved residues
and crystallized water molecules, and the interactions between TbREL1 and the
ATP substrate are investigated and discussed. During the apo simulations, a
significant structural rearrangement of the enzyme&apos;s active site opens an
additional cavity adjacent to the ATP binding site that could be exploited in
the development of effective inhibitors against this protozoan parasite.
State-of-the-art electrostatics calculations reveal a novel putative RNA
binding site, a discovery that has previously eluded scientists. Ultimately,
we use the insights gained through the MD simulations to make several
predictions, which we anticipate will help direct future experimental studies
and structure-based drug discovery efforts against this vital enzyme.

Introduction

The existence and widespread occurrence of several devastating trypanosomal
tropical diseases, such as Chagas&apos; disease and African sleeping sickness,
cause an estimated 1 million deaths</field></doc>
</add>
