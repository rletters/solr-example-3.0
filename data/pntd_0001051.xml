<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001051</field>
<field name="doi">10.1371/journal.pntd.0001051</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Hipólito Nzwalo, Julie Cliff</field>
<field name="title">Konzo: From Poverty, Cassava, and Cyanogen Intake to Toxico-Nutritional Neurological Disease</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">6</field>
<field name="pages">e1051</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Konzo is a distinct neurological entity with selective upper motor neuron
damage, characterized by an abrupt onset of an irreversible, non-progressive,
and symmetrical spastic para/tetraparesis. Despite its severity, konzo remains
a neglected disease. The disease is associated with high dietary cyanogen
consumption from insufficiently processed roots of bitter cassava combined
with a protein-deficient diet. Epidemics occur when these conditions coincide
at times of severe food shortage. Up to 1993, outbreaks in poor rural areas in
Africa contributed to more than 3,700 cases of konzo. The number of affected
people is underestimated. From unofficial reports, the number of cases was
estimated to be at least 100,000 in 2000, in contrast to the 6,788 cases
reported up to 2009 from published papers.

Introduction

Konzo is a distinct neurological entity with selective upper motor neuron
damage, characterized by an abrupt onset of an irreversible, non-progressive,
and symmetrical spastic para/tetraparesis [1]–[8].

The disease is associated with prolonged high dietary cyanogen consumption
from insufficiently processed roots of bitter cassava combined with a protein-
deficient diet low in sulphur amino acids (SAAs) [1]–[8].

Since its first description by the Italian doctor Trolli eight decades ago in
the former Belgian Congo (now the Democratic Republic of Congo [DRC]),
epidemics have been reported from many cassava-consuming areas in rural
Africa. Up to 1993, the total of reported cases was approximately 3,700 to
4,000 [9]–[11].

Konzo remains a health problem in Africa. Since 1993, the disease has extended
beyond its first reported boundaries [12], and the reported number of konzo
cases has almost doubled, reaching a total of 6,788 (Table 1, Figure 1).

Figure 1

Countries in Africa where konzo has been reported.

Table 1

Total number of konzo cases reported up to 2009.

Country

Prior to 1975

1975–1993

1994–2009

Total

Democratic Rebublic of Congo

1,237

919

1,303

3,459

Mozambique

2,123

281

2404

Tanzania

121

238

359

Central African Republic

16

81

97

Cameroon

469

469

Total

1,237

3,179

2,372

6,788

Adapted from [9].

Outbreaks in past decades in Cameroon [12], Mozambique [13], [14], Tanzania
[15], the Central African Republic [16], and the DRC [17], [18] often received
insignificant attention from the media and local health authorities despite
the clinical severity of konzo. As in the earlier reported outbreaks, poverty
in association with agricultural crises provoked by drought or war was a
constant feature. Those affected belonged to the poorest segments of the most
remote rural areas of Africa, perpetuating the silence around the disease.

Cassava is also an important source of food in the tropics outside of Africa
[19], and, although remote, the possibility of konzo occurring in these areas
should be considered. Another disease associated with chronic cassava
consumption, tropical ataxic neuropathy (TAN), has been described recently in
India [20]. The same socioeconomic factors implicated in konzo were present,
and in some cases, patients had clinical features compatible with konzo.

Lessons from the previous outbreaks indicate that the number of people
affected by konzo is underestimated. Unofficial reports point to an alarming
number of 100,000 cases in the DRC in 2000 [</field></doc>
</add>
