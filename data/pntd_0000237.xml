<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000237</field>
<field name="doi">10.1371/journal.pntd.0000237</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Mitchell G. Weiss</field>
<field name="title">Stigma and the Social Burden of Neglected Tropical Diseases</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">5</field>
<field name="pages">e237</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
In a village in Uganda where onchocerciasis is endemic, a 25-year-old woman
responded to questions about a photograph of a skin lesion presented with the
story of a villager suffering from characteristic dermatitis. She described
her community&apos;s experience as follows:

“They are hiding their skin so that people cannot see them. I have not heard
of anyone who wants others to know about it. No one will allow them to lead,
and many people ignore them. They are considered dangerous. People fear
contact with them. I feel sorry for them. Even me, I feared that from staying
and meeting them we could get the disease … They find it hard to marry, and
marriages can break because of this condition.”

Introduction

Over the past half century, social stigma has become an increasingly important
topic for health social sciences. Among neglected tropical diseases (NTDs), to
which I restrict my attention in this article, leprosy has been a major focus
of stigma studies from the outset. Other NTDs for which stigma is an important
consideration include onchocerciasis, lymphatic filariasis, plague, Buruli
ulcer, leishmaniasis, and Chagas disease. Public health interest in stigma has
been especially concerned with the social burden it attaches to illness, as
illustrated by the account presented above. Stigma is also an important social
determinant of the effectiveness of disease control through its effect on
help-seeking and treatment adherence. Furthermore, stigma influences political
commitment to disease control. Although that is typically a problem because
stigma may encourage neglect, for agencies committed to working on problems
that matter, recognition of the serious impact of stigma may encourage them to
support disease control. The recent histories of onchocerciasis and lymphatic
filariasis control, noted later in this article, illustrate this point.

The impact of stigma is not readily accounted for in the epidemiological data
that characterize the defined burden of disease. Instead, stigma imposes what
has been termed a “hidden burden” [1]. Increasing health research interest in
the topic is indicated by the literature cited in Medline. The first citation
appeared in 1950, and there was no more than one citation in seven of the next
15 years to 1964. With the publication of Goffman&apos;s seminal treatise on stigma
in 1963 [2], many more followed. Six citations, mostly concerned with mental
health but one with leprosy stigma, are listed for 1965, and there has not
been a year since then without a contribution to the health literature on
social stigma. In recent years, the number has increased sharply, to 458 in
2006 (Figure 1).

Figure 1

Medline Citations for Social Stigma (1965–2007).

The annual number of citations for articles identified in a search for
“stigma” as a text word (i.e., in the title or abstract) and excluding
references to usage as a botanical term.

Here, I address key questions about how concepts of stigma have changed over
time. Who is affected, and how? What are the relevant distinctions between
stigma associated with culture-specific meaning of a disease and with the
social response to signs and symptoms? Current interest in the topic aims to
apply answers to such questions in disease control to reduce the social burden
of NTDs. Ideally, practical health social science interest aims to transform
social stigma into social support.</field></doc>
</add>
