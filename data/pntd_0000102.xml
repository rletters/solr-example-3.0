<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000102</field>
<field name="doi">10.1371/journal.pntd.0000102</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Judd L. Walson, Grace John-Stewart</field>
<field name="title">Treatment of Helminth Co-Infection in Individuals with HIV-1: A Systematic Review of the Literature</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2007</field>
<field name="volume">1</field>
<field name="number">3</field>
<field name="pages">e102</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background and Objectives

The HIV-1 pandemic has disproportionately affected individuals in resource-
constrained settings. It is important to determine if other prevalent
infections affect the progression of HIV-1 in co-infected individuals in these
settings. Some observational studies suggest that helminth infection may
adversely affect HIV-1 progression. We sought to evaluate existing evidence on
whether treatment of helminth infection impacts HIV-1 progression.

Review Methods

This review was conducted using the HIV/AIDS Cochrane Review Group (CRG)
search strategy and guidelines. Published and unpublished studies were
obtained from The Cochrane Library (Issue 3, 2006), MEDLINE (November 2006),
EMBASE (November 2006), CENTRAL (July 2006), and AIDSEARCH (August 2006).
Databases listing conference abstracts and scanned reference lists were
searched, and authors of included studies were contacted. Data regarding
changes in CD4 count, HIV-1 RNA levels, clinical staging and/or mortality were
extracted and compared between helminth-treated and helminth-untreated or
helminth-uninfected individuals.

Results

Of 6,384 abstracts identified, 15 met criteria for potential inclusion, of
which 5 were eligible for inclusion. In the single randomized controlled trial
(RCT) identified, HIV-1 and schistosomiasis co-infected individuals receiving
treatment for schistosomiasis had a significantly lower change in plasma HIV-1
RNA over three months (−0.001 log10 copies/mL) compared to those receiving no
treatment (+0.21 log10 copies/mL), (p = 0.03). Four observational studies met
inclusion criteria, and all of these suggested a possible beneficial effect of
helminth eradication on plasma HIV-1 RNA levels when compared to plasma HIV-1
RNA changes prior to helminth treatment or to helminth-uninfected or
persistently helminth-infected individuals. The follow-up duration in these
studies ranged from three to six months. The reported magnitude of effect on
HIV-1 RNA was variable, ranging from 0.07–1.05 log10 copies/mL. None of the
included studies showed a significant benefit of helminth treatment on CD4
decline, clinical staging, or mortality.

Conclusion

There are insufficient data available to determine the potential benefit of
helminth eradication in HIV-1 and helminth co-infected adults. Data from a
single RCT and multiple observational studies suggest possible benefit in
reducing plasma viral load. The impact of de-worming on markers of HIV-1
progression should be addressed in larger randomized studies evaluating
species-specific effects and with a sufficient duration of follow-up to
document potential differences on clinical outcomes and CD4 decline.

Author Summary

Many people living in areas of the world most affected by the HIV/AIDS
pandemic are also exposed to other common infections. Parasitic infections
with helminths (intestinal worms) are common in Africa and affect over half of
the population in some areas. There are plausible biological reasons why
treating helminth infections in people with HIV may slow down the progression
of HIV to AIDS. Thus, treating people with HIV for helminths in areas with a
high prevalence of both HIV and helminth infections may be a feasible strategy
to help people with HIV delay progression of their disease or initiation of
antiretroviral therapy. After a comprehe</field></doc>
</add>
