<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001360</field>
<field name="doi">10.1371/journal.pntd.0001360</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Steve J. Torr, Glyn A. Vale</field>
<field name="title">Is the Even Distribution of Insecticide-Treated Cattle Essential for Tsetse Control? Modelling the Impact of Baits in Heterogeneous Environments</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">10</field>
<field name="pages">e1360</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Eliminating Rhodesian sleeping sickness, the zoonotic form of Human African
Trypanosomiasis, can be achieved only through interventions against the
vectors, species of tsetse (Glossina). The use of insecticide-treated cattle
is the most cost-effective method of controlling tsetse but its impact might
be compromised by the patchy distribution of livestock. A deterministic
simulation model was used to analyse the effects of spatial heterogeneities in
habitat and baits (insecticide-treated cattle and targets) on the distribution
and abundance of tsetse.

Methodology/Principal Findings

The simulated area comprised an operational block extending 32 km from an area
of good habitat from which tsetse might invade. Within the operational block,
habitat comprised good areas mixed with poor ones where survival probabilities
and population densities were lower. In good habitat, the natural daily
mortalities of adults averaged 6.14% for males and 3.07% for females; the
population grew 8.4× in a year following a 90% reduction in densities of
adults and pupae, but expired when the population density of males was reduced
to &lt;0.1/km2; daily movement of adults averaged 249 m for males and 367 m for
females. Baits were placed throughout the operational area, or patchily to
simulate uneven distributions of cattle and targets. Gaps of 2–3 km between
baits were inconsequential provided the average imposed mortality per km2
across the entire operational area was maintained. Leaving gaps 5–7 km wide
inside an area where baits killed 10% per day delayed effective control by
4–11 years. Corrective measures that put a few baits within the gaps were more
effective than deploying extra baits on the edges.

Conclusions/Significance

The uneven distribution of cattle within settled areas is unlikely to
compromise the impact of insecticide-treated cattle on tsetse. However, where
areas of &gt;3 km wide are cattle-free then insecticide-treated targets should be
deployed to compensate for the lack of cattle.

Author Summary

Eliminating Rhodesian sleeping sickness, the zoonotic form of Human African
Trypanosomiasis found in East and Southern Africa, can be achieved only
through eliminating the vectors, species of tsetse fly (Glossina). The
deployment of insecticide-treated cattle is the most cost-effective means of
achieving this. However, the even distribution of insecticide-treated cattle
is seldom possible due to the patchy distribution of grazing, water and human
settlement. We used a simulation model to explore the likely impact of such
patchiness on the outcome of control operations against tsetse. The results
suggest that even in areas that are highly suitable for tsetse, gaps of up to
3 km in the distribution of insecticide-treated cattle will not have a
material impact on the success of an operation provided the overall mean
density of cattle across all areas is adequate to achieve control (e.g., ∼4
insecticide-treated cattle/km2 killing 10% per day of the tsetse in the area
treated). If the gaps are larger than 3 km, then deploying insecticide-treated
targets at densities of 4/km2 in the cattle-free areas will ensure success.

Introduction

Rhodesian sleeping sickness, caused by Trypanosoma brucei rhodesiense, is
transmitted by tsetse flies (Glossina spp.) a</field></doc>
</add>
