<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000963</field>
<field name="doi">10.1371/journal.pntd.0000963</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Matthew T. Aliota, Cheng-Chen Chen, Henry Dagoro, Jeremy F. Fuchs, Bruce M. Christensen</field>
<field name="title">Filarial Worms Reduce Plasmodium Infectivity in Mosquitoes</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">2</field>
<field name="pages">e963</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Co-occurrence of malaria and filarial worm parasites has been reported, but
little is known about the interaction between filarial worm and malaria
parasites with the same Anopheles vector. Herein, we present data evaluating
the interaction between Wuchereria bancrofti and Anopheles punctulatus in
Papua New Guinea (PNG). Our field studies in PNG demonstrated that An.
punctulatus utilizes the melanization immune response as a natural mechanism
of filarial worm resistance against invading W. bancrofti microfilariae. We
then conducted laboratory studies utilizing the mosquitoes Armigeres
subalbatus and Aedes aegypti and the parasites Brugia malayi, Brugia pahangi,
Dirofilaria immitis, and Plasmodium gallinaceum to evaluate the hypothesis
that immune activation and/or development by filarial worms negatively impact
Plasmodium development in co-infected mosquitoes. Ar. subalbatus used in this
study are natural vectors of P. gallinaceum and B. pahangi and they are
naturally refractory to B. malayi (melanization-based refractoriness).

Methodology/Principal Findings

Mosquitoes were dissected and Plasmodium development was analyzed six days
after blood feeding on either P. gallinaceum alone or after taking a bloodmeal
containing both P. gallinaceum and B. malayi or a bloodmeal containing both P.
gallinaceum and B. pahangi. There was a significant reduction in the
prevalence and mean intensity of Plasmodium infections in two species of
mosquito that had dual infections as compared to those mosquitoes that were
infected with Plasmodium alone, and was independent of whether the mosquito
had a melanization immune response to the filarial worm or not. However, there
was no reduction in Plasmodium development when filarial worms were present in
the bloodmeal (D. immitis) but midgut penetration was absent, suggesting that
factors associated with penetration of the midgut by filarial worms likely are
responsible for the observed reduction in malaria parasite infections.

Conclusions/Significance

These results could have an impact on vector infection and transmission
dynamics in areas where Anopheles transmit both parasites, i.e., the
elimination of filarial worms in a co-endemic locale could enhance malaria
transmission.

Author Summary

The parasites that cause malaria and human lymphatic filariasis are both
transmitted by mosquitoes, and often times in areas where these two diseases
are co-endemic, mosquitoes in the genus Anopheles transmit both parasites.
Currently, it is unknown how parasite transmission is effected when malaria
and filarial worm parasites share the same vector. Here, we show that when
these two parasites share the same mosquito host, there is a significant
reduction in the intensity and prevalence of Plasmodium infections. This
reduction occurs regardless of the mosquito having a melanization-based immune
response activated by filarial worms or when filarial worms successfully
develop within the mosquito host. We also observed that filarial worm
penetration of the mosquito midgut was necessary for malaria parasite
reduction to occur. Our study provides new insight into the relationship
between malaria and filarial worm parasites with their mosquito host, which
could impact transmission dynamics in areas where both parasites are
transmitted by the same mosquito specie</field></doc>
</add>
