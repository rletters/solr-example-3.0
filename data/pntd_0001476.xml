<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001476</field>
<field name="doi">10.1371/journal.pntd.0001476</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Min-Shi Lee, Pai-Shan Chiang, Shu-Ting Luo, Mei-Liang Huang, Guan-Yuan Liou, Kuo-Chien Tsao, Tzou-Yien Lin</field>
<field name="title">Incidence Rates of Enterovirus 71 Infections in Young Children during a Nationwide Epidemic in Taiwan, 2008–09</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">2</field>
<field name="pages">e1476</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Objective

Enterovirus 71 (EV71) is causing life-threatening outbreaks in tropical Asia.
In Taiwan and other tropical Asian countries, although nationwide EV71
epidemics occur cyclically, age-specific incidence rates of EV71 infections
that are critical to estimate disease burden and design vaccine trials are not
clear. A nationwide EV71 epidemic occurred in 2008–09 in Taiwan, which
provided a unique opportunity to estimate age-specific incidence rates of EV71
infections.

Study Design

We prospectively recruited 749 healthy neonates and conducted follow-ups from
June 2006 to December 2009. Sera were obtained from participants at 0, 6, 12,
24, and 36 months of age for measuring EV71 neutralizing antibody titers. If
the participants developed suspected enterovirus illnesses, throat swabs were
collected for virus isolation.

Results

We detected 28 EV71 infections including 20 symptomatic and 8 asymptomatic
infections. Age-specific incidence rates of EV71 infection increased from 1.71
per 100 person-years at 0–6 months of age to 4.09, 5.74, and 4.97 per 100
person-years at 7–12, 13–24, and 25–36 months of age, respectively. Cumulative
incidence rate was 15.15 per 100 persons by 36 months of age, respectively.

Conclusions

Risk of EV71 infections in Taiwan increased after 6 months of age during EV71
epidemics. The cumulative incidence rate was 15% by 36 months of age, and 29%
of EV71 infections were asymptomatic in young children.

Author Summary

Enterovirus 71 (EV71) was first isolated in California, USA, in 1969. Since
then, EV71 has been identified globally. Recently, EV71 caused several life-
threatening outbreaks in young children in tropical Asia. Development of EV71
vaccines becomes national priority in several Asia countries including Taiwan.
To design clinical trials of EV71 vaccines, age-specific incidence rates of
EV71 infections are required to identify target populations, estimate disease
burdens, select endpoints of clinical efficacy, and estimate sample size. In
Taiwan, nationwide EV71 epidemics occurred every 3–4 years but age-specific
incidences of EV71 infection are not available. In 2006, we initiated a
prospective cohort study in northern Taiwan to recruit neonates and follow up
them. In 2008–09, a nationwide EV71 epidemic occurred and we found that age-
specific incidence rates of EV71 infection increased from 1.71 per 100 person-
years at 0–6 months of age to 4.09, 5.74, and 4.97 per 100 person-years at
7–12, 13–24, and 25–36 months of age, respectively. The cumulative incidence
rate was 15% by 36 months of age, and 29% of EV71 infections were asymptomatic
in young children. These findings would be helpful to development of EV71
vaccines in Taiwan and other Asian tropical countries.

Introduction

Enterovirus 71 (EV71) was first isolated in California, USA, in 1969. Since
then, EV71 has been identified globally. The clinical spectrum of EV71
infection ranges from asymptomatic infection, to mild hand-foot-mouth disease
(HFMD), and severe cases with central nervous system (CNS), and
cardiopulmonary involvement [1], [2]. Recent studies have further demonstrated
that CNS-complicated EV71 infections could cause long-term cognitive and motor
deficits [3], [4]. Globally, two pat</field></doc>
</add>
