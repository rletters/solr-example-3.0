<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001406</field>
<field name="doi">10.1371/journal.pntd.0001406</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Juliane Schroeder, Najmeeyah Brown, Paul Kaye, Toni Aebischer</field>
<field name="title">Single Dose Novel Salmonella Vaccine Enhances Resistance against Visceralizing L. major and L. donovani Infection in Susceptible BALB/c Mice</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">12</field>
<field name="pages">e1406</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Visceral leishmaniasis is a major neglected tropical disease, with an
estimated 500,000 new cases and more than 50,000 deaths attributable to this
disease every year. Drug therapy is available but costly and resistance
against several drug classes has evolved. Despite all efforts, no commercial,
let alone affordable, vaccine is available to date. Thus, the development of
cost effective, needle-independent vaccines is a high priority. Here, we have
continued efforts to develop live vaccine carriers based on recombinant
Salmonella. We used an in silico approach to select novel Leishmania parasite
antigens from proteomic data sets, with selection criteria based on protein
abundance, conservation across Leishmania species and low homology to host
species. Five chosen antigens were differentially expressed on the surface or
in the cytosol of Salmonella typhimurium SL3261. A two-step procedure was
developed to select optimal Salmonella vaccine strains for each antigen, based
on bacterial fitness and antigen expression levels. We show that vaccine
strains of Salmonella expressing the novel Leishmania antigens LinJ08.1190 and
LinJ23.0410 significantly reduced visceralisation of L. major and enhanced
systemic resistance against L. donovani in susceptible BALB/c mice. The
results show that Salmonella are valid vaccine carriers for inducing
resistance against visceral leishmaniasis but that their use may not be
suitable for all antigens.

Author Summary

The leishmaniases are tropical diseases that affect the poorest of the poor.
They are caused by Leishmania species, protozoan parasites transmitted by
blood sucking insects and the visceral form of the disease is fatal. Vaccines
that would tremendously boost disease control strategies need to be designed
cost-efficiently and for the existing infrastructure. Salmonella-based live
vaccines could fulfil these requirements as they can be cheaply produced on an
industrial scale and the lyophilized product can be stored at room temperature
and upon rehydration is ready for oral, needle-free application. Salmonella,
like Leishmania, are intracellular pathogens that primarily target host
macrophages. The bacteria induce a viscerotropic immune response. Herein lies
a potentially significant advantage of using attenuated Salmonella as delivery
vehicles for parasite antigens for vaccination against visceral leishmaniasis.
We used in vivo inducible promoters and optimized expression systems to
construct attenuated Salmonella carriers that deliver novel vaccine antigens
and show a host protective effect in small rodent models of visceral
leishmaniasis. These proof-of-concept studies should serve to further promote
exploration of live Salmonella as a cost effective and widely applicable
carrier for vaccination against leishmaniases.

Introduction

The leishmaniases are regarded as neglected tropical diseases. The causative
protozoan parasites are transmitted through the bite of sandfly vectors.
Currently an estimated 12 million people are infected, while 350 million
people in 88 countries worldwide are at risk to develop one of the diseases
associated with Leishmania parasites
(http://www.who.int/leishmaniasis/burden/en/; [1]). The most severe form is
visceral leishmaniasis (VL; also known as kala azar i</field></doc>
</add>
