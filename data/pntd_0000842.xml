<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000842</field>
<field name="doi">10.1371/journal.pntd.0000842</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Peter E. Kima, J. Alfredo Bonilla, Eumin Cho, Blaise Ndjamen, Johnathan Canton, Nicole Leal, Martin Handfield</field>
<field name="title">Identification of Leishmania Proteins Preferentially Released in Infected Cells Using Change Mediated Antigen Technology (CMAT)</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">10</field>
<field name="pages">e842</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Although Leishmania parasites have been shown to modulate their host cell&apos;s
responses to multiple stimuli, there is limited evidence that parasite
molecules are released into infected cells. In this study, we present an
implementation of the change mediated antigen technology (CMAT) to identify
parasite molecules that are preferentially expressed in infected cells. Sera
from mice immunized with cell lysates prepared from L. donovani or L. pifanoi-
infected macrophages were adsorbed with lysates of axenically grown
amastigotes of L. donovani or L. pifanoi, respectively, as well as uninfected
macrophages. The sera were then used to screen inducible parasite expression
libraries constructed with genomic DNA. Eleven clones from the L. pifanoi and
the L. donovani screen were selected to evaluate the characteristics of the
molecules identified by this approach. The CMAT screen identified genes whose
homologs encode molecules with unknown function as well as genes that had
previously been shown to be preferentially expressed in the amastigote form of
the parasite. In addition a variant of Tryparedoxin peroxidase that is
preferentially expressed within infected cells was identified. Antisera that
were then raised to recombinant products of the clones were used to validate
that the endogenous molecules are preferentially expressed in infected cells.
Evaluation of the distribution of the endogenous molecules in infected cells
showed that some of these molecules are secreted into parasitophorous vacuoles
(PVs) and that they then traffic out of PVs in vesicles with distinct
morphologies. This study is a proof of concept study that the CMAT approach
can be applied to identify putative Leishmania parasite effectors molecules
that are preferentially expressed in infected cells. In addition we provide
evidence that Leishmania molecules traffic out of the PV into the host cell
cytosol and nucleus.

Author Summary

Leishmania are intracellular parasites that reside within parasitophorous
vacuoles (PV) in phagocytes. From within these compartments parasites control
the host cell&apos;s responses to multiple stimuli. There is limited knowledge of
the molecules that Leishmania parasites elaborate in the host cell to target
processes therein. Furthermore, the mechanism by which such molecules would
access their targets beyond the PV is not known. In the study presented here,
we implemented the change mediated antigen technology (CMAT) to identify
parasite molecules that are preferentially expressed inside infected cells.
The approach was based on the reasoning that parasites express ‘new’ or
antigenically modified molecules in the intracellular environment; therefore
antiserum that is reactive to infected cells would contain immunoglobulins
that are specific to these ‘new’ molecules. After adsorption of the antiserum
with axenically cultured parasites, the antiserum was used to screen a
parasite genomic expression library to identify genes encoding the molecules
that are preferentially expressed in infected cells. We present for the first
time evidence that some of these CMAT molecules accumulate in the PV and then
traffic into the host cell in vesicles of distinct morphologies. Furthermore,
several of th</field></doc>
</add>
