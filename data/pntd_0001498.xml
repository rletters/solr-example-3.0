<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001498</field>
<field name="doi">10.1371/journal.pntd.0001498</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Peter J. Hotez, Eric Dumonteil, Laila Woc-Colburn, Jose A. Serpa, Sarah Bezek, Morven S. Edwards, Camden J. Hallmark, Laura W. Musselwhite, Benjamin J. Flink, Maria Elena Bottazzi</field>
<field name="title">Chagas Disease: “The New HIV/AIDS of the Americas”</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">5</field>
<field name="pages">e1498</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Endemic Chagas disease has emerged as an important health disparity in the
Americas. As a result, we face a situation in both Latin America and the US
that bears a resemblance to the early years of the HIV/AIDS pandemic.

Neglected tropical diseases (NTDs) are among the most common conditions
afflicting the estimated 99 million people who live on less than US$2 per day
in the Latin American and Caribbean (LAC) region [1]. Almost all of the
“bottom 100 million” living in the Americas suffer from at least one NTD [1],
and according to some estimates, the NTDs cause a burden of disease in the LAC
region that closely approximates or even exceeds that resulting from HIV/AIDS
[2]. Chagas disease (American trypanosomiasis) is a vector-borne disease and a
leading cause of the deaths and disability-adjusted life years (DALYs) lost
that result from NTDs in the LAC region [2]. With approximately 10 million
people living with Chagas disease, this condition is one of the most common
NTDs affecting the bottom 100 million in the region, a prevalence exceeded
only by hookworm and other soil-transmitted helminth infections [1], [2].
Moreover, among the NTDs in the Americas, Chagas disease ranks near the top in
terms of annual deaths and DALYs lost [1], [2].

While most of the world&apos;s cases of Chagas disease occur in the LAC region,
there is increasing recognition that many people with Trypanosoma cruzi
infection also live in the US and Europe [3]. In practical terms, the
“globalization” of Chagas translates to up to 1 million cases in the US alone,
with an especially high burden of disease in Texas and along the Gulf coast
[4], [5], although other estimates suggest that there are approximately
300,000 cases in the US [6], in addition to thousands of cases documented in
Canada, Europe, Australia, and Japan [3]. Among those living with Chagas
disease around the world today, 20%–30% (roughly 2–3 million people) are
either currently suffering from Chagasic cardiomyopathy or will develop this
clinical sequela [7]. Chagasic cardiomyopathy is a highly debilitating
condition characterized by cardiac arrhythmias, heart failure, and risk of
sudden death from ventricular fibrillation or tachycardia or thromboembolic
events [7]. Another estimate suggests that up to 5.4 million people living
today will develop Chagasic cardiomyopathy [8], [9]. Damage to the
gastrointestinal tract can also produce debilitating megaesophagus and
megacolon [7].

There are a number of striking similarities between people living with Chagas
disease and people living with HIV/AIDS, particularly for those with HIV/AIDS
who contracted the disease in the first two decades of the HIV/AIDS epidemic.
Both diseases are health disparities, disproportionately affecting people
living in poverty [1], [2]. Both are chronic conditions requiring prolonged
treatment courses: a lifetime of antiretroviral therapy for HIV/AIDS patients,
and one to three months of therapy for those with Chagas disease [7].
Treatment for HIV/AIDS is lifesaving, although it seldom if ever results in
cure, while for Chagas disease, the treatment has proven efficacy only for the
acute stages of the infection or in children up to 12 years of age during the
early chronic phase of the i</field></doc>
</add>
