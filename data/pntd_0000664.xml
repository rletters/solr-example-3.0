<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000664</field>
<field name="doi">10.1371/journal.pntd.0000664</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Luciana Inácia Gomes, Letícia Helena dos Santos Marques, Martin Johannes Enk, Maria Cláudia de Oliveira, Paulo Marcos Zech Coelho, Ana Rabello</field>
<field name="title">Development and Evaluation of a Sensitive PCR-ELISA System for Detection of Schistosoma Infection in Feces</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">4</field>
<field name="pages">e664</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

A PCR-enzyme-linked immunosorbent assay (PCR-ELISA) was developed to overcome
the need for sensitive techniques for the efficient diagnosis of Schistosoma
infection in endemic settings with low parasitic burden.

Methodology/Principal Findings

This system amplifies a 121-base pair tandem repeat DNA sequence, immobilizes
the resultant 5′ biotinylated product on streptavidin-coated strip-well
microplates and uses anti-fluorescein antibodies conjugated to horseradish
peroxidase to detect the hybridized fluorescein-labeled oligonucleotide probe.
The detection limit of the Schistosoma PCR-ELISA system was determined to be
1.3 fg of S. mansoni genomic DNA (less than the amount found in a single cell)
and estimated to be 0.15 S. mansoni eggs per gram of feces (fractions of an
egg). The system showed good precision and genus specificity since the DNA
target was found in seven Schistosoma DNA samples: S. mansoni, S. haematobium,
S. bovis, S. intercalatum, S. japonicum, S. magrebowiei and S. rhodaini. By
evaluating 206 patients living in an endemic area in Brazil, the prevalence of
S. mansoni infection was determined to be 18% by examining 12 Kato-Katz slides
(41.7 mg/smear, 500 mg total) of a single fecal sample from each person, while
the Schistosoma PCR-ELISA identified a 30% rate of infection using 500-mg of
the same fecal sample. When considering the Kato-Katz method as the reference
test, artificial sensitivity and specificity rates of the PCR-ELISA system
were 97.4% and 85.1%, respectively. The potential for estimating parasitic
load by DNA detection in feces was assessed by comparing absorbance values and
eggs per gram of feces, with a Spearman correlation coefficient of 0.700
(P&lt;0.0001).

Conclusions/Significance

This study reports the development and field evaluation of a sensitive
Schistosoma PCR-ELISA, a system that may serve as an alternative for
diagnosing Schistosoma infection.

Author Summary

Schistosomiasis is a neglected disease caused by worms of the genus
Schistosoma. The transmission cycle requires contamination of bodies of water
by parasite eggs present in excreta, specific snails as intermediate hosts and
human contact with water. Fortunately, relatively safe and easily
administrable drugs are available and, as the outcome of repeated treatment, a
reduction of severe clinical forms and a decrease in the number of infected
persons has been reported in endemic areas. The routine method for diagnosis
is the microscopic examination but it fails when there are few eggs in the
feces, as usually occurs in treated but noncured persons or in areas with low
levels of transmission. This study reports the development of the PCR-ELISA
system for the detection of Schistosoma DNA in human feces as an alternative
approach to diagnose light infections. The system permits the enzymatic
amplification of a specific region of the DNA from minute amounts of parasite
material. Using the proposed PCR-ELISA approach for the diagnosis of a
population in an endemic area in Brazil, 30% were found to be infected, as
compared with the 18% found by microscopic fecal examination. Although the
technique requires a complex laboratory infrastructure and specific funding it
may be use</field></doc>
</add>
