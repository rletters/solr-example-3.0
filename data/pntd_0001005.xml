<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001005</field>
<field name="doi">10.1371/journal.pntd.0001005</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Abigail Weaver, Patrick Brown, Shannon Huey, Marco Magallon, E. Brennan Bollman, Dominique Mares, Thomas G. Streit, Marya Lieberman</field>
<field name="title">A Low-Tech Analytical Method for Diethylcarbamazine Citrate in Medicated Salt</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">2</field>
<field name="pages">e1005</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

The World Health Organization has called for an effort to eliminate Lymphatic
Filariasis (LF) around the world. In regions where the disease is endemic,
local production and distribution of medicated salt dosed with
diethylcarbamazine (DEC) has been an effective method for eradicating LF. A
partner of the Notre Dame Haiti program, Group SPES in Port-au-Prince, Haiti,
produces a medicated salt called Bon Sel. Coarse salt is pre-washed and
sprayed with a solution of DEC citrate and potassium iodate. Iodine levels are
routinely monitored on site by a titrimetric method. However, the factory had
no method for monitoring DEC. Critical analytical issues include 1)
determining whether the amount of DEC in each lot of Bon Sel is within safe
and therapeutically useful limits, 2) monitoring variability within and
between production runs, and 3) determining the effect of a common local
practice (washing salt before use) on the availability of DEC. This paper
describes a novel titrimetric method for analysis of DEC citrate in medicated
salt. The analysis needs no electrical power and requires only a balance,
volumetric glassware, and burets that most salt production programs have on
hand for monitoring iodine levels. The staff of the factory used this analysis
method on site to detect underloading of DEC on the salt by their sprayer and
to test a process change that fixed the problem.

Author Summary

As researchers develop more sophisticated technologies, parts of the world are
left behind. The front lines of fighting many diseases lie in regions where
expensive technology is not feasible. As part of the effort to eradicate
lymphatic filariasis in Haiti, our group&apos;s goal was to design an assay that
would allow a chemist, with basic equipment, to quantify the levels of
diethylcarbamazine citrate on medicated salt. With access to university
research facilities, we were able to devise and test a back-titration
procedure that can measure the medication levels with sufficient accuracy and
precision. Our method capitalized on the fact that the medication is acidic.
This characteristic allows us to combine an unknown, medicated salt sample
with a known quantity of base and then back-titrate with acid to determine
diethylcarbamazine citrate concentration based on the neutralization point.
Developing this protocol has put the power of quality control into the hands
of the Haitian factory producing the medicated salt. With the ability to
better monitor dosing levels, we have increased the effectiveness of this
program in Haiti. Using modern research facilities to produce effective, low-
tech methods could be a useful approach for tackling many worldwide medical
and environmental issues.

Introduction

The World Health Organization has called for an effort to eliminate Lymphatic
Filariasis (LF) around the world. [1] A nematode worm (Wuchereria bancrofti)
is the cause of 90% of lymphatic filariasis cases globally. Mosquito bites
transmit larval nematodes (microfilariae) present in the blood stream of
infected persons, and although the adult nematodes are resistant to medical
treatment, human transmission in endemic regions can be stopped by
administering drugs, such as diethylcarbamazine (DEC), that kill the
microfilariae. DEC has had a long</field></doc>
</add>
