<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001337</field>
<field name="doi">10.1371/journal.pntd.0001337</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Jessica Ingram, Giselle Knudsen, K. C. Lim, Elizabeth Hansell, Judy Sakanari, James McKerrow</field>
<field name="title">Proteomic Analysis of Human Skin Treated with Larval Schistosome Peptidases Reveals Distinct Invasion Strategies among Species of Blood Flukes</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">9</field>
<field name="pages">e1337</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Skin invasion is the initial step in infection of the human host by
schistosome blood flukes. Schistosome larvae have the remarkable ability to
overcome the physical and biochemical barriers present in skin in the absence
of any mechanical trauma. While a serine peptidase with activity against
insoluble elastin appears to be essential for this process in one species of
schistosomes, Schistosoma mansoni, it is unknown whether other schistosome
species use the same peptidase to facilitate entry into their hosts.

Methods

Recent genome sequencing projects, together with a number of biochemical
studies, identified alternative peptidases that Schistosoma japonicum or
Trichobilharzia regenti could use to facilitate migration through skin. In
this study, we used comparative proteomic analysis of human skin treated with
purified cercarial elastase, the known invasive peptidase of S. mansoni, or S.
mansoni cathespin B2, a close homolog of the putative invasive peptidase of S.
japonicum, to identify substrates of either peptidase. Select skin proteins
were then confirmed as substrates by in vitro digestion assays.

Conclusions

This study demonstrates that an S. mansoni ortholog of the candidate invasive
peptidase of S. japonicum and T. regenti, cathepsin B2, is capable of
efficiently cleaving many of the same host skin substrates as the invasive
serine peptidase of S. mansoni, cercarial elastase. At the same time,
identification of unique substrates and the broader species specificity of
cathepsin B2 suggest that the cercarial elastase gene family amplified as an
adaptation of schistosomes to human hosts.

Author Summary

Schistosome parasites are a major cause of disease in the developing world,
but the mechanism by which these parasites first infect their host has been
studied at the molecular level only for S. mansoni. In this paper, we have
mined recent genome annotations of S. mansoni and S. japonicum, a zoonotic
schistosome species, to identify differential expansion of peptidase gene
families that may be involved in parasite invasion and subsequent migration
through skin. Having identified a serine peptidase gene family in S. mansoni
and a cysteine peptidase gene family in S. japonicum, we then used a
comparative proteomic approach to identify potential substrates of
representative members of both classes of enzymes from S. mansoni in human
skin. The results of this study suggest that while these species evolved to
use different classes of peptidases in host invasion, both are capable of
cleaving components of the epidermis and dermal extracellular matrix, as well
as proteins involved in the host immune response against the migrating
parasite.

Introduction

Human skin is a formidable barrier for much of the microbial world. In
addition to the mechanical barrier of structural proteins in the epidermis,
basement membrane and dermal extracellular matrix, both the epidermis and
dermis are bathed in plasma proteins, including early sentinels of the immune
system [1]. In order to successfully breach this barrier, an invading pathogen
must degrade protein matrices while minimizing the immune response that it
elicits. To this end, many invading organisms utilize insect bites or other
mechanical</field></doc>
</add>
