<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000715</field>
<field name="doi">10.1371/journal.pntd.0000715</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Joseph Mathu Ndung'u, Sylvain Bieler, Giorgio Roscigno</field>
<field name="title">“Piggy-Backing” on Diagnostic Platforms Brings Hope to Neglected Diseases: The Case of Sleeping Sickness</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">5</field>
<field name="pages">e715</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Neglected infectious diseases (NIDs) attract little interest from commercial
companies that invest in diagnostics and therapeutics, mainly because the
people that they affect are amongst the poorest in the world, who cannot
afford to pay for them. Many commercial companies shy away from manufacturing
diagnostic tests for NIDs because a return on investment is not usually
guaranteed.

It is therefore not surprising that for a disease such as human African
trypanosomiasis (HAT), or sleeping sickness, no diagnostic test has ever been
manufactured under full registration by any regulatory agency. Tests that are
available today are produced by academic institutions, with no guarantee that
good manufacturing practice for in-vitro diagnostics (GMP-IVD) is adhered to.
The card agglutination test for trypanosomiasis (CATT), developed in 1978, is
the primary screening tool used in areas where Trypanosoma brucei gambiense is
endemic [1]. Detection of antibodies against trypanosomes using CATT is a
sensitive indicator of infection. However, in populations undergoing
screening, where prevalence of the disease is usually below 2% and specificity
of the CATT test is around 95%, a large number of positive results turn out to
be false-positives, and the positive predictive value of the test is not good
enough for it to be used on its own to guide treatment. The test is
manufactured using whole T. b. gambiense organisms recovered from infected
laboratory animals in a complex and risky process, has inferior sensitivity in
some disease foci, and can be performed only by trained personnel.
Furthermore, it is incapable of differentiating between active and cured
infections, as antibodies tend to stay in the blood for prolonged periods
after patients have been cured [2], [3]. What is worse, no similar test is
available for T. b. rhodesiense infection.

Until now, no successful attempt has been made to transform the CATT into a
single-format lateral flow test (LFT), which would make it more accessible to
diagnostics facilities. This could again be because a LFT for HAT provides
little promise for a return on investment, especially if it is to be delivered
at a price that is affordable to the public sector in endemic countries. Yet
for diseases that are comparatively more attractive, such as tuberculosis
(TB), HIV, malaria, and avian and swine flu, there has been more commercial
interest. In the late 1990s, intense lobbying by endemic countries, the World
Health Organization (WHO) and the international community resulted in a
paradigm shift, when at the beginning of this decade, the pharmaceutical
industry agreed to provide free drugs for HAT, preventing a potentially
embarrassing situation [4]. However, this goodwill could not be extended to
diagnostics as no company was manufacturing any tests for HAT.

A private foundation in Switzerland, the Foundation for Innovative New
Diagnostics (FIND), has devised a novel approach towards development of
diagnostic tests for NIDs that is generating a lot of interest in industry.
FIND, established in 2003, supports the development of diagnostic tests for
diseases of poverty, including TB, HAT, and malaria. The unique management
structure of FIND comprises diagnostics programmes that exist as independent
vertical business units, supported by expertise that cuts acr</field></doc>
</add>
