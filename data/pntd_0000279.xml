<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000279</field>
<field name="doi">10.1371/journal.pntd.0000279</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Peter J. Hotez</field>
<field name="title">Training the Next Generation of Global Health Scientists: A School of Appropriate Technology for Global Health</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">8</field>
<field name="pages">e279</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
In March of 2008, the Bill &amp; Melinda Gates Foundation made the impressive
announcement that it will accept proposals for a new Grand Challenges
Explorations program [1]. Grand Challenges Explorations will provide $100
million for global health scientists to identify new ways to protect against
infectious diseases (including neglected tropical diseases [NTDs]), to create
new drugs or delivery systems, to prevent or cure HIV/AIDS, and to explore the
basis of latency in tuberculosis [1]. In so doing, the Gates Foundation will
build on its long-standing multibillion dollar commitments to develop and test
new drugs, diagnostics, and vaccines for NTDs, as well as the better known
“big three” diseases, HIV/AIDS, tuberculosis, and malaria, and to fund
critically needed operational research in support of large-scale control
programs for these conditions [1]. The Gates Foundation is not alone—the
United Kingdom&apos;s Wellcome Trust has a £15 billion investment portfolio of
which a significant amount is devoted to global infectious diseases [2], while
the United States National Institutes of Health (NIH) also devotes a
significant amount of funding towards global health [3]. Therefore, in the
coming decade we can expect that these initiatives will contribute
significantly towards reducing the so-called 10/90 gap, a term coined by the
Global Forum for Health Research to refer to the finding that only 10% or less
of the global expenditure on medical research and development is directed
towards neglected health problems that disproportionately affect the poorest
people in developing regions of sub-Saharan Africa, Asia, and tropical regions
of the Americas.

While these new funded initiatives portend great promise for addressing the
global 10/90 gap, I am also concerned that there is an important piece missing
from these efforts that could undermine their success. I believe that at
several levels we are failing to train the next generation of global public
health scientists who either have the expertise to build these new
technologies or who will understand how to apply these new technologies to
public health practice [4]–[7]. The term appropriate technology has been used
to describe innovations, which are “developed, produced, delivered and
monitored within a comprehensive framework that takes into account the
systems, the individuals, and the community.” [7]. In a previous paper, my co-
authors and I cited as examples of appropriate technology the storied
successes of vaccines for polio and measles, and how because of their
appropriate use these diseases were eradicated from the Western Hemisphere; in
the case of smallpox vaccine, we have achieved global eradication [7]. In
contrast, the re-emergence of malaria in India and elsewhere as a consequence
of chloroquine drug resistance by the malaria parasite and resistance to DDT
by the mosquito vector are cited as examples of inappropriate use of
technology during the 1960s [7],[8]. My concern is that unless we train a new
generation of global health scientists to use appropriately the new Gates
Foundation–Wellcome Trust–NIH–Howard Hughes Medical Institute–funded
technologies, we risk seeing these impressive investments fail to materialize
into innovations that actually make a difference in the lives of poor people
in developing countries.

So exactly where</field></doc>
</add>
