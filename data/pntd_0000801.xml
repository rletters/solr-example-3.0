<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000801</field>
<field name="doi">10.1371/journal.pntd.0000801</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Yu Rong Yang, Gail M. Williams, Philip S. Craig, Donald P. McManus</field>
<field name="title">Impact of Increased Economic Burden Due to Human Echinococcosis in an Underdeveloped Rural Community of the People's Republic of China</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">9</field>
<field name="pages">e801</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Ningxia is located in western People&apos;s Republic of China, which is
hyperendemic for human cystic echinococcosis (CE) throughout the entire area
with alveolar echinococcosis (AE) hyperendemic in the south. This is in part
due to its underdeveloped economy. Despite the recent rapid growth in P.R.
China&apos;s economy, medical expenditure for hospitalization of echinococcosis
cases has become one of the major poverty generators in rural Ningxia,
resulting in a significant social problem.

Methodology/Principal Findings

We reviewed the 2000 inpatient records with liver CE in surgical departments
of hospitals from north, central and south Ningxia for the period 1996–2002.
We carried out an analysis of health care expenditure of inpatient treatment
in public hospitals, and examined the financial inequalities relating to human
echinococcosis and the variation in per capita income between various
socioeconomic groups with different levels of gross domestic product for
different years. Hospital charges for Yinchuan, NHAR&apos;s capital city in the
north, increased approximately 35-fold more than the annual income of rural
farmers with the result that they preferred to seek health care in local
county hospitals, despite higher quality and more efficient treatment and
diagnosis available in the city. Household income levels thus strongly
influenced the choice of health care provider and the additional expense
impeded access of poor people to better quality treatment.

Conclusions/Significance

Information on socioeconomic problems arising from echinococcosis, which adds
considerably to the burden on patient families and communities, needs to be
collected as a prerequisite for developing policies to tackle the disease in
rural Ningxia.

Author Summary

This paper compares medical expenditure for hospital treatment of
echinococcosis in NHAR, western People&apos;s Republic of China, for different
years, different regions and different socioeconomic groups. The results show
that the level of household income strongly influences health care decisions.
This study represents an effort to determine the effect of hospital charges
for inpatient treatment of echinococcosis on the choice of provider in NHAR,
and quantitatively examines this topic for the rural poor. The findings show
that low income individuals from rural areas opted to visit a local county
hospital rather than an urban hospital for hydatid surgery despite the
inferior infrastructure, personnel and general health care facilities
available. There are a number of policy implications. For example, enhancing
the quality and service of county hospitals in rural areas will benefit those
with lower incomes, thus improving access of rural residents to health
facilities for higher quality diagnosis and efficient treatment. Thus, we
advocate that government policy should be to increase investment in health
care in poor rural areas, and to launch relevant medical aid projects to help
those in poverty.

Introduction

Since the inception of market reforms in the early 1980s, the annual health
expenditure of People&apos;s Republic of China (P.R. China) has increased
consistently [1], [2]. But, contrary to this increase, two national healthcare
surveys [3], [4] showed that health ins</field></doc>
</add>
