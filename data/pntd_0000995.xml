<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000995</field>
<field name="doi">10.1371/journal.pntd.0000995</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Mushfiqur R. Tarafder, Hélène Carabin, Stephen T. McGarvey, Lawrence Joseph, Ernesto Balolong Jr., Remigio Olveda</field>
<field name="title">Assessing the Impact of Misclassification Error on an Epidemiological Association between Two Helminthic Infections</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">3</field>
<field name="pages">e995</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Polyparasitism can lead to severe disability in endemic populations. Yet, the
association between soil-transmitted helminth (STH) and the cumulative
incidence of Schistosoma japonicum infection has not been described. The aim
of this work was to quantify the effect of misclassification error, which
occurs when less than 100% accurate tests are used, in STH and S. japonicum
infection status on the estimation of this association.

Methodology/Principal Findings

Longitudinal data from 2276 participants in 50 villages in Samar province,
Philippines treated at baseline for S. japonicum infection and followed for
one year, served as the basis for this analysis. Participants provided 1–3
stool samples at baseline and 12 months later (2004–2005) to detect infections
with STH and S. japonicum using the Kato-Katz technique. Variation from day-
to-day in the excretion of eggs in feces introduces individual variations in
the sensitivity and specificity of the Kato-Katz to detect infection. Bayesian
logit models were used to take this variation into account and to investigate
the impact of misclassification error on the association between these
infections. Uniform priors for sensitivity and specificity of the diagnostic
test to detect the three STH and S. japonicum were used. All results were
adjusted for age, sex, occupation, and village-level clustering. Without
correction for misclassification error, the odds ratios (ORs) between
hookworm, Ascaris lumbricoides, and Trichuris trichiura, and S. japonicum
infections were 1.28 (95% Bayesian credible intervals: 0.93, 1.76), 0.91 (95%
BCI: 0.66, 1.26), and 1.11 (95% BCI: 0.80, 1.55), respectively, and 2.13 (95%
BCI: 1.16, 4.08), 0.74 (95% BCI: 0.43, 1.25), and 1.32 (95% BCI: 0.80, 2.27),
respectively, after correction for misclassification error for both exposure
and outcome.

Conclusions/Significance

The misclassification bias increased with decreasing test accuracy. Hookworm
infection was found to be associated with increased 12-month cumulative
incidence of S. japonicum infection after correction for misclassification
error. Such important associations might be missed in analyses which do not
adjust for misclassification errors.

Author Summary

Hookworm, roundworm, and whipworm are collectively known as soil-transmitted
helminths. These worms are prevalent in most of the developing countries along
with another parasitic infection called schistosomiasis. The tests commonly
used to detect infection with these worms are less than 100% accurate. This
leads to misclassification of infection status since these tests cannot always
correctly indentify infection. We conducted an epidemiological study where
such a test, the Kato-Katz technique, was used. In our study we tried to show
how misclassification error can influence the association between soil-
transmitted helminth infection and schistosomiasis in humans. We used a
statistical technique to calculate epidemiological measures of association
after correcting for the inaccuracy of the test. Our results show that there
is a major difference between epidemiological measures of association before
and after the correction of the inaccuracy of the test. After correction of
the inaccuracy of the test, soil</field></doc>
</add>
