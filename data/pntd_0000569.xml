<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000569</field>
<field name="doi">10.1371/journal.pntd.0000569</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Robert A. Harrison, Adam Hargreaves, Simon C. Wagstaff, Brian Faragher, David G. Lalloo</field>
<field name="title">Snake Envenoming: A Disease of Poverty</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">12</field>
<field name="pages">e569</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Most epidemiological and clinical reports on snake envenoming focus on a
single country and describe rural communities as being at greatest risk.
Reports linking snakebite vulnerability to socioeconomic status are usually
limited to anecdotal statements. The few reports with a global perspective
have identified the tropical regions of Asia and Africa as suffering the
highest levels of snakebite-induced mortality. Our analysis examined the
association between globally available data on snakebite-induced mortality and
socioeconomic indicators of poverty.

Methodology/Principal Findings

We acquired data on (i) the Human Development Index, (ii) the Per Capita
Government Expenditure on Health, (iii) the Percentage Labour Force in
Agriculture and (iv) Gross Domestic Product Per Capita from publicly available
databases on the 138 countries for which snakebite-induced mortality rates
have recently been estimated. The socioeconomic datasets were then plotted
against the snakebite-induced mortality estimates (where both datasets were
available) and the relationship determined. Each analysis illustrated a strong
association between snakebite-induced mortality and poverty.

Conclusions/Significance

This study, the first of its kind, unequivocally demonstrates that snake
envenoming is a disease of the poor. The negative association between
snakebite deaths and government expenditure on health confirms that the burden
of mortality is highest in those countries least able to deal with the
considerable financial cost of snakebite.

Author Summary

Every year snake envenoming kills more people in the tropics than some of the
world&apos;s recognised neglected tropical diseases (NTDs), including
schistosomiasis and leishmaniasis. While lacking the epidemic potential of an
infectious/vector-borne disease, snake envenoming in rural tropical
communities has as great a medical mortality, if not morbidity, as the NTDs.
The recent categorisation of snake envenoming as an NTD is an important
advance that hopefully will result in the wider recognition and allocation of
resources, particularly since death from snake envenoming is preventable;
antivenom is very effective when the appropriate antivenom is correctly
administered. Snake envenoming urgently requires international support to
instigate the epidemiological, health education, and effective treatment
initiatives that proved so potent in addressing the medical burden of NTDs
such as leprosy and dracunculosis. All the global estimates of snake
envenoming and deaths from snakebite indicate that mortality is highest in the
world&apos;s tropical countries. Here we examined associations between the globally
available data on (i) snakebite-induced mortality and (ii) socioeconomic
markers of poverty. Our data unequivocally establishes that snake envenoming
is globally associated with poverty, a distinctive characteristic of the
neglected tropical diseases.

Introduction

Our knowledge of the global medical burden of snakebite is limited to just a
few reports based primarily on either hospital records [1] or the
epidemiological literature [2],[3], and more recently, the latter in
combination with WHO mortality data [4]. Despite the nearly universal
distribution of venomous snakes (the South Pole, Greenland, New Zealand and
Madagascar being the major exceptions), each rep</field></doc>
</add>
