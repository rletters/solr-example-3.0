<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000755</field>
<field name="doi">10.1371/journal.pntd.0000755</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Narcis B. Kabatereine, Mwele Malecela, Mounir Lado, Sam Zaramba, Olga Amiel, Jan H. Kolaczinski</field>
<field name="title">How to (or Not to) Integrate Vertical Programmes for the Control of Major Neglected Tropical Diseases in Sub-Saharan Africa</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">6</field>
<field name="pages">e755</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Combining the delivery of multiple health interventions has the potential to
minimize costs and expand intervention coverage. Integration of mass drug
administration is therefore being encouraged for delivery of preventive
chemotherapy (PCT) to control onchocerciasis, lymphatic filariasis,
schistosomiasis, soil-transmitted helminthiasis, and trachoma in sub-Saharan
Africa, as there is considerable geographical overlap of these neglected
tropical diseases (NTDs). With only a handful of countries having embarked on
integrated NTD control, experience on how to develop and implement an
efficient integrated programme is limited. Historically, national and global
programmes were focused on the control of only one disease, usually through a
comprehensive approach that involved several interventions including PCT.
Overcoming the resulting disease-specific structures and thinking, and
ensuring that the integrated programme is embedded within the existing health
structures, pose considerable challenges to policy makers and implementers
wishing to embark on integrated NTD control. By sharing experiences from
Uganda, Tanzania, Southern Sudan, and Mozambique, this symposium article aims
to outlines key challenges and solutions to assist countries in establishing
efficient integrated NTD programmes.

The Challenge

Resources for disease control are limited and thus need to be used efficiently
[1]. This need is particularly apparent for the control of neglected tropical
diseases (NTDs), which, until recently, was largely unfunded [2]. Integration
of disease-specific programmes is therefore being encouraged for
onchocerciasis, lymphatic filariasis (LF), schistosomiasis, soil-transmitted
helminth (STH) infection, and trachoma [3], [4]. These NTDs occur over more or
less the same areas and their control depends, although not exclusively, on
regular mass drug administration (MDA) of safe and effective preventive
chemotherapy (PCT) [5]; combined PCT delivery should thus minimize costs and
increase coverage [6], [7]. However, “real world” experience of implementing
such integrated NTD control is limited [8], [9], making it difficult to decide
how best to embark on, and proceed with, the development of an integrated NTD
control programme in a manner that promotes efficiency and local
ownership—both prerequisites for sustainability.

Tutorial

What is integrated NTD control?

Integrated delivery of health services covers a range of approaches and the
definition is therefore context dependent [10]–[12]. Here we focus on a group
of NTDs for which regular MDA of PCT is key to effective control, namely
onchocerciasis, LF, schistosomiasis, STH infection, and trachoma. In this
context, integration is usually applied to creation of “PCT packages” by
combining MDA for more than one NTD. Some countries have formed umbrella NTD
programmes to oversee vertical delivery of these packages through campaigns or
other channels. In other settings, a more “horizontal” approach is applied
whereby intervention packages are delivered as part of routine health care and
education programmes. Both approaches provide opportunities for “integration”
[13], [14] and are by no means mutually exclusive [15]. Instead they should be
coordinated and c</field></doc>
</add>
