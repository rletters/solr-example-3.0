<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001499</field>
<field name="doi">10.1371/journal.pntd.0001499</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Yasmin Moolani, Gene Bukhman, Peter J. Hotez</field>
<field name="title">Neglected Tropical Diseases as Hidden Causes of Cardiovascular Disease</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">6</field>
<field name="pages">e1499</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
An important component of the burden of cardiovascular disease in low- and
middle-income countries may be attributed to the neglected tropical diseases.

There is a growing awareness of the importance of chronic non-communicable
diseases (CNCDs) in the world&apos;s low- and middle-income countries (LMICs).
Beginning in the 1990s, Murray and Lopez predicted a doubling of death rates
due to cardiovascular disease in developing countries by 2020 [1], while a
substantial rise was also predicted by Leeder et al. [2]. Based on World
Health Organization (WHO) predictions, 75% of the burden of cardiovascular
disease is found in LMICs [3]. Alarming increases have also been noted for
other CNCDs in LMICs including cancer, chronic respiratory diseases, and
diabetes [4]. In September 2011, a report by the World Economic Forum and the
Harvard School of Public Health estimated the global economic burden of CNCDs
over the next two decades to be US$47 trillion [5]. During this same month,
the United Nations General Assembly held a high-level meeting to discuss
prevention and control of CNCDs, including cardiovascular diseases, in LMICs
[6]. These initiatives have focused on preventable risk factors attributable
to lifestyle changes such as tobacco and alcohol use, prolonged unhealthy
nutrition, and physical inactivity, which currently account for a high
proportion of cardiovascular deaths in North America and Europe [4]–[6].

While there is no question that obesity, tobacco, and alcohol represent major
underlying conditions responsible for the rise of cardiovascular and other
CNCDs in LMICs, they do not provide a complete picture. In March of 2011,
Partners in Health and Harvard Medical School sponsored a conference entitled
“The Long Tail of Global Health Equity: Tackling the Endemic Non-Communicable
Diseases of the Bottom Billion” to examine in more detail some of the
neglected causes of CNCDs, particularly those that are unique to the world&apos;s
poorest people in LMICs. The conference highlighted important risk factors
apart from the lifestyle changes linked to CNCDs in high-income countries [7].
Specifically with respect to neglected populations, an important component of
cardiovascular disease may be attributable to neglected tropical diseases
(NTDs) and other infections of poverty. For example, the Heart of Soweto Study
from South Africa identified rheumatic heart disease, tuberculosis, and HIV as
significant contributors to heart disease and more common than coronary artery
disease. Even in the urbanized region of Soweto where there is a high
prevalence of vascular risk factors, non-ischemic etiologies are still the
dominant cause of heart failure [8], [9].

On a global level, the contribution of infections of poverty to heart disease
can be seen in analyzing the Global Burden of Disease estimates from the WHO.
According to this data, approximately 8.8% of the disability-adjusted life
years (DALYs) of LMICs may be attributable to cardiovascular disease [3].
Almost one-half of this cardiovascular disease burden is attributable to
ischemic heart disease, more than one-third to cerebrovascular disease, and
the remainder to hypertensive and inflammatory causes, as well as rheumatic
heart disease (Figure 1). A detailed analysis of these conditions suggests
that NTDs and other neglected infections may account for a significant
compo</field></doc>
</add>
