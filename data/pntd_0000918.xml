<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000918</field>
<field name="doi">10.1371/journal.pntd.0000918</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Denise G. Martinez-Lopez, Mark Fahey, Jenifer Coburn</field>
<field name="title">Responses of Human Endothelial Cells to Pathogenic and Non-Pathogenic Leptospira Species</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">12</field>
<field name="pages">e918</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Leptospirosis is a widespread zoonotic infection that primarily affects
residents of tropical regions, but causes infections in animals and humans in
temperate regions as well. The agents of leptospirosis comprise several
members of the genus Leptospira, which also includes non-pathogenic,
saprophytic species. Leptospirosis can vary in severity from a mild, non-
specific illness to severe disease that includes multi-organ failure and
widespread endothelial damage and hemorrhage. To begin to investigate how
pathogenic leptospires affect endothelial cells, we compared the responses of
two endothelial cell lines to infection by pathogenic versus non-pathogenic
leptospires. Microarray analyses suggested that pathogenic L. interrogans and
non-pathogenic L. biflexa triggered changes in expression of genes whose
products are involved in cellular architecture and interactions with the
matrix, but that the changes were in opposite directions, with infection by L.
biflexa primarily predicted to increase or maintain cell layer integrity,
while L. interrogans lead primarily to changes predicted to disrupt cell layer
integrity. Neither bacterial strain caused necrosis or apoptosis of the cells
even after prolonged incubation. The pathogenic L. interrogans, however, did
result in significant disruption of endothelial cell layers as assessed by
microscopy and the ability of the bacteria to cross the cell layers. This
disruption of endothelial layer integrity was abrogated by addition of the
endothelial protective drug lisinopril at physiologically relevant
concentrations. These results suggest that, through adhesion of L. interrogans
to endothelial cells, the bacteria may disrupt endothelial barrier function,
promoting dissemination of the bacteria and contributing to severe disease
manifestations. In addition, supplementing antibiotic therapy with lisinopril
or derivatives with endothelial protective activities may decrease the
severity of leptospirosis.

Author Summary

Leptospirosis is a widespread zoonotic infection that primarily affects
residents of tropical regions, but is seen occasionally in temperate regions
as well. Leptospirosis can vary in severity from a mild, non-specific illness
to severe disease that includes multi-organ failure and widespread endothelial
damage and hemorrhage. To investigate how pathogenic leptospires affect
endothelial cells, we compared the responses of two endothelial cell lines to
infection by pathogenic versus non-pathogenic leptospires. Our analyses
suggested that pathogenic L. interrogans and non-pathogenic L. biflexa caused
changes in expression of genes whose products are involved in cellular
architecture and interactions with the matrix, but that the changes were in
opposite directions, with infection by L. biflexa primarily maintaining cell
layer integrity, while L. interrogans disrupted cell layers. In fact, L.
interrogans caused significant disruption of endothelial cell layers, but this
damage could be abrogated by the endothelial protective drug lisinopril. Our
results suggest that L. interrogans binds to endothelial cells and disrupts
endothelial barrier function, which may promote dissemination of the bacteria
and contribute to severe disease manifestations. This disruption may be slowed
by endothelial-protective drugs to decrease damage in leptospirosis</field></doc>
</add>
