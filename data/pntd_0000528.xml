<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000528</field>
<field name="doi">10.1371/journal.pntd.0000528</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Ross A. Paveley, Sarah A. Aynsley, Peter C. Cook, Joseph D. Turner, Adrian P. Mountford</field>
<field name="title">Fluorescent Imaging of Antigen Released by a Skin-Invading Helminth Reveals Differential Uptake and Activation Profiles by Antigen Presenting Cells</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">10</field>
<field name="pages">e528</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Infection of the mammalian host by the parasitic helminth Schistosoma mansoni
is accompanied by the release of excretory/secretory molecules (ES) from
cercariae which aid penetration of the skin. These ES molecules are potent
stimulants of innate immune cells leading to activation of acquired immunity.
At present however, it is not known which cells take up parasite antigen, nor
its intracellular fate. Here, we develop a technique to label live infectious
cercariae which permits the imaging of released antigens into macrophages (MΦ)
and dendritic cells (DCs) both in vitro and in vivo. The amine reactive tracer
CFDA-SE was used to efficiently label the acetabular gland contents of
cercariae which are released upon skin penetration. These ES products, termed
‘0-3hRP’, were phagocytosed by MHC-II+ cells in a Ca+ and actin-dependent
manner. Imaging of a labelled cercaria as it penetrates the host skin over 2
hours reveals the progressive release of ES material. Recovery of cells from
the skin shows that CFDA-SE labelled ES was initially (3 hrs) taken up by Gr1
+MHC-II− neutrophils, followed (24 hrs) by skin-derived F4/80+MHC-IIlo MΦ and
CD11c+ MHC-IIhi DC. Subsequently (48 hrs), MΦ and DC positive for CFDA-SE were
detected in the skin-draining lymph nodes reflecting the time taken for
antigen-laden cells to reach sites of immune priming. Comparison of in vitro-
derived MΦ and DC revealed that MΦ were slower to process 0-3hRP, released
higher quantities of IL-10, and expressed a greater quantity of arginase-1
transcript. Combined, our observations on differential uptake of cercarial ES
by MΦ and DC suggest the development of a dynamic but ultimately balanced
response that can be potentially pushed towards immune priming (via DC) or
immune regulation (via MΦ).

Author Summary

Schistosomiasis is caused by the parasitic worm Schistosoma with over 200
million people infected across 76 countries. The parasitic larvae (called
cercariae) infect mammalian hosts via the skin, but the exact mechanisms by
which dermal cells interact with molecules released by invading larvae are
unclear. A better understanding of the infection process and stimulation of
the early immune response would thus enable a targeted approach towards the
development of drugs and vaccines. Here, we have used the fluorescent tracer
CFDA-SE to label infectious cercariae and, together with confocal microscopy,
have for the first time tracked in real time the parasite infecting via the
epidermis and depositing excretory/secretory material in its wake. Phagocytic
macrophages and dendritic cells in the skin internalised excretory/secretory
molecules released by the larvae, and both cell types were subsequently
located in the draining lymph nodes where priming of the acquired immune
response occurs. In vitro studies determined that macrophages were slower to
process released parasite material than dendritic cells; they also secreted
lower levels of pro-inflammatory cytokines but greater quantities of
regulatory IL-10. The relative abundance of macrophages versus dendritic cells
in the skin infection site and their differential rates of antigen processing
may be crucial in determining the success of adaptive immune priming in
response </field></doc>
</add>
