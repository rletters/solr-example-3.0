<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001204</field>
<field name="doi">10.1371/journal.pntd.0001204</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Asha Jayakumar, Tiago M. Castilho, Esther Park, Karen Goldsmith-Pestana, Jenefer M. Blackwell, Diane McMahon-Pratt</field>
<field name="title">TLR1/2 Activation during Heterologous Prime-Boost Vaccination (DNA-MVA) Enhances CD8+ T Cell Responses Providing Protection against Leishmania (Viannia)</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">6</field>
<field name="pages">e1204</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Leishmania (Viannia) parasites present particular challenges, as human and
murine immune responses to infection are distinct from other Leishmania
species, indicating a unique interaction with the host. Further, vaccination
studies utilizing small animal models indicate that modalities and antigens
that prevent infection by other Leishmania species are generally not
protective.

Methodology

Using a newly developed mouse model of chronic L. (Viannia) panamensis
infection and the heterologous DNA prime – modified vaccinia virus Ankara
(MVA) boost vaccination modality, we examined whether the conserved vaccine
candidate antigen tryparedoxin peroxidase (TRYP) could provide protection
against infection/disease.

Results

Heterologous prime – boost (DNA/MVA) vaccination utilizing TRYP antigen can
provide protection against disease caused by L. (V.) panamensis. However,
protection is dependent on modulating the innate immune response using the
TLR1/2 agonist Pam3CSK4 during DNA priming. Prime-boost vaccination using DNA
alone fails to protect. Prior to infection protectively vaccinated mice
exhibit augmented CD4 and CD8 IFNγ and memory responses as well as decreased
IL-10 and IL-13 responses. IL-13 and IL-10 have been shown to be independently
critical for disease in this model. CD8 T cells have an essential role in
mediating host defense, as CD8 depletion reversed protection in the vaccinated
mice; vaccinated mice depleted of CD4 T cells remained protected. Hence,
vaccine-induced protection is dependent upon TLR1/2 activation instructing the
generation of antigen specific CD8 cells and restricting IL-13 and IL-10
responses.

Conclusions

Given the general effectiveness of prime-boost vaccination, the recalcitrance
of Leishmania (Viannia) to vaccine approaches effective against other species
of Leishmania is again evident. However, prime-boost vaccination modality can
with modulation induce protective responses, indicating that the delivery
system is critical. Moreover, these results suggest that CD8 T cells should be
targeted for the development of a vaccine against infection caused by
Leishmania (Viannia) parasites. Further, TLR1/2 modulation may be useful in
vaccines where CD8 T cell responses are critical.

Author Summary

Leishmania (Viannia) are the predominant agents of leishmaniasis in Latin
America. Given the fact that leishmaniasis is a zoonosis, eradication is
unlikely; a vaccine could provide effective prevention of disease. However,
these parasites present a challenge and we do not fully understand what
elements of the host immune defense prevent disease. We examined the ability
of vaccination to protect against L. (Viannia) infection using the highly
immunogenic heterologous prime-boost (DNA-modified vaccinia virus) modality
and a single Leishmania antigen (TRYP). Although this mode of vaccination can
induce protection against other leishmaniases (cutaneous, visceral), no
protection was observed against L. (V.) panamensis. However, we found that if
the vaccination was modified and the innate immune response was activated
through Toll-like receptor1/2(TLR1/2) during the DNA priming, vaccinated mice
were protected. Protection was dependent on C</field></doc>
</add>
