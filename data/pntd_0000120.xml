<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000120</field>
<field name="doi">10.1371/journal.pntd.0000120</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Paul N. Levett</field>
<field name="title">Sequence-Based Typing of Leptospira: Epidemiology in the Genomic Era</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2007</field>
<field name="volume">1</field>
<field name="number">2</field>
<field name="pages">e120</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Background

Leptospirosis is a zoonotic disease of global distribution, maintained in
nature by chronic renal infection of carrier animals. Infected animals excrete
the organisms in their urine and contaminate the environment. Human infection
is acquired directly via exposure to the organism in tissues or body fluids,
but most often occurs indirectly via exposure to the organisms in the
environment. Thus human exposure usually occurs remotely from the reservoir or
source animal.

The incidence of leptospirosis is very strongly associated with rainfall.
Excess rainfall events that cause massive flooding are associated with the
potential for huge outbreaks, particularly in densely populated regions of the
tropical world. In such situations it is difficult if not impossible to
identify the reservoir(s) of the organisms that cause human cases.

Historically, leptospires were classified into two species, Leptospira
interrogans and L. biflexa, which comprised pathogenic and nonpathogenic
strains, respectively. Within these two species, several hundred serovars were
recognised [1]. Identification of isolates to serovar level is an essential
step to understanding the epidemiology of the disease in both humans and
animals in any geographic region. However, serovar identification remains a
relatively blunt tool with which to investigate fine details of epidemiology.

Genetic variation observed within the genus Leptospira [2],[3] led to the
replacement of the historical classification by one based on DNA relatedness
[4],[5]. In this system 14 species are currently recognised and at least
another six species are in the process of being described. The development of
a molecular classification provided the basis for understanding the
genetically distinct subtypes of serovar Hardjo. Two genotypes of serovar
Hardjo, with distinct biological characteristics and geographical
distribution, named Hardjoprajitno and Hardjobovis [6]–[8] are classified
within L. interrogans and L. borgpetersenii, respectively. Evidence of
horizontal gene transfer within the genus, mediated by insertion sequences, is
widespread [9]–[12].

A wide range of methods based upon restriction endonuclease digests, random
amplification, or hybridization have been applied to typing of leptospiral
isolates, with moderate success. The availability of increasing numbers of
genome sequences has facilitated the application of sequence-based approaches
that can yield much deeper information about relationships between strains
[13]–[15].

A New MLST Study

In recent years Thailand has undergone a significant outbreak of leptospirosis
[16],[17]. From 1996 to 2000, reported cases increased approximately 40-fold
to a peak of 14,000 cases per year [17]. In the absence of climatic or
behavioural changes to account for the outbreak, hypotheses for this
relatively sudden, explosive increase in incidence include the emergence of a
clone of Leptospira better equipped for survival in a maintenance host or in
the environment, or one of greater virulence. Serological identification of
isolates indicated that many were of L. interrogans serovar Autumnalis. The
recently published paper by Thaipadungpanit and colleagues in PLoS Neglected
Tropical Diseases [18] brought the discriminatory power of sequence-based
typing to bear on the epidemiology of this rapidly emerging disease.

In this study, a multilocus sequence typing (</field></doc>
</add>
