<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000814</field>
<field name="doi">10.1371/journal.pntd.0000814</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Raida Petrela, Loreta Kuneshka, Eli Foto, Ferit Zavalani, Luigi Gradoni</field>
<field name="title">Pediatric Visceral Leishmaniasis in Albania: A Retrospective Analysis of 1,210 Consecutive Hospitalized Patients (1995–2009)</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">9</field>
<field name="pages">e814</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Little information is available about infantile visceral leishmaniasis (VL) in
Albania as regards incidence, diagnosis and management of the disease.

Methodology/Principal Findings

Demographic data, clinical and laboratory features and therapeutic findings
were considered in children admitted to University Hospital of Tirana from
1995 to 2009, and diagnosed as having VL. The diagnosis was based on bone-
marrow microscopy/culture in 77.5% of patients, serology in 16.1%, and ex
juvantibus in 6.4%. A total of 1,210 children were considered, of whom 74%
came from urbanized areas. All patients were in the age range 0–14 years, with
a median of 4 years. Hepatosplenomegaly was recorded in 100%, fever in 95.4%
and moderate to severe anemia in 88% of cases. Concomitant conditions were
frequent: 84% had bronchopneumonia; diarrhea was present in 27%, with acute
manifestations in 5%; 3% had salmonellosis. First-line therapy was meglumine
antimoniate for all patients, given at the standard Sbv dosage of 20 mg/kg/day
for 21 to 28 days. Two children died under treatment, one of sepsis, the other
of acute renal impairment. There were no cases of primary unresponsiveness to
treatment, and only 8 (0.67%) relapsed within 6–12 months after therapy. These
patients have been re-treated with liposomal amphotericin B, with successful
cure.

Conclusions

Visceral leishmaniasis in pediatric age is relatively frequent in Albania;
therefore an improvement is warranted of a disease-specific surveillance
system in this country, especially as regards diagnosis. Despite recent
reports on decreased responses to antimonial drugs of patients with
Mediterranean VL, meglumine antimoniate treatment appears to be still highly
effective in Albania.

Author Summary

Albania is a developing country that is rapidly improving in social, economic
and sanitary conditions. The health care system in still in progress and the
impact of some infectious diseases remains poorly understood. In particular,
little information is available on incidence, clinical features and response
to treatment of visceral leishmaniasis (VL) in childhood. We performed a
retrospective analysis of data recorded from 1995 to 2009 at the national
pediatric reference hospital of Tirana where any child suspected for VL is
referred for specific diagnosis and treatment. Epidemiology, clinical features
and management of the disease were considered. The main findings can be
summarized as follows: i) The incidence of the disease in Albanian children
(25/100,000 in the age group 0–6 years) is much higher than in developed
Mediterranean countries endemic for VL; ii) The disease is associated with
poor sanitary conditions as suggested by the high rate of severe clinical
features and frequency of co-morbidities; iii) The cheapest drug available for
Mediterranean VL treatment (meglumine antimoniate) is highly effective (99%
full cure rate) and well tolerated. Limitations were identified in the low
standard laboratory diagnostic capability and unsatisfactory medical
surveillance in less urbanized areas. An improvement is warranted of a
disease-specific surveillance system in Albania.

Introduction

Zoonotic visceral leishmaniasis (VL) is a disseminated protozoan infection
transmitted by phlebotomine sa</field></doc>
</add>
