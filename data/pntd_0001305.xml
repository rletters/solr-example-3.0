<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001305</field>
<field name="doi">10.1371/journal.pntd.0001305</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Caroline J. Lavender, Janet A. M. Fyfe, Joseph Azuolas, Karen Brown, Rachel N. Evans, Lyndon R. Ray, Paul D. R. Johnson</field>
<field name="title">Risk of Buruli Ulcer and Detection of Mycobacterium ulcerans in Mosquitoes in Southeastern Australia</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">9</field>
<field name="pages">e1305</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Buruli ulcer (BU) is a destructive skin condition caused by infection with the
environmental bacterium, Mycobacterium ulcerans. The mode of transmission of
M. ulcerans is not completely understood, but several studies have explored
the role of biting insects. In this study, we tested for an association
between the detection of M. ulcerans in mosquitoes and the risk of BU disease
in humans in an endemic area of southeastern Australia.

Methodology/Principal Findings

Adult mosquitoes were trapped in seven towns on the Bellarine Peninsula in
Victoria, Australia, from December 2004 to December 2009 and screened for M.
ulcerans by real-time PCR. The number of laboratory-confirmed cases of BU in
permanent residents of these towns diagnosed during the same period was
tallied to determine the average cumulative incidence of BU in each location.
Pearson&apos;s correlation coefficient (r) was calculated for the proportion of M.
ulcerans-positive mosquitoes per town correlated with the incidence of BU per
town. We found a strong dose-response relationship between the detection of M.
ulcerans in mosquitoes and the risk of human disease (r, 0.99; 95% CI,
0.92–0.99; p&lt;0.001).

Conclusions/Significance

The results of this study strengthen the hypothesis that mosquitoes are
involved in the transmission of M. ulcerans in southeastern Australia. This
has implications for the development of intervention strategies to control and
prevent BU.

Author Summary

Buruli ulcer (BU) is a destructive skin condition caused by infection with the
environmental bacterium, Mycobacterium ulcerans. BU has been reported in more
than 30 countries in Africa, the Americas, Asia and the Western Pacific. How
people become infected with M. ulcerans is not completely understood, but
numerous studies have explored the role of biting insects. In 2007, it was
discovered that M. ulcerans could be detected in association with mosquitoes
trapped in one town in southeastern Australia during a large outbreak of BU.
In the present study we investigated whether there was a relationship between
the incidence of BU in humans in several towns and the likelihood of detecting
M. ulcerans in mosquitoes trapped in those locations. We found a strong
association between the proportion of M. ulcerans-positive mosquitoes and the
incidence of human disease. The results of this study strengthen the
hypothesis that mosquitoes are involved in the transmission of M. ulcerans in
southeastern Australia. This has implications for the development of
strategies to control and prevent BU.

Introduction

Mycobacterium ulcerans is an environmental pathogen that causes Buruli ulcer
(BU), a slowly destructive infection of skin and soft tissue that can leave
sufferers permanently disabled if not treated appropriately [1]. Classified by
the World Health Organization (WHO) as a neglected tropical disease, BU has
been reported in more than 30 countries in Africa, the Americas, Asia and the
Western Pacific, mainly with tropical and subtropical climates. Australia is
the only developed country with significant local transmission of BU. Foci of
infection have been described in tropical Far North Queensland [2], the
Capricorn Coast region of central Queensland [3], the Northern T</field></doc>
</add>
