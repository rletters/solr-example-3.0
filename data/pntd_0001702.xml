<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001702</field>
<field name="doi">10.1371/journal.pntd.0001702</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Diana N. J. Lockwood, Peter Nicholls, W. Cairns S. Smith, Loretta Das, Pramila Barkataki, Wim van Brakel, Sujai Suneetha</field>
<field name="title">Comparing the Clinical and Histological Diagnosis of Leprosy and Leprosy Reactions in the INFIR Cohort of Indian Patients with Multibacillary Leprosy</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">6</field>
<field name="pages">e1702</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The ILEP Nerve Function Impairment in Reaction (INFIR) is a cohort study
designed to identify predictors of reactions and nerve function impairment in
leprosy. The aim was to study correlations between clinical and histological
diagnosis of reactions.

Methodology/Principal Findings

Three hundred and three newly diagnosed patients with World Health
Organization multibacillary (MB) leprosy from two centres in India were
enrolled in the study. Skin biopsies taken at enrolment were assessed using a
standardised proforma to collect data on the histological diagnosis of
leprosy, leprosy reactions and the certainty level of the diagnosis. The
pathologist diagnosed definite or probable Type 1 Reactions (T1R) in 113 of
265 biopsies from patients at risk of developing reactions whereas clinicians
diagnosed skin only reactions in 39 patients and 19 with skin and nerve
involvement. Patients with Borderline Tuberculoid (BT) leprosy had a clinical
diagnosis rate of reactions of 43% and a histological diagnosis rate of 61%;
for patients with Borderline Lepromatous (BL) leprosy the clinical and
histological diagnosis rates were 53.7% and 46.2% respectively. The
sensitivity and specificity of clinical diagnosis for T1R was 53.1% and 61.9%
for BT patients and 61.1% and 71.0% for BL patients. Erythema Nodosum Leprosum
(ENL) was diagnosed clinically in two patients but histologically in 13
patients. The Ridley-Jopling classification of patients (n = 303) was 42.8%
BT, 27.4% BL, 9.4% Lepromatous Leprosy (LL), 13.0% Indeterminate and 7.4% with
non-specific inflammation. This data shows that MB classification is very
heterogeneous and encompasses patients with no detectable bacteria and high
immunological activity through to patients with high bacterial loads.

Conclusions/Significance

Leprosy reactions may be under-diagnosed by clinicians and increasing biopsy
rates would help in the diagnosis of reactions. Future studies should look at
sub-clinical T1R and ENL and whether they have impact on clinical outcomes.

Author Summary

Leprosy affects skin and peripheral nerves. Although we have antibiotics to
treat the mycobacterial infection, the accompanying inflammation is a major
part of the disease process. This can worsen after starting antibacterial
treatment with episodes of immune mediated inflammation, so called reactions.
These are associated with worsening of nerve damage. However, diagnosing these
reactions is not straightforward. They can be diagnosed clinically by
examination or by microscopic examination of the skin biopsies. We studied a
cohort of 303 newly diagnosed leprosy patients in India and compared the
diagnosis rates by clinical examination and microscopy and found that the
microscopic diagnosis has higher rates of diagnosis for both types of
reaction. This suggests that clinicians and pathologists have different
thresholds for diagnosing reactions. More work is needed to optimise both
clinical and pathological diagnosis. In this cohort 43% of patients had
Borderline Tuberculoid leprosy, an immunologically active type, and 20% of the
biopsies showed only minimal inflammation, perhaps these patients had very
early disease or self-healing. The public </field></doc>
</add>
