<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001266</field>
<field name="doi">10.1371/journal.pntd.0001266</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Naomi A. Dyer, Sophie Ravel, Kwang-Shik Choi, Alistair C. Darby, Sandrine Causse, Berisha Kapitano, Martin J. R. Hall, Keith Steen, Pascal Lutumba, Joules Madinga, Steve J. Torr, Loyce M. Okedi, Michael J. Lehane, Martin J. Donnelly</field>
<field name="title">Cryptic Diversity within the Major Trypanosomiasis Vector Glossina fuscipes Revealed by Molecular Markers</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">8</field>
<field name="pages">e1266</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The tsetse fly Glossina fuscipes s.l. is responsible for the transmission of
approximately 90% of cases of human African trypanosomiasis (HAT) or sleeping
sickness. Three G. fuscipes subspecies have been described, primarily based
upon subtle differences in the morphology of their genitalia. Here we describe
a study conducted across the range of this important vector to determine
whether molecular evidence generated from nuclear DNA (microsatellites and
gene sequence information), mitochondrial DNA and symbiont DNA support the
existence of these taxa as discrete taxonomic units.

Principal Findings

The nuclear ribosomal Internal transcribed spacer 1 (ITS1) provided support
for the three subspecies. However nuclear and mitochondrial sequence data did
not support the monophyly of the morphological subspecies G. f. fuscipes or G.
f. quanzensis. Instead, the most strongly supported monophyletic group was
comprised of flies sampled from Ethiopia. Maternally inherited loci (mtDNA and
symbiont) also suggested monophyly of a group from Lake Victoria basin and
Tanzania, but this group was not supported by nuclear loci, suggesting
different histories of these markers. Microsatellite data confirmed strong
structuring across the range of G. fuscipes s.l., and was useful for deriving
the interrelationship of closely related populations.

Conclusion/Significance

We propose that the morphological classification alone is not used to classify
populations of G. fuscipes for control purposes. The Ethiopian population,
which is scheduled to be the target of a sterile insect release (SIT)
programme, was notably discrete. From a programmatic perspective this may be
both positive, given that it may reflect limited migration into the area or
negative if the high levels of differentiation are also reflected in
reproductive isolation between this population and the flies to be used in the
release programme.

Author Summary

Glossina fuscipes s.l. tsetse flies are responsible for transmission of
approximately 90% of the cases of Human African Typanosomiasis in Sub Saharan
Africa. It was previously proposed on the basis of morphology that G. fuscipes
is composed of three sub-species. Using genetic evidence from G. fuscipes
nuclear, mitochondrial and symbiont DNA, we show that the morphological
subspecies do not correspond well to genetic differences between the flies and
morphologically similar flies may have arisen more than once in the evolution
of this species. Instead, we found at least 5 main allopatrically distributed
groups of G. fuscipes flies. The most genetically distinct group of flies
originated from Ethiopia, where a sterile insect release programme is planned.
Given that tsetse control often exploits species-specific behaviours there is
a pressing need to establish the taxonomic status and ranges of these five
groups. Moreover given that we were only able to perform limited sampling in
many parts of the species distribution further groups within G. fuscipes are
likely to be awaiting discovery.

Introduction

Control of Human African Trypansomiasis (HAT) has largely been based upon the
detection an</field></doc>
</add>
