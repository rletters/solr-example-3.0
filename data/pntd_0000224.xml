<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000224</field>
<field name="doi">10.1371/journal.pntd.0000224</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Mary J. Warrell, Anna Riddell, Ly-Mee Yu, Judith Phipps, Linda Diggle, Hervé Bourhy, Jonathan J. Deeks, Anthony R. Fooks, Laurent Audry, Sharon M. Brookes, François-Xavier Meslin, Richard Moxon, Andrew J. Pollard, David A. Warrell</field>
<field name="title">A Simplified 4-Site Economical Intradermal Post-Exposure Rabies Vaccine Regimen: A Randomised Controlled Comparison with Standard Methods</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">4</field>
<field name="pages">e224</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The need for economical rabies post-exposure prophylaxis (PEP) is increasing
in developing countries. Implementation of the two currently approved
economical intradermal (ID) vaccine regimens is restricted due to confusion
over different vaccines, regimens and dosages, lack of confidence in
intradermal technique, and pharmaceutical regulations. We therefore compared a
simplified 4-site economical PEP regimen with standard methods.

Methods

Two hundred and fifty-four volunteers were randomly allocated to a single
blind controlled trial. Each received purified vero cell rabies vaccine by one
of four PEP regimens: the currently accepted 2-site ID; the 8-site regimen
using 0.05 ml per ID site; a new 4-site ID regimen (on day 0, approximately
0.1 ml at 4 ID sites, using the whole 0.5 ml ampoule of vaccine; on day 7, 0.1
ml ID at 2 sites and at one site on days 28 and 90); or the standard 5-dose
intramuscular regimen. All ID regimens required the same total amount of
vaccine, 60% less than the intramuscular method. Neutralising antibody
responses were measured five times over a year in 229 people, for whom
complete data were available.

Findings

All ID regimens showed similar immunogenicity. The intramuscular regimen gave
the lowest geometric mean antibody titres. Using the rapid fluorescent focus
inhibition test, some sera had unexpectedly high antibody levels that were not
attributable to previous vaccination. The results were confirmed using the
fluorescent antibody virus neutralisation method.

Conclusions

This 4-site PEP regimen proved as immunogenic as current regimens, and has the
advantages of requiring fewer clinic visits, being more practicable, and
having a wider margin of safety, especially in inexperienced hands, than the
2-site regimen. It is more convenient than the 8-site method, and can be used
economically with vaccines formulated in 1.0 or 0.5 ml ampoules. The 4-site
regimen now meets all requirements of immunogenicity for PEP and can be
introduced without further studies.

Trial Registration

Controlled-Trials.com ISRCTN 30087513

Author Summary

All human deaths from rabies result from failure to give adequate prophylaxis.
After a rabid animal bite, immediate wound cleaning, rabies vaccine and
immunoglobulin injections effectively prevent fatal infection. Immunoglobulin
is very rarely available in developing countries, where prevention relies on
efficacious vaccine. WHO approved vaccines are prohibitively expensive, but 2
economical regimens (injecting small amounts of vaccine intradermally, into
the skin, at 2 or 8 sites on the first day of the course) have been used for
many years in a few places. Practical or perceived difficulties have
restricted widespread uptake of economical methods. These could largely be
overcome by introducing a new, simpler regimen, involving 4 site injections on
the first day. We vaccinated volunteers to compare the antibody levels induced
by the 4-site intradermal regimen with those induced by the current 2-site and
8-site regimens and the “gold standard” intramuscular regimen favoured
internationally. A</field></doc>
</add>
