<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000910</field>
<field name="doi">10.1371/journal.pntd.0000910</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Cinthia A. Acka, Giovanna Raso, Eliézer K. N'Goran, Andres B. Tschannen, Isaac I. Bogoch, Essane Séraphin, Marcel Tanner, Brigit Obrist, Jürg Utzinger</field>
<field name="title">Parasitic Worms: Knowledge, Attitudes, and Practices in Western Côte d’Ivoire with Implications for Integrated Control</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">12</field>
<field name="pages">e910</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

In the developing world where parasitic worm infections are pervasive,
preventive chemotherapy is the key strategy for morbidity control. However,
local knowledge, attitudes, and practices (KAP) of parasitic worms are poorly
understood, although such information is required for prevention and
sustainable control.

Methods

We carried out KAP surveys in two rural communities of Côte d&apos;Ivoire that were
subjected to school-based and community-based research and control activities.
We used qualitative and quantitative methods. The former included
observations, in-depth interviews with key informants, and focus group
discussions with school children and adults. Quantitative methods consisted of
a structured questionnaire administered to household heads.

Principal Findings

Access to clean water was lacking in both communities and only a quarter of
the households had functioning latrines. There was a better understanding of
soil-transmitted helminthiasis than intestinal schistosomiasis, but community-
based rather than school-based interventions appeared to improve knowledge of
schistosomiasis. In the villages with community-based interventions, three-
quarters of household interviewees knew about intestinal schistosomiasis
compared to 14% in the village where school-based interventions were
implemented (P&lt;0.001). Whereas two-thirds of respondents from the community-
based intervention village indicated that the research and control project was
the main source of information, only a quarter of the respondents cited the
project as the main source.

Conclusions/Significance

Preventive chemotherapy targeting school-aged children has limitations, as
older population segments are neglected, and hence lack knowledge about how to
prevent and control parasitic worm infections. Improved access to clean water
and sanitation is necessary, along with health education to make a durable
impact against helminth infections.

Author Summary

There is a need to better understand communities&apos; knowledge, attitudes, and
practices (KAP) of neglected tropical diseases to improve prevention and
control efforts. We studied the socio-cultural aspects of parasitic worm
infections in two villages (Mélapleu and Zouatta II) of western Côte d&apos;Ivoire,
where research and control activities have been implemented. Zouatta II was
exposed to a community-based approach, while school-based interventions were
implemented in Mélapleu. KAP surveys were carried out using qualitative and
quantitative methods. Although there was some knowledge of parasitic worm
infections in both villages, we found important differences between the two
villages regarding intestinal schistosomiasis: there was a better
understanding of this disease in Zouatta II. However, even the community-based
research and control efforts implemented in Zouatta II were ineffective in
transforming the information conveyed into preventive behavior related to
water contact. Our results suggest that KAP of parasitic worm infections
conveyed by research and control activities targeting only school-aged
children have shortcomings as older population groups are left out. Hence, for
effective control of parasitic </field></doc>
</add>
