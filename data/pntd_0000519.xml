<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000519</field>
<field name="doi">10.1371/journal.pntd.0000519</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Lily A. Trunck, Katie L. Propst, Vanaporn Wuthiekanun, Apichai Tuanyok, Stephen M. Beckstrom-Sternberg, James S. Beckstrom-Sternberg, Sharon J. Peacock, Paul Keim, Steven W. Dow, Herbert P. Schweizer</field>
<field name="title">Molecular Basis of Rare Aminoglycoside Susceptibility and Pathogenesis of Burkholderia pseudomallei Clinical Isolates from Thailand</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">9</field>
<field name="pages">e519</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Burkholderia pseudomallei is intrinsically resistant to aminoglycosides and
macrolides, mostly due to AmrAB-OprA efflux pump expression. We investigated
the molecular mechanisms of aminoglycoside susceptibility exhibited by Thai
strains 708a, 2188a, and 3799a.

Methodology/Principal Findings

qRT-PCR revealed absence of amrB transcripts in 708a and greatly reduced
levels in 2188a and 3799a. Serial passage on increasing gentamicin
concentrations yielded 2188a and 3799a mutants that became simultaneously
resistant to other aminoglycosides and macrolides, whereas such mutants could
not be obtained with 708a. Transcript analysis showed that the resistance of
the 2188a and 3799a mutants was due to upregulation of amrAB-oprA expression
by unknown mechanism(s). Use of a PCR walking strategy revealed that the
amrAB-oprA operon was missing in 708a and that this loss was associated with
deletion of more than 70 kb of genetic material. Rescue of the amrAB-oprB
region from a 708a fosmid library and sequencing showed the presence of a
large chromosome 1 deletion (131 kb and 141 kb compared to strains K96243 and
1710b, respectively). This deletion not only removed the amrAB-oprA operon,
but also the entire gene clusters for malleobactin and cobalamin synthesis.
Other genes deleted included the anaerobic arginine deiminase pathway,
putative type 1 fimbriae and secreted chitinase. Whole genome sequencing and
PCR analysis confirmed absence of these genes from 708a. Despite missing
several putative virulence genes, 708a was fully virulent in a murine
melioidosis model.

Conclusions/Significance

Strain 708a may be a natural candidate for genetic manipulation experiments
that use Select Agent compliant antibiotics for selection and validates the
use of laboratory-constructed Δ(amrAB-oprA) mutants in such experiments.

Author Summary

Burkholderia pseudomallei is the etiologic agent of melioidosis, an emerging
tropical disease. Because of low infectious dose, broad-host-range
infectivity, intrinsic antibiotic resistance and historic precedent as a
bioweapon, B. pseudomallei was listed in the United States as a Select Agent
and Priority Pathogen of biodefense concern by the US Centers for Disease
Control and Prevention and the National Institute of Allergy and Infectious
Diseases. The mechanisms governing antibiotic resistance and/or susceptibility
and virulence in this bacterium are not well understood. Most clinical and
environmental B. pseudomallei isolates are highly resistant to
aminoglycosides, but susceptible variants do exist. The results of our studies
with three such variants from Thailand reveal that lack of expression or
deletion of an efflux pump is responsible for this susceptibility. The large
deletion present in one strain not only removes an efflux pump but also
several putative virulence genes, including an entire siderophore gene
cluster. Despite this deletion, the strain is fully virulent in an acute mouse
melioidosis model. In summary, our findings shed light on mechanisms of
antibiotic resistance and pathogenesis. They also validate the previously
advocated use of laboratory-constructed, ami</field></doc>
</add>
