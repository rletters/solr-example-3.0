<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000167</field>
<field name="doi">10.1371/journal.pntd.0000167</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Sérgio S. Cunha, Neal Alexander, Mauricio L. Barreto, Emilia S. Pereira, Inês Dourado, Maria de Fátima Maroja, Yury Ichihara, Silvana Brito, Susan Pereira, Laura C. Rodrigues</field>
<field name="title">BCG Revaccination Does Not Protect Against Leprosy in the Brazilian Amazon: A Cluster Randomised Trial</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">2</field>
<field name="pages">e167</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Although BCG has been found to impart protection against leprosy in many
populations, the utility of repeat or booster BCG vaccinations is still
unclear. When a policy of giving a second BCG dose to school children in
Brazil was introduced, a trial was conducted to assess its impact against
tuberculosis, and a leprosy component was then undertaken in parallel.
Objective: to estimate the protection against leprosy imparted by a second
dose of BCG given to schoolchildren.

Methods and Findings

This is a cluster randomised community trial, with 6 years and 8 months of
follow-up. Study site: City of Manaus, Amazon region, a leprosy-endemic area
in Brazil. Participants: 99,770 school children with neonatal BCG (aged 7–14
years at baseline), of whom 42,662 were in the intervention arm
(revaccination). Intervention: BCG given by intradermal injection. Main
outcome: Leprosy (all clinical forms). Results: The incidence rate ratio of
leprosy in the intervention over the control arm within the follow-up, in
schoolchildren with neonatal BCG, controlled for potential confounders and
adjusted for clustering, was 0.99 (95% confidence interval: 0.68 to 1.45).

Conclusions/Significance

There was no evidence of protection conferred by the second dose of BCG
vaccination in school children against leprosy during the trial follow-up.
These results point to a need to consider the effectiveness of the current
policy of BCG vaccination of contacts of leprosy cases in Brazilian Amazon
region.

Author Summary

BCG is a vaccine developed and used to protect against tuberculosis, but it
can also protect against leprosy. In Brazil, children receive BCG at birth,
and since 1996 a trial has been conducted to find out if a second dose of BCG
administered to schoolchildren gives additional protection against
tuberculosis. We use this trial to find out if such vaccination protects
against leprosy. The trial was conducted in the Brazilian Amazon, involving
almost 100,000 children aged 7–14 years who had received neonatal BCG. Half of
them received a second dose of BCG at school, and the other half did not. We
followed the children for 6 years and observed that there were as many new
cases of leprosy in the vaccinated children as in the unvaccinated children.
Therefore, we concluded that a second dose of BCG given at school age in the
Brazilian Amazon offers no additional protection against leprosy.

Introduction

BCG vaccination is given routinely to neonates to prevent tuberculosis in
Brazil and in most of the world. BCG also protects against leprosy, with
estimates of protection ranging from 20% to 90% [1],[2]. In Brazil, in
addition to routine BCG vaccination at birth to prevent tuberculosis, BCG is
officially recommended for household contacts of leprosy cases. In 1994 the
Brazilian Ministry of Health expanded its tuberculosis control policy to
recommend the routine BCG vaccination of school age children (around 7–14
years old). Given the high coverage of neonatal vaccination, this was
effectively revaccination for most children. A large cluster randomised trial
(BCG-REVAC) was started in 1996 to assess the effectiveness against
tuberculosis of BCG vaccination of sc</field></doc>
</add>
