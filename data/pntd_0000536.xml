<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000536</field>
<field name="doi">10.1371/journal.pntd.0000536</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Elizabeth Castro Moreno, Andréa Vieira Gonçalves, Anderson Vieira Chaves, Maria Norma Melo, José Roberto Lambertucci, Antero Silva Ribeiro Andrade, Deborah Negrão-Corrêa, Carlos Mauricio de Figueiredo Antunes, Mariângela Carneiro</field>
<field name="title">Inaccuracy of Enzyme-Linked Immunosorbent Assay Using Soluble and Recombinant Antigens to Detect Asymptomatic Infection by Leishmania infantum</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">10</field>
<field name="pages">e536</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

One of the most important drawbacks in visceral leishmaniasis (VL) population
studies is the difficulty of diagnosing asymptomatic carriers. The aim of this
study, conducted in an urban area in the Southeast of Brazil, was to evaluate
the performance of serology to identify asymptomatic VL infection in
participants selected from a cohort with a two-year follow-up period.

Methodology

Blood samples were collected in 2001 from 136 cohort participants (97 positive
and 39 negatives, PCR/hybridization carried out in 1999). They were clinically
evaluated and none had progressed to disease from their asymptomatic state. As
controls, blood samples from 22 control individuals and 8 patients with kala-
azar were collected. Two molecular biology techniques (reference tests) were
performed: PCR with Leishmania-generic primer followed by hybridization using
L. infantum probe, and PCR with specific primer to L. donovani complex. Plasma
samples were tested by ELISA using three different antigens: L. infantum and
L. amazonensis crude antigens, and rK39 recombinant protein. Accuracy of the
serological tests was evaluated using sensitivity, specificity, likelihood
ratio and ROC curve.

Findings

The presence of Leishmania was confirmed, by molecular techniques, in all
kala-azar patients and in 117 (86%) of the 136 cohort participants. Kala-azar
patients showed high reactivity in ELISAs, whereas asymptomatic individuals
presented low reactivity against the antigens tested. When compared to
molecular techniques, the L. amazonensis and L. infantum antigens showed
higher sensitivity (49.6% and 41.0%, respectively) than rK39 (26.5%); however,
the specificity of rK39 was higher (73.7%) than L. amazonensis (52.6%) and L.
infantum antigens (36.8%). Moreover, there was low agreement among the
different antigens used (kappa&lt;0.10).

Conclusions

Serological tests were inaccurate for diagnosing asymptomatic infections
compared to molecular methods; this could lead to misclassification bias in
population studies. Therefore, studies which have used serological assays to
estimate prevalence, to evaluate intervention programs or to identify risk
factors for Leishmania infection, may have had their results compromised.

Author Summary

Visceral leishmaniasis or kala-azar in Brazil, caused by a parasite protozoon,
is transmitted to humans by sandflies and has wild and domestic dogs as
reservoirs. The disease can become chronic, characterized by fever, weight
loss, and enlargement of the liver and spleen. However, the infection in
humans can be asymptomatic, and the importance of this phase is unknown. The
low levels of immune response and the small number of circulating parasites in
the blood are the main problems in identifying the asymptomatic phase. In this
study we evaluated the performance of the serology tests compared with
molecular tests to identify asymptomatic infection in individuals identified
in an urban area of Brazil. Also included were non-infected individuals
(healthy controls) and patients (diseased controls). Blood was collected and
examined by different sero</field></doc>
</add>
