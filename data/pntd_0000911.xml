<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000911</field>
<field name="doi">10.1371/journal.pntd.0000911</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Richard W. Merritt, Edward D. Walker, Pamela L. C. Small, John R. Wallace, Paul D. R. Johnson, M. Eric Benbow, Daniel A. Boakye</field>
<field name="title">Ecology and Transmission of Buruli Ulcer Disease: A Systematic Review</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">12</field>
<field name="pages">e911</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Buruli ulcer is a neglected emerging disease that has recently been reported
in some countries as the second most frequent mycobacterial disease in humans
after tuberculosis. Cases have been reported from at least 32 countries in
Africa (mainly west), Australia, Southeast Asia, China, Central and South
America, and the Western Pacific. Large lesions often result in scarring,
contractual deformities, amputations, and disabilities, and in Africa, most
cases of the disease occur in children between the ages of 4–15 years. This
environmental mycobacterium, Mycobacterium ulcerans, is found in communities
associated with rivers, swamps, wetlands, and human-linked changes in the
aquatic environment, particularly those created as a result of environmental
disturbance such as deforestation, dam construction, and agriculture. Buruli
ulcer disease is often referred to as the “mysterious disease” because the
mode of transmission remains unclear, although several hypotheses have been
proposed. The above review reveals that various routes of transmission may
occur, varying amongst epidemiological setting and geographic region, and that
there may be some role for living agents as reservoirs and as vectors of M.
ulcerans, in particular aquatic insects, adult mosquitoes or other biting
arthropods. We discuss traditional and non-traditional methods for indicting
the roles of living agents as biologically significant reservoirs and/or
vectors of pathogens, and suggest an intellectual framework for establishing
criteria for transmission. The application of these criteria to the
transmission of M. ulcerans presents a significant challenge.

Author Summary

Buruli ulcer (BU) is a serious necrotizing cutaneous infection caused by
Mycobacterium ulcerans. It is a neglected emerging disease that has recently
been reported in some countries as the second most frequent mycobacterial
disease in humans after tuberculosis (TB). Cases have been reported from at
least 32 countries in Africa (mainly west), Australia, Southeast Asia, China,
Central and South America, and the Western Pacific. BU is a disease found in
rural areas located near wetlands (ponds, swamps, marshes, impoundments,
backwaters) and slow-moving rivers, especially in areas prone to human-made
disturbance and flooding. Despite considerable research on this disease in
recent years, the mode of transmission remains unclear, although several
hypotheses have been proposed. In this article we review the current state of
knowledge on the ecology and transmission of M. ulcerans in Africa and
Australia, discuss traditional and non-traditional methods for investigating
transmission, and suggest an intellectual framework for establishing criteria
for transmission.

Introduction

Buruli ulcer (BU) is a serious necrotizing cutaneous infection caused by
Mycobacterium ulcerans [1]–[7]. Before the causative agent was specifically
identified, it was clinically given geographic designations such as
Bairnsdale, Searles, and Kumasi ulcer, depending on the country [8]–[11]. BU
is a neglected emerging disease that has recently been reported in some
countries as the second most frequent mycobacterial disease in humans after
tuberculosis (TB) [12]–[14]. Large lesions often result in scarring,
contractual deformi</field></doc>
</add>
