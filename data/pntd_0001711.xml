<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001711</field>
<field name="doi">10.1371/journal.pntd.0001711</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Nádia C. Düppre, Luiz Antonio B. Camacho, Anna M. Sales, Ximena Illarramendi, José Augusto C. Nery, Elizabeth P. Sampaio, Euzenir N. Sarno, Samira Bührer-Sékula</field>
<field name="title">Impact of PGL-I Seropositivity on the Protective Effect of BCG Vaccination among Leprosy Contacts: A Cohort Study</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">6</field>
<field name="pages">e1711</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Contacts of leprosy patients are at increased risk of developing leprosy and
need to be targeted for early diagnosis. Seropositivity to the phenolic
glycolipid I (PGL-I) antigen of Mycobacterium leprae has been used to identify
contacts who have an increased risk of developing leprosy. In the present
study, we studied the effect of seropositivity in patient contacts, on the
risk of developing leprosy, stratified by Bacille Calmette Guerin (BCG)
vaccination after index case diagnosis.

Methodology/Principal Findings

Leprosy contacts were examined as part of the surveillance programme of the
Oswaldo Cruz Institute Leprosy Outpatient Clinic in Rio de Janeiro.
Demographic, social, epidemiological and clinical data were collected. The
presence of IgM antibodies to PGL-I in sera and BCG vaccination status at the
time of index case diagnosis were evaluated in 2,135 contacts. During follow-
up, 60 (2.8%; 60/2,135) leprosy cases were diagnosed: 41 among the 1,793
PGL-I-negative contacts and 19 among the 342 PGL-I-positive contacts. Among
PGL-I-positive contacts, BCG vaccination after index case diagnosis increased
the adjusted rate of developing clinical manifestations of leprosy (Adjusted
Rate Ratio (aRR) = 4.1; 95% CI: 1.8–8.2) compared with the PGL-I-positive
unvaccinated contacts (aRR = 3.2; 95% CI: 1.2–8.1). The incidence density was
highest during the first year of follow-up for the PGL-I-positive vaccinated
contacts. However, all of those contacts developed PB leprosy, whereas most MB
cases (4/6) occurred in PGL-I-positive unvaccinated contacts.

Conclusion

Contact examination combined with PGL-I testing and BCG vaccination remain
important strategies for leprosy control. The finding that rates of leprosy
cases were highest among seropositive contacts justifies targeting this
specific group for close monitoring. Furthermore, it is recommended that
PGL-I-positive contacts and contacts with a high familial bacteriological
index, regardless of serological response, should be monitored. This group
could be considered as a target for chemoprophylaxis.

Author Summary

Although leprosy has become a neglected disease, it is an important cause of
disability, and 250,000 new cases are still diagnosed worldwide every year.
The current study was carried out in Brazil, where almost 40,000 new cases of
leprosy are diagnosed every year. The study targeted contacts of leprosy
patients, who are at the highest risk of contracting the disease. We studied
2,135 contacts who were diagnosed at the Leprosy Outpatient Clinic at the
Oswaldo Cruz Foundation in Rio de Janeiro, RJ, Brazil, between 1987 and 2007.
The presence of antibodies against a specific Mycobacterium leprae antigen
(PGL-I) at the first examination and BCG vaccination status were evaluated.
PGL-I-positive contacts had a higher risk of developing leprosy than
PGL-I-negative contacts. Among the former, vaccinated contacts were at higher
risk than unvaccinated contacts. Our results indicate that contact examination
combined with PGL-I testing and BCG vaccination appears to justify the
targeting of PGL-I-positive individuals for enhanced surveillance.
Furthermore, it is highly recommended tha</field></doc>
</add>
