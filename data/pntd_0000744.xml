<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000744</field>
<field name="doi">10.1371/journal.pntd.0000744</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Bouke C. de Jong, Martin Antonio, Sebastien Gagneux</field>
<field name="title">Mycobacterium africanum—Review of an Important Cause of Human Tuberculosis in West Africa</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">9</field>
<field name="pages">e744</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Mycobacterium africanum consists of two phylogenetically distinct lineages
within the Mycobacterium tuberculosis complex, known as M. africanum West
African 1 and M. africanum West African 2. These lineages are restricted to
West Africa, where they cause up to half of human pulmonary tuberculosis. In
this review we discuss the definition of M. africanum, describe the prevalence
and restricted geographical distribution of M. africanum West African 1 and 2,
review the occurrence of M. africanum in animals, and summarize the phenotypic
differences described thus far between M. africanum and M. tuberculosis sensu
stricto.

Introduction

Mycobacterium africanum causes up to half of human tuberculosis (TB) in West
Africa [1],[2]. It was first described as a distinct sub-species within the
Mycobacterium tuberculosis complex (MTBC) by Castets and colleagues in 1968
[3]. M. africanum yields variable results on classical biochemical
characterization, which has complicated the proper classification of the sub-
species. This classification has been revised since the advent of molecular
genotyping techniques (Figure 1) [4]. Thus, M. africanum type I (West African
clade) has recently been sub-divided into M. africanum type I, West African 1
(MAF1), prevalent around the Gulf of Guinea, and M. africanum type I, West
African 2 (MAF2), prevalent in western West Africa [5] (Figure 1). The M.
africanum type II (East African clade) has been reclassified into M.
tuberculosis sensu stricto (Figure 1) [4], and is indicated as “Uganda”
genotype in Figure 2. Castets wrote a French review on M. africanum in 1979
[6]. While both M. africanum type I and type II were previously reviewed by
Onipede and colleagues [7], this review focuses on the two West African
lineages within M. africanum type I, MAF1 and MAF2. We use the term “M.
africanum” when referencing studies in which no molecular distinction between
MAF1 and MAF2 was included or when MAF1 and MAF2 were described together.

Figure 1

Nomenclature of M. africanum as related to its biochemical and molecular
classification.

Biochemical classification is reviewed in Table 1. LSPs, large sequence
polymorphisms [5]. Spoligotype signatures are reviewed in [16].

Figure 2

The position of M. africanum in the global phylogeny of the M. tuberculosis
complex (MTBC) as originally published by Hershberg et al.[23].

This phylogeny is based on over 65 kb of DNA sequence data (89 concatenated
gene sequences) in each of 108 strains of the MTBC and was inferred using
maximum parsimony, which resulted in a single tree with negligable homoplasy
[23]. Analysis by the neighbor-joining method resulted in an identical tree
topology with high statistical support for all main branches [23]. This
phylogeny has been referred to as the most robust and most detailed phylogeny
of the MTBC to date, and thus should be considered as the new gold standard
for classification of the MTBC [87]. The six main MTBC lineages adapted to
humans and the animal strains are indicated in different colors. The human
MTBC lineages include four M. tuberculosis lineages and the two M. africanum
type I lineages. The “Uganda” genotype (formally referred to as M. africanum
type II), which is a sub-lineage within M. tuberculosis lineage 4 (also known
as the Euro-American lineage), is also shown. These</field></doc>
</add>
