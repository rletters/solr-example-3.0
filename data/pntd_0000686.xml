<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000686</field>
<field name="doi">10.1371/journal.pntd.0000686</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Melissa L. Burke, Donald P. McManus, Grant A. Ramm, Mary Duke, Yuesheng Li, Malcolm K. Jones, Geoffrey N. Gobert</field>
<field name="title">Co-ordinated Gene Expression in the Liver and Spleen during Schistosoma japonicum Infection Regulates Cell Migration</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">5</field>
<field name="pages">e686</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Determining the molecular events induced in the spleen during schistosome
infection is an essential step in better understanding the immunopathogenesis
of schistosomiasis and the mechanisms by which schistosomes modulate the host
immune response. The present study defines the transcriptional and cellular
events occurring in the murine spleen during the progression of Schistosoma
japonicum infection. Additionally, we compared and contrasted these results
with those we have previously reported for the liver. Microarray analysis
combined with flow cytometry and histochemistry demonstrated that
transcriptional changes occurring in the spleen were closely related to
changes in cellular composition. Additionally, the presence of alternatively
activated macrophages, as indicated by up-regulation of Chi3l3 and Chi3l4 and
expansion of F4/80+ macrophages, together with enhanced expression of the
immunoregulatory genes ANXA1 and CAMP suggests the spleen may be an important
site for the control of S. japonicum-induced immune responses. The most
striking difference between the transcriptional profiles of the infected liver
and spleen was the contrasting expression of chemokines and cell adhesion
molecules. Lymphocyte chemokines, including the homeostatic chemokines CXCL13,
CCL19 and CCL21, were significantly down-regulated in the spleen but up-
regulated in the liver. Eosinophil (CCL11, CCL24), neutrophil (CXCL1) and
monocyte (CXCL14, CCL12) chemokines and the cell adhesion molecules VCAM1,
NCAM1, PECAM1 were up-regulated in the liver but unchanged in the spleen.
Chemokines up-regulated in both organs were expressed at significantly higher
levels in the liver. Co-ordinated expression of these genes probably
contributes to the development of a chemotactic signalling gradient that
promotes recruitment of effector cells to the liver, thereby facilitating the
development of hepatic granulomas and fibrosis. Together these data provide,
for the first time, a comprehensive overview of the molecular events occurring
in the spleen during schistosomiasis and will substantially further our
understanding of the local and systemic mechanisms driving the
immunopathogenesis of this disease.

Author Summary

Schistosomiasis is a significant cause of illness and death in the developing
world. Inflammation and scarring in the liver and enlargement of the spleen
(splenomegaly) are common features of the disease. Changes occurring in the
spleen have the potential to influence the way in which the body deals with
infection but the mechanisms driving these changes are not well characterised.
In the present study we determined, for the first time, the gene expression
profile of the mouse spleen during infection with Schistosoma japonicum and
compared these results to those previously reported for the liver to determine
if processes occurring in these organs co-operate to promote hepatic
inflammation and granuloma formation. Our data indicated that gene expression
in the spleen is related to the types of cells present and suggest that the
spleen might be important in controlling schistosome-induced inflammation.
Comparison of the liver and spleen showed that expression of cell signalling
molecules (chemokines) was much higher in the liver, p</field></doc>
</add>
