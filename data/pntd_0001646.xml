<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001646</field>
<field name="doi">10.1371/journal.pntd.0001646</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Jürg Utzinger</field>
<field name="title">A Research and Development Agenda for the Control and Elimination of Human Helminthiases</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">4</field>
<field name="pages">e1646</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
In this issue of PLoS Neglected Tropical Diseases, the Disease Reference Group
on Helminth Infections (DRG4) has put forward a collection of eight reviews
that, taken together, outline a compelling research and development agenda for
the control and elimination of helminth diseases of humans
(http://www.ploscollections.org/helminths [1]–[8]). Emphasis is placed on six
major helminth infections: (i) soil-transmitted helminthiasis; (ii)
schistosomiasis; (iii) lymphatic filariasis; (iv) onchocerciasis; (v) food-
borne trematodiasis; and (vi) cysticercosis/taeniasis. Selection of these
helminthiases is justified on multiple grounds. Firstly, as shown in Table 1,
more than half of the world&apos;s population is at risk of one or several of these
helminthiases, and hundreds of millions of people are currently infected.
Secondly, consequences of the mainly long-term chronic infection include
suffering, stigmatisation, subtle and gross morbidity (e.g., anaemia, limb
deformations and blindness), and premature death, hence causing an intolerable
global burden [9]–[17]. These features, in turn, exacerbate poverty [18]–[20].
Thirdly, there is growing commitment at all levels—from local communities to
politicians, philanthropic organisations, and civil society—to control and
eventually eliminate/eradicate the major human helminthiases.

Table 1

Helminth Infections Emphasised by DRG4 for Development of a Research Agenda
for Control and Elimination.

Helminth Infection

Causative Agent(s)

At-Risk Population (Millions)

No. of People Infected (Millions)

No. of People with Morbidity (Millions)

No. of Deaths per Year (Thousands)

Global Burden (Thousand DALYs)

Reference(s)

Soil-transmitted helminthiasis

Ascariasis

Ascaris lumbricoides

5,416

807–1,221

350

3–60

1,817–10,500

[10], [11], [17]

Trichuriasis

Trichuris trichiura

5,307

604–795

220

3–10

1,006–6,400

[10], [11], [17]

Hookworm infection

Ancylostoma duodenale and Necator americanus

5,346

576–740

150

3–65

59–22,100

[10], [11], [17]

Strongyloidiasis

Strongyloides stercoralis

n.d.

30–100

n.d.

n.d.

n.d.

[10]

Lymphatic filariasis

Wuchereria bancrofti, Brugia malayi, and B. timori

&gt;1,000

120

43

0

5,777

[11], [14]

Schistosomiasisa

Schistosoma haematobium, S. japonicum, and S. mansoni

779

207

120

15–280

1,702–4,500

[11], [12]

Food-borne trematodiasis

Clonorchiasis

Clonorchis sinensis

601

15.3

1.1

5.6

275

[13], [16]

Paragonimiasis

Paragonimus spp.

292

23.2

5.3

0.2

197

[13], [16]

Fascioliasis

Fasciola gigantica and F. hepatica

91

2.6

0.3

0

35

[13], [16]

Opisthorchiasis

Opisthorchis felineus and O. viverrini

80

8.4

0.3

1.3

74

[13], [16]

Intestinal fluke infections

Echinostoma spp., Fasciolopsis buski, Metagonimus spp., and Heterophyidae

n.d.

6.7

0.9

0

84

[13], [16]

Onchocerciasis

Onchocerca volvulus

120

37

n.d.

0.05 (in the OCP area)

484

[9], [14]

Cysticercosis/taeniasis

Taenia solium and T. saginata

n.d.

n.d.

n.d.

n.d.

&gt;2,000

[15]

a Listed are the three main schistosome species parasitising humans; of lesser
importance are S. guineenisis and S. intercalatum (both restricted to West and
Central Africa) and S. mekongi (restricted to Cambodia and Lao PDR).

DALY, disability-adjusted life year; n.d., not determined; OCP, Onchocerciasis
Control Programme.

Figure 1 s</field></doc>
</add>
