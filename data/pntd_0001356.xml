<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001356</field>
<field name="doi">10.1371/journal.pntd.0001356</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Amira A. Roess, Benjamin P. Monroe, Eric A. Kinzoni, Seamus Gallagher, Saturnin R. Ibata, Nkenda Badinga, Trolienne M. Molouania, Fredy S. Mabola, Jean V. Mombouli, Darin S. Carroll, Adam MacNeil, Noelle A. Benzekri, Cynthia Moses, Inger K. Damon, Mary G. Reynolds</field>
<field name="title">Assessing the Effectiveness of a Community Intervention for Monkeypox Prevention in the Congo Basin</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">10</field>
<field name="pages">e1356</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

In areas where health resources are limited, community participation in the
recognition and reporting of disease hazards is critical for the
identification of outbreaks. This is particularly true for zoonotic diseases
such as monkeypox that principally affect people living in remote areas with
few health services. Here we report the findings of an evaluation measuring
the effectiveness of a film-based community outreach program designed to
improve the understanding of monkeypox symptoms, transmission and prevention,
by residents of the Republic of the Congo (ROC) who are at risk for disease
acquisition.

Methodology/Principal Findings

During 90 days, monkeypox outreach was conducted for ∼23,860 people in
northern ROC. Two hundred seventy-one attendees (selected via a structured
sample) were interviewed before and after participating in a small-group
outreach session. The proportion of interviewees demonstrating monkeypox-
specific knowledge before and after was compared. Significant gains were
measured in areas of disease recognition, transmission, and mitigation of
risk. The ability to recognize at least one disease symptom and a willingness
to take a family member with monkeypox to the hospital increased from 49 and
45% to 95 and 87%, respectively (p&lt;0.001, both). Willingness to deter
behaviors associated with zoonotic risk, such as eating the carcass of a
primate found dead in the forest, remained fundamentally unchanged however,
suggesting additional messaging may be needed.

Conclusions/Significance

These results suggest that our current program of film-based educational
activities is effective in improving disease-specific knowledge and may
encourage individuals to seek out the advice of health workers when monkeypox
is suspected.

Author Summary

Human monkeypox is a potentially severe illness that begins with a high fever
soon followed by the development of a smallpox-like rash. Both monkeypox and
smallpox are caused by infection with viruses in the genus Orthopoxvirus. But
smallpox, which only affected humans, has been eradicated, whereas monkeypox
continues to occur when humans come into contact with infected animals. There
are currently no drugs specifically available for the treatment of monkeypox,
and the use of vaccines for prevention is limited due to safety concerns.
Therefore, monkeypox prevention depends on diminishing human contact with
infected animals and preventing person-to-person spread of the virus. The
authors describe a film-based method for community outreach intended to
increase monkeypox knowledge among residents of communities in the Republic of
the Congo. Outreach was performed to ∼23,600 rural Congolese. The
effectiveness of the outreach was evaluated using a sample of individuals who
attended small-group sessions. The authors found that among the participants,
the ability to recognize monkeypox symptoms and the willingness to take ill
family members to the hospital was significantly increased after seeing the
films. In contrast, the willingness to deter some high-risk behaviors, such as
eating animal carcasses found </field></doc>
</add>
