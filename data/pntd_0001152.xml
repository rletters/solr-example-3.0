<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001152</field>
<field name="doi">10.1371/journal.pntd.0001152</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Hélène Carabin, Patrick Cyaga Ndimubanzi, Christine M. Budke, Hai Nguyen, Yingjun Qian, Linda Demetry Cowan, Julie Ann Stoner, Elizabeth Rainwater, Mary Dickey</field>
<field name="title">Clinical Manifestations Associated with Neurocysticercosis: A Systematic Review</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">5</field>
<field name="pages">e1152</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The clinical manifestations of neurocysticercosis (NCC) are poorly understood.
This systematic review aims to estimate the frequencies of different
manifestations, complications and disabilities associated with NCC.

Methods

A systematic search of the literature published from January 1, 1990, to June
1, 2008, in 24 different electronic databases and 8 languages was conducted.
Meta-analyses were conducted when appropriate.

Results

A total of 1569 documents were identified, and 21 included in the analysis.
Among patients seen in neurology clinics, seizures/epilepsy were the most
common manifestations (78.8%, 95%CI: 65.1%–89.7%) followed by headaches
(37.9%, 95%CI: 23.3%–53.7%), focal deficits (16.0%, 95%CI: 9.7%–23.6%) and
signs of increased intracranial pressure (11.7%, 95%CI: 6.0%–18.9%). All other
manifestations occurred in less than 10% of symptomatic NCC patients. Only
four studies reported on the mortality rate of NCC.

Conclusions

NCC is a pleomorphic disease linked to a range of manifestations. Although
definitions of manifestations were very rarely provided, and varied from study
to study, the proportion of NCC cases with seizures/epilepsy and the
proportion of headaches were consistent across studies. These estimates are
only applicable to patients who are ill enough to seek care in neurology
clinics and likely over estimate the frequency of manifestations among all NCC
cases.

Author Summary

Neurocysticercosis is an infection of the brain with the flatworm Taenia
solium which is normally transmitted between humans and pigs. Sometimes,
humans can infect other humans and the larva of the parasite can go the brain,
causing the disease neurocysticercosis. There has never been a systematic
review of what clinical signs are found among people with neurocysticercosis.
We conducted a thorough review of the literature to answer this question. We
reviewed 1569 and 21 were of a sufficient quality to be included in the final
analysis. Among neurocysticercosis patients who are seeking care in neurology
clinics, about 79% have seizures/epilepsy, 38% severe headaches, 16% focal
deficits and 12% signs of increased intracranial pressure. Several other
symptoms were also reported in less than 10% of patients. People with
neurocysticercosis who seek care in neurology clinics show a whole range of
manifestations. Clinicians should be encouraged to consider neurocysticercosis
in their differential diagnosis when a patient presented with one of the
symptoms described in this review. This would ultimately improve the estimates
of the frequency of symptoms associated with neurocysticercosis.

Introduction

Neurocysticercosis (NCC) is primarily found in countries with poor sanitation
and hygiene and improper slaughterhouse services. However, due to
globalization and immigration, NCC is increasingly being reported in developed
countries [1]. Humans become infected by ingesting Taenia solium eggs that
later develop into oncospheres. These larvae can migrate to any organ in the
body, but most reports have focused on cysts located in the Central Nervous
System (CNS), eyes, muscles or subcutaneous tissues. The larvae have been
found in several locations in the CNS. This diversity of loca</field></doc>
</add>
