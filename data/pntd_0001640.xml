<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001640</field>
<field name="doi">10.1371/journal.pntd.0001640</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Jinning Yu, Hao Li, Qing Tang, Simon Rayner, Na Han, Zhenyang Guo, Haizhou Liu, James Adams, Wei Fang, Xiaoyan Tao, Shumei Wang, Guodong Liang</field>
<field name="title">The Spatial and Temporal Dynamics of Rabies in China</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">5</field>
<field name="pages">e1640</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background and Objectives

Recent years have seen a rapid increase in the number of rabies cases in China
and an expansion in the geographic distribution of the virus. In spite of the
seriousness of the outbreak and increasing number of fatalities, little is
known about the phylogeography of the disease in China. In this study, we
report an analysis of a set of Nucleocapsid sequences consisting of samples
collected through the trial Chinese National Surveillance System as well as
publicly available sequences. This sequence set represents the most
comprehensive dataset from China to date, comprising 210 sequences (including
57 new samples) from 15 provinces and covering all epidemic regions. Using
this dataset we investigated genetic diversity, patterns of distribution, and
evolutionary history.

Results

Our analysis indicates that the rabies virus in China is primarily defined by
two clades that exhibit distinct population subdivision and translocation
patterns and that contributed to the epidemic in different ways. The younger
clade originated around 1992 and has properties that closely match the
observed spread of the recent epidemic. The older clade originated around 1960
and has a dispersion pattern that suggests it represents a strain associated
with a previous outbreak that remained at low levels throughout the country
and reemerged in the current epidemic.

Conclusions

Our findings provide new insight into factors associated with the recent
epidemic and are relevant to determining an effective policy for controlling
the virus.

Author Summary

Rabies is a major problem in developing countries and responsible for more
than 55,000 deaths annually. More than half of the cases occur in Asia and
China has the second highest incidence of rabies after India. Human rabies
cases in China decreased during the early 1990s but the virus began to re-
emerge in the latter half of the decade and spread rapidly across the country
with a corresponding increase in cases. To try and learn more about the
epidemic, in 2006 the government implemented a trial surveillance program to
sample and screen canine populations in locations where human cases were
reported. In this work we selected a subset of samples (representative of the
entire epidemic region) for sequencing and investigated the history and origin
of the virus in China and examined the variation from a geographical
perspective. Our results indicate that the epidemic is primarily composed of a
younger strain with a geographical dispersion that was consistent with the
recorded spread of the virus and a second older strain that corresponds to a
previous epidemic. This second group exhibits a different geographical
pattern, and it appears that this strain remained at low levels throughout the
country and was able to re-emerge as the epidemic took hold.

Introduction

Rabies is an enzootic disease that causes severe dysfunction to the central
nervous system [1]. While cases are relatively rare in developed countries,
the virus has significant impact on a global scale, with more than 55,000
deaths reported annually [2], and represents a major public health issue in
many countries. More than half of these cases occur in Asia and China has the
second highest incidence of rabies after India [3], [4]</field></doc>
</add>
