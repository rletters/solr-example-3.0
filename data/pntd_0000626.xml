<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000626</field>
<field name="doi">10.1371/journal.pntd.0000626</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Tiziana Lembo, Katie Hampson, Magai T. Kaare, Eblate Ernest, Darryn Knobel, Rudovick R. Kazwala, Daniel T. Haydon, Sarah Cleaveland</field>
<field name="title">The Feasibility of Canine Rabies Elimination in Africa: Dispelling Doubts with Data</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">2</field>
<field name="pages">e626</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Canine rabies causes many thousands of human deaths every year in Africa, and
continues to increase throughout much of the continent.

Methodology/Principal Findings

This paper identifies four common reasons given for the lack of effective
canine rabies control in Africa: (a) a low priority given for disease control
as a result of lack of awareness of the rabies burden; (b) epidemiological
constraints such as uncertainties about the required levels of vaccination
coverage and the possibility of sustained cycles of infection in wildlife; (c)
operational constraints including accessibility of dogs for vaccination and
insufficient knowledge of dog population sizes for planning of vaccination
campaigns; and (d) limited resources for implementation of rabies surveillance
and control. We address each of these issues in turn, presenting data from
field studies and modelling approaches used in Tanzania, including burden of
disease evaluations, detailed epidemiological studies, operational data from
vaccination campaigns in different demographic and ecological settings, and
economic analyses of the cost-effectiveness of dog vaccination for human
rabies prevention.

Conclusions/Significance

We conclude that there are no insurmountable problems to canine rabies control
in most of Africa; that elimination of canine rabies is epidemiologically and
practically feasible through mass vaccination of domestic dogs; and that
domestic dog vaccination provides a cost-effective approach to the prevention
and elimination of human rabies deaths.

Author Summary

Elimination of canine rabies has been achieved in some parts of the world, but
the disease still kills many thousands of people each year in Africa. Here we
counter common arguments given for the lack of effective canine rabies control
in Africa presenting detailed data from a range of settings. We conclude that
(1) rabies substantially affects public and animal health sectors, hence
regional and national priorities for control ought to be higher, (2) for
practical purposes domestic dogs are the sole maintenance hosts and main
source of infection for humans throughout most of Africa and Asia and
sufficient levels of vaccination coverage in domestic dog populations should
lead to elimination of canine rabies in most areas, (3) the vast majority of
domestic dog populations across sub-Saharan Africa are accessible for
vaccination with community sensitization being of paramount importance for the
success of these programs, (4) improved local capacity in rabies surveillance
and diagnostics will help evaluate the impact of control and elimination
efforts, and (5) sustainable resources for effective dog vaccination campaigns
are likely to be available through the development of intersectoral financing
schemes involving both medical and veterinary sectors.

Introduction

Rabies is a viral zoonosis caused by negative-stranded RNA viruses from the
Lyssavirus genus. Genetic variants of the genotype 1 Lyssavirus (the cause of
classical rabies) are maintained in different parts of the world by different
reservoir hosts within ‘host-adaptive landscapes’ [1]. Although rabies can
infect and be transmitted by a wide range of mammals, reservoirs comprise only
mammalian species within </field></doc>
</add>
