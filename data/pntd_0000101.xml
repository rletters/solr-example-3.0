<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000101</field>
<field name="doi">10.1371/journal.pntd.0000101</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Régis Pouillot, Gonçalo Matias, Christelle Mbondji Wondje, Françoise Portaels, Nadia Valin, François Ngos, Adelaïde Njikap, Laurent Marsollier, Arnaud Fontanet, Sara Eyangoh</field>
<field name="title">Risk Factors for Buruli Ulcer: A Case Control Study in Cameroon</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2007</field>
<field name="volume">1</field>
<field name="number">3</field>
<field name="pages">e101</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Buruli ulcer is an infectious disease involving the skin, caused by
Mycobacterium ulcerans. This disease is associated with areas where the water
is slow-flowing or stagnant. However, the exact mechanism of transmission of
the bacillus and the development of the disease through human activities is
unknown.

Methodology/Principal Findings

A case-control study to identify Buruli ulcer risk factors in Cameroon
compared case-patients with community-matched controls on one hand and family-
matched controls on the other hand. Risk factors identified by the community-
matched study (including 163 pairs) were: having a low level of education,
swamp wading, wearing short, lower-body clothing while farming, living near a
cocoa plantation or woods, using adhesive bandages when hurt, and using
mosquito coils. Protective factors were: using bed nets, washing clothes, and
using leaves as traditional treatment or rubbing alcohol when hurt. The
family-matched study (including 118 pairs) corroborated the significance of
education level, use of bed nets, and treatment with leaves.

Conclusions/Significance

Covering limbs during farming activities is confirmed as a protective factor
guarding against Buruli ulcer disease, but newly identified factors including
wound treatment and use of bed nets may provide new insight into the unknown
mode of transmission of M. ulcerans or the development of the disease.

Author Summary

Buruli ulcer (BU) is a neglected tropical infectious disease caused by
Mycobacterium ulcerans. While BU is associated with areas where the water is
slow-flowing or stagnant, the exact mechanism of transmission of the bacillus
is unknown, impairing efficient control programs. Two hypotheses are proposed
in the literature: previous trauma at the lesion site, and transmission
through aquatic insect bites. Using results from a face-to-face questionnaire,
our study compared characteristics from Cameroonian patients with Buruli ulcer
to people without Buruli ulcer. This latter group of people was chosen within
the community or within the family of case patients. The statistical analysis
confirmed some well-known factors associated with the presence of BU, such as
wearing short lower-body clothing while farming, but it showed that the use of
bed nets and the treatment of wounds with leaves is less frequent in case
patients. These newly identified factors may provide new insight into the mode
of transmission of M. ulcerans. The implication of domestic or peridomestic
insects, suggested by the influence of the use of bed nets, should be
confirmed in specific studies.

Introduction

Buruli ulcer (BU) is an infectious disease involving the skin, caused by
Mycobacterium ulcerans, characterized by a painless nodule, papule, plaque or
edema, evolving into a painless ulcer with undermined edges, often leading to
disabling sequelae [1]. BU has been reported from 30 countries in Africa, the
Americas, Asia and the Western Pacific, mainly in tropical and subtropical
regions [2],[3]. The epidemiologic pattern is defined by the presence of
confined foci where BU is endemic [1],[3], with prevalence ranging from a few
cases to up to 22% in given communities [4]. The preventive and therapeutic
tools for </field></doc>
</add>
