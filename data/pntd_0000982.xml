<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000982</field>
<field name="doi">10.1371/journal.pntd.0000982</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Katie Hampson, Sarah Cleaveland, Deborah Briggs</field>
<field name="title">Evaluation of Cost-Effective Strategies for Rabies Post-Exposure Vaccination in Low-Income Countries</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">3</field>
<field name="pages">e982</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Prompt post-exposure prophylaxis (PEP) is essential in preventing the fatal
onset of disease in persons exposed to rabies. Unfortunately, life-saving
rabies vaccines and biologicals are often neither accessible nor affordable,
particularly to the poorest sectors of society who are most at risk and upon
whom the largest burden of rabies falls. Increasing accessibility, reducing
costs and preventing delays in delivery of PEP should therefore be
prioritized.

Methodology/Principal Findings

We analyzed different PEP vaccination regimens and evaluated their relative
costs and benefits to bite victims and healthcare providers. We found PEP
vaccination to be an extremely cost-effective intervention (from $200 to less
than $60/death averted). Switching from intramuscular (IM) administration of
PEP to equally efficacious intradermal (ID) regimens was shown to result in
significant savings in the volume of vaccine required to treat the same number
of patients, which could mitigate vaccine shortages, and would dramatically
reduce the costs of implementing PEP. We present financing mechanisms that
would make PEP more affordable and accessible, could help subsidize the cost
for those most in need, and could even support new and existing rabies control
and prevention programs.

Conclusions/Significance

We conclude that a universal switch to ID delivery would improve the
affordability and accessibility of PEP for bite victims, leading to a likely
reduction in human rabies deaths, as well as being economical for healthcare
providers.

Author Summary

Rapid delivery of post-exposure vaccination is essential for preventing the
fatal onset of rabies in persons bitten by rabid animals. But for communities
most at risk of exposure to rabies (in resource poor countries where domestic
dog rabies is still common), post-exposure vaccines are often not affordable
and are only available in limited quantities. Several safe and effective
regimens for delivery of these vaccines are recommended by WHO, but these are
inconsistently implemented and there are no clear recommendations as to which
is the best regimen for specific settings. We developed a framework for
comparing the cost-effectiveness of different vaccination regimens, including
existing approved regimens and new candidates subject to approval, in terms of
costs per death averted. We demonstrate that post-exposure vaccination is an
extremely cost-effective public health intervention and that changing delivery
from intramuscular to intradermal vaccination has considerable benefits. Large
savings in the volume of vaccine required to treat the same number of patients
could potentially both mitigate vaccine shortages and reduce delays in
delivery, enabling wider vaccine distribution, and thus improving the
accessibility and affordability of these life-saving vaccines. We also present
financing mechanisms that could help subsidize the cost for those most in
need, and even support new and existing rabies control and prevention
programs, without compromising existing healthcare budgets.

Introduction

Rabies is invariably fatal once clinical signs appear but can be readily
prevented after exposure with prompt and appropriate post-exposure prophylaxis
(PEP) [1]. PEP is therefore the most critical life-saving intervention
essential for the preventio</field></doc>
</add>
