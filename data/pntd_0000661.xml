<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000661</field>
<field name="doi">10.1371/journal.pntd.0000661</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Ana Olga Mocumbi, Najma Latif, Magdi H. Yacoub</field>
<field name="title">Presence of Circulating Anti-Myosin Antibodies in Endomyocardial Fibrosis</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">4</field>
<field name="pages">e661</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Endomyocardial Fibrosis (EMF) is a tropical restrictive cardiomyopathy of
unknown etiology with high prevalence in Sub-Saharan Africa, for which it is
unclear whether the primary target of injury is the endocardial endothelium,
the subendocardial fibroblast, the coronary microcirculation or the myocyte.
In an attempt to explore the possibility of endocardial lesions being a result
of an immune response against the myocyte we assessed the presence and
frequency of circulating anti-myocardial antibodies in EMF patients.

Methodology/Principal Findings

EMF classification, assessment of severity and staging was based on
echocardiography. We used sodium dodecylsulfate polyacrylamide gel
electrophoresis (SDS-PAGE) of myocardial proteins followed by western blotting
to screen serum samples for antiheart antibodies G and M classes. The degree
of serum reactivity was correlated with the severity and activity of EMF. We
studied 56 EMF patients and 10 healthy controls. IgG reactivity against
myocardial proteins was stronger and more frequent in patients with EMF when
compared to controls (30/56; 53.6% vs. 1/10; 10%, respectively). IgM
reactivity was weak in both groups, although higher in EMF patients (11/56;
19.6%) when compared to controls (n = 0). EMF patients showed greater
frequency and reactivity of IgG antibodies against myocardial proteins of
molecular weights 35 kD, 42 kD and 70 kD (p values &lt;0.01, &lt;0.01 and &lt;0.05
respectively).

Conclusions

The presence of antibodies against myocardial proteins was demonstrated in a
subset of EMF patients. These immune markers seem to be related with activity
and might provide an adjunct tool for diagnosis and classification of EMF,
therefore improving its management by identifying patients who may benefit
from immunosuppressive therapy. Further research is needed to clarify the role
of autoimmunity in the pathogenesis of EMF.

Author Summary

Endomyocardial Fibrosis is a tropical disease in which the heart cannot open
properly to receive blood due to a scar that covers its inner layer. It
affects mainly children and adolescents, and has a poor prognosis because the
cause and mechanisms of scarring are unknown. The conventional treatment is
frustrating and does not alter the natural history of the disease. Despite
affecting several million people worldwide there has been little investigation
on the mechanisms of the disease or drug development to improve its prognosis.
In this study we investigate the presence of antibodies against the myocardial
cells of African patients with severe and advanced EMF aiming at uncovering
new pathways for the disease. Our results reveal that EMF patients have anti-
myocardial antibodies in their blood. The reaction of these antibodies with
the heart may be one of the mechanisms involved in the genesis of the fibrotic
lesions. This knowledge may help in diagnosing the condition and provide
alternatives for its management, using drugs that reduce the impact of the
circulating antibodies in the cardiac tissue. The significance of these
results needs confirmation on studies involving larger number of subjects due
to frequent finding of antiheart antibodies in African populations with heart
failure of any cause.

Introduction

Endomyocardial Fibrosis (EMF) is a tropical cardiomyopathy of unclear
etiopathogenesis and poor prog</field></doc>
</add>
