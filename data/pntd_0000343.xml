<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000343</field>
<field name="doi">10.1371/journal.pntd.0000343</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Barry J. Beaty, Denis J. Prager, Anthony A. James, Marcelo Jacobs-Lorena, Louis H. Miller, John H. Law, Frank H. Collins, Fotis C. Kafatos</field>
<field name="title">From Tucson to Genomics and Transgenics: The Vector Biology Network and the Emergence of Modern Vector Biology</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">3</field>
<field name="pages">e343</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Introduction

The research enterprise is often charged with responding to emerging
scientific needs and opportunities involving important public health issues.
The establishment of the Vector Biology Network (VBN) by The MacArthur
Foundation to address critical scientific, research, and human resource needs
in vector biology is a model of how to respond to such a charge efficiently
and effectively. When the Network was formed in 1989, the resurgence of
vector-borne diseases, including malaria and dengue, had revealed critical
research and training needs in vector biology. Major knowledge gaps in vector
molecular biology and pathogen transmission limited development of new
approaches for vector and disease control. The loss of vector
biologists/medical entomologists complicated control of these diseases.
Members of the VBN, an international consortium of research laboratories,
collaborated in the development and application of modern molecular and
genetic approaches in vector biology and in the training of a new cadre of
scientists capable of developing and applying modern tools to combat these
diseases. The VBN exploited the expertise and resources of the consortium
institutions, and exceeded all of its milestones, including: development of
molecular and genomic approaches in vector biology; genetic transformation of
insect vectors; characterization of vector immune systems; identification of
molecular and biological bases of vector competence; and exploitation of
identified vulnerabilities to interfere with pathogen transmission. The VBN
recruited many established scientists from other fields and trained a new
generation of leaders in vector biology. Most importantly, the VBN
participated in and helped catalyze a remarkable renaissance in vector
biology/medical entomology.

A Case Study of Collaborative Development of a Research Field

Research on mosquitoes and other arthropod vectors of infectious diseases is
of great importance for tackling neglected tropical diseases in the developing
world. Vector biology is now a well-established and rapidly advancing field
that is providing new understanding of vectors and vector pathogen
interactions and creating new opportunities for vector-borne disease (VBD)
control. Many key milestones and recruitment and training of new researchers
in this field were made possible by the coalescence of several laboratories in
the US and Europe into a collaborative research network focused on the biology
of disease vectors. The Vector Biology Network (VBN) was established and
funded for ten years (1990–2000) by The John D. and Catherine T. MacArthur
Foundation. The VBN was reinforced through endorsement and, in some cases,
coordinated funding by the World Health Organization (WHO) and national
research agencies and foundations, which collectively contributed to a
remarkable renaissance of vector biology. The history of the VBN provides a
valuable case study of how effective strategies can be developed and deployed
to address cost-effectively newly recognized scientific challenges related to
important public health and social goals.

The Challenge: Resurgence of Vector-Borne Diseases

The stimulus for developing the VBN was the worldwide resurgence of VBDs.
While e</field></doc>
</add>
