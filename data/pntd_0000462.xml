<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000462</field>
<field name="doi">10.1371/journal.pntd.0000462</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Manoj Gambhir, Maria-Gloria Basáñez, Matthew J. Burton, Anthony W. Solomon, Robin L. Bailey, Martin J. Holland, Isobel M. Blake, Christl A. Donnelly, Ibrahim Jabr, David C. Mabey, Nicholas C. Grassly</field>
<field name="title">The Development of an Age-Structured Model for Trachoma Transmission Dynamics, Pathogenesis and Control</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">6</field>
<field name="pages">e462</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Trachoma, the worldwide leading infectious cause of blindness, is due to
repeated conjunctival infection with Chlamydia trachomatis. The effects of
control interventions on population levels of infection and active disease can
be promptly measured, but the effects on severe ocular sequelae require long-
term monitoring. We present an age-structured mathematical model of trachoma
transmission and disease to predict the impact of interventions on the
prevalence of blinding trachoma.

Methodology/Principal Findings

The model is based on the concept of multiple reinfections leading to
progressive conjunctival scarring, trichiasis, corneal opacity and blindness.
It also includes aspects of trachoma natural history, such as an increasing
rate of recovery from infection and a decreasing chlamydial load with
subsequent infections that depend upon a (presumed) acquired immunity that
clears infection with age more rapidly. Parameters were estimated using
maximum likelihood by fitting the model to pre-control infection prevalence
data from hypo-, meso- and hyperendemic communities from The Gambia and
Tanzania. The model reproduces key features of trachoma epidemiology: 1) the
age-profile of infection prevalence, which increases to a peak at very young
ages and declines at older ages; 2) a shift in this prevalence peak, toward
younger ages in higher force of infection environments; 3) a raised overall
profile of infection prevalence with higher force of infection; and 4) a
rising profile, with age, of the prevalence of the ensuing severe sequelae
(trachomatous scarring, trichiasis), as well as estimates of the number of
infections that need to occur before these sequelae appear.

Conclusions/Significance

We present a framework that is sufficiently comprehensive to examine the
outcomes of the A (antibiotic) component of the SAFE strategy on disease. The
suitability of the model for representing population-level patterns of
infection and disease sequelae is discussed in view of the individual
processes leading to these patterns.

Author Summary

Trachoma is the worldwide leading infectious cause of blindness and is due to
repeated conjunctival infection with Chlamydia trachomatis bacteria. The
effects of control interventions on population levels of infection and active
disease can be promptly measured, but the effects on severe ocular disease
outcomes require long-term monitoring. We present a mathematical model of
trachoma transmission and disease to predict the impact of interventions on
blinding trachoma. The model is based on the concept of multiple re-infections
leading to progressive scarring of the eye and the potentially blinding
disease sequelae. It includes aspects of trachoma natural history such as an
increasing rate of recovery from infection, and a decreasing chlamydial load
with subsequent infections. The model reproduces key features of trachoma
epidemiology such as the age-profile of infection prevalence; a shift in the
prevalence peak toward younger ages in higher-transmission environments; and a
rising profile of the prevalence of the severe sequelae (scarring,
trichiasis), as well as estimates of the number of</field></doc>
</add>
