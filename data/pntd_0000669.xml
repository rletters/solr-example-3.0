<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000669</field>
<field name="doi">10.1371/journal.pntd.0000669</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Claudia Bönsch, Christoph Kempf, Ivo Mueller, Laurens Manning, Moses Laman, Timothy M. E. Davis, Carlos Ros</field>
<field name="title">Chloroquine and Its Derivatives Exacerbate B19V-Associated Anemia by Promoting Viral Replication</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">4</field>
<field name="pages">e669</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

An unexpectedly high seroprevalence and pathogenic potential of human
parvovirus B19 (B19V) have been observed in certain malaria-endemic countries
in parallel with local use of chloroquine (CQ) as first-line treatment for
malaria. The aims of this study were to assess the effect of CQ and other
common antimalarial drugs on B19V infection in vitro and the possible
epidemiological consequences for children from Papua New Guinea (PNG).

Methodology/Principal Findings

Viral RNA, DNA and proteins were analyzed in different cell types following
infection with B19V in the presence of a range of antimalarial drugs.
Relationships between B19V infection status, prior 4-aminoquinoline use and
anemia were assessed in 200 PNG children &lt;10 years of age participating in a
case-control study of severe infections. In CQ-treated cells, the synthesis of
viral RNA, DNA and proteins was significantly higher and occurred earlier than
in control cells. CQ facilitates B19V infection by minimizing intracellular
degradation of incoming particles. Only amodiaquine amongst other antimalarial
drugs had a similar effect. B19V IgM seropositivity was more frequent in 111
children with severe anemia (hemoglobin &lt;50 g/L) than in 89 healthy controls
(15.3% vs 3.4%; P = 0.008). In children who were either B19V IgM or PCR
positive, 4-aminoquinoline use was associated with a significantly lower
admission hemoglobin concentration.

Conclusions/Significance

Our data strongly suggest that 4-aminoquinoline drugs and their metabolites
exacerbate B19V-associated anemia by promoting B19V replication. Consideration
should be given for choosing a non-4-aminoquinoline drug to partner
artemisinin compounds in combination antimalarial therapy.

Author Summary

Human parvovirus B19 (B19V) is typically associated with a childhood febrile
illness known as erythema infectiosum. The infection usually resolves without
consequence in healthy individuals. However, in patients with immunologic
and/or hematologic disorders, B19V can cause a significant pathology. The
virus infects and kills red cell precursors but anemia rarely supervenes
unless there is pre-existing anemia such as in children living in malaria-
endemic regions. The link between B19V infection and severe anemia has,
however, only been confirmed in certain malaria-endemic countries in parallel
with chloroquine (CQ) usage. This raises the possibility that CQ may increase
the risk of severe anemia by promoting B19V infection. To test this
hypothesis, we examined the direct effect of CQ and other commonly used
antimalarial drugs on B19V infection in cultured cell lines. Additionally, we
examined the correlation between B19V infection, hemoglobin levels and use of
CQ in children from Papua New Guinea hospitalized with severe anemia. The
results suggest strongly that CQ and its derivatives aggravate B19V-associated
anemia by promoting B19V replication. Hence, careful consideration should be
given in choosing the drug partnering artemisinin compounds in combination
antimalarial therapy in order to minimize contribution of B19V to severe
anemia.

Introduction

Human parvovirus B19 (B19V) is a nonenveloped icosahedral virus with a single-
stranded DNA genome which has been classified within the Erythrovir</field></doc>
</add>
