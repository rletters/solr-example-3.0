<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001677</field>
<field name="doi">10.1371/journal.pntd.0001677</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Ralf Ignatius, Jean Bosco Gahutu, Christian Klotz, Christian Steininger, Cyprien Shyirambere, Michel Lyng, Andre Musemakweri, Toni Aebischer, Peter Martus, Gundel Harms, Frank P. Mockenhaupt</field>
<field name="title">High Prevalence of Giardia duodenalis Assemblage B Infection and Association with Underweight in Rwandan Children</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">6</field>
<field name="pages">e1677</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Giardia duodenalis is highly endemic in East Africa but its effects on child
health, particularly of submicroscopic infections, i.e., those below the
threshold of microscopy, and of genetic subgroups (assemblages), are not well
understood. We aimed at addressing these questions and at examining
epidemiological characteristics of G. duodenalis in southern highland Rwanda.

Methodology/Principal Findings

In 583 children &lt;5 years of age from communities and health facilities,
intestinal parasites were assessed by triplicate light microscopy and by PCR
assays, and G. duodenalis assemblages were genotyped. Cluster effects of
villages were taken into account in statistical analysis. The prevalence of G.
duodenalis as detected by microscopy was 19.8% but 60.1% including PCR
results. Prevalence differed with residence, increased with age, and was
reduced by breastfeeding. In 492 community children without, with
submicroscopic and with microscopic infection, underweight (weight-for-age
z-score &lt;−2 standard deviations) was observed in 19.7%, 22.1%, and 33.1%,
respectively, and clinically assessed severe malnutrition in 4.5%, 9.5%, and
16.7%. Multivariate analysis identified microscopically detectable G.
duodenalis infection as an independent predictor of underweight and clinically
assessed severe malnutrition. Submicroscopic infection showed respective
trends. Overall, G. duodenalis was not associated with gastrointestinal
symptoms but assemblages A parasites (proportion, 13%) were increased among
children with vomiting and abdominal pain.

Conclusions/Significance

The prevalence of G. duodenalis in high-endemicity areas may be greatly
underestimated by light microscopy, particularly when only single stool
samples are analysed. Children with submicroscopic infections show limited
overt manifestation, but constitute unrecognized reservoirs of transmission.
The predominance of assemblage B in Rwanda may be involved in the seemingly
unimposing manifestation of G. duodenalis infection. However, the association
with impaired child growth points to its actual relevance. Longitudinal
studies considering abundant submicroscopic infections are needed to clarify
the actual contribution of G. duodenalis to morbidity in areas of high
endemicity.

Author Summary

Giardia duodenalis is a protozoan parasite causing gastroenteritis. Although
the parasite occurs worldwide, its regional prevalence varies considerably.
Using PCR as a highly sensitive molecular diagnostic tool, we detected G.
duodenalis in 60% of 583 children younger than five years in southern Rwanda.
It was by far the most frequent intestinal parasite detected in this
population. Importantly, two out of three infections would have been
undetected if only the commonly used light microscopy had been applied.
Genotyping revealed the presence of two distinct types of parasites, and only
the infrequent subtype showed a weak association with gastrointestinal
symptoms. However, G. duodenalis infection was associated with underweight and
clinically assessed severe malnutrition. The data call for the establishment
of more sensitive than light microscopy, yet simple diagnostic to</field></doc>
</add>
