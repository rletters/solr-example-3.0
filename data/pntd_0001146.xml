<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0001146</field>
<field name="doi">10.1371/journal.pntd.0001146</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Enrico Brunetti, Hector H. Garcia, Thomas Junghanss, on behalf of the members of the International CE Workshop in Lima, Peru, 2009</field>
<field name="title">Cystic Echinococcosis: Chronic, Complex, and Still Neglected</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">7</field>
<field name="pages">e1146</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
The Overall Scene

Cystic echinococcosis (CE), an infection with the larval form of the dog
tapeworm Echinococcus granulosus, still causes serious lung and liver disease
with a worldwide geographical distribution. This parasitic infection is
preventable, eliminable, and treatable—in theory. The biological cycle can be
attacked at various points: regular dog deworming, controlled sheep
slaughtering, vaccination of the intermediate (sheep) animal host, and
possibly in the future, vaccination of the definitive (dog) animal host
(Figure 1). However, breaking the cycle in practice is difficult and requires
long-lasting efforts. Control programs are expensive to set up and sustain.
With the currently available options, a period of 20 years is needed to reach
elimination, a goal that, unsurprisingly, has only been reached in rich
countries [1].

Figure 1

Life cycle of Echinococcus granulosus in a community of the Middle Atlas
region, Morocco.

(We thank M. Kachani, College of Veterinary Medicine, Western University of
Health Sciences, for the pictures.)

At the current pace of control, patients suffering from CE will be seen for
many decades to come. CE disease is chronic, complex, and neglected [2]–[4].
It is still poorly understood, and recommendations for diagnosis and treatment
have not progressed beyond expert opinions and are not necessarily adopted by
clinicians because of lack of grade I evidence.

The critical issues are:

1. CE may develop silently over years and even decades until it surfaces with signs and symptoms or as a chance finding on an ultrasound (US) scan or chest X-rays requested for unrelated reasons. Clinical manifestations may mean that the cyst is already complicated, e.g., ruptured into the biliary or bronchial tree, secondarily infected with bacteria, or leaking and causing allergic reactions if not anaphylactic shock.

2. Screening large samples of populations to detect asymptomatic cases is expensive. As with all screening procedures, ethical issues arise: do all patients in whom cysts are found require treatment? Is the treatment which we then offer well established and safe? And is it available at all? Screening projects in endemic areas are often inadequately prepared, as the clinical management is not provided locally for those who are found positive.

Problems start with the screening tool. With the exception of liver US, the
available methods are far from satisfactory. In regards to serology, the
sensitivity and specificity of several antigens have been well defined [5],
[6], but available assays still lack standardization, sensitivity, and
specificity [7]. Controversies on the usefulness for clinical diagnosis and
screening remain unresolved [8]. Serodiagnostic performance depends on several
factors, such as cyst location, cyst stage, and even cyst size, but these and
other variables have not been thoroughly assessed to date.

Ultrasound is an indispensable tool, but will likely miss very small cysts,
and its efficacy is mostly restricted to intraabdominal organs. Additionally,
some cyst stages may be difficult to distinguish from non-parasitic cysts,
which are common. The problem continues when an echinococcal cyst has been
diagnosed. In settings where health care facilities are several days of travel
away from the</field></doc>
</add>
