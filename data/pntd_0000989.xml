<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000989</field>
<field name="doi">10.1371/journal.pntd.0000989</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Laura K. Sirot, Melissa C. Hardstone, Michelle E. H. Helinski, José M. C. Ribeiro, Mari Kimura, Prasit Deewatthanawong, Mariana F. Wolfner, Laura C. Harrington</field>
<field name="title">Towards a Semen Proteome of the Dengue Vector Mosquito: Protein Identification and Potential Functions</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">3</field>
<field name="pages">e989</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

No commercially licensed vaccine or treatment is available for dengue fever, a
potentially lethal infection that impacts millions of lives annually. New
tools that target mosquito control may reduce vector populations and break the
cycle of dengue transmission. Male mosquito seminal fluid proteins (Sfps) are
one such target since these proteins, in aggregate, modulate the reproduction
and feeding patterns of the dengue vector, Aedes aegypti. As an initial step
in identifying new targets for dengue vector control, we sought to identify
the suite of proteins that comprise the Ae. aegypti ejaculate and determine
which are transferred to females during mating.

Methodology and Principal Findings

Using a stable-isotope labeling method coupled with proteomics to distinguish
male- and female-derived proteins, we identified Sfps and sperm proteins
transferred from males to females. Sfps were distinguished from sperm proteins
by comparing the transferred proteins to sperm-enriched samples derived from
testes and seminal vesicles. We identified 93 male-derived Sfps and 52
predicted sperm proteins that are transferred to females during mating. The
Sfp protein classes we detected suggest roles in protein
activation/inactivation, sperm utilization, and ecdysteroidogenesis. We also
discovered that several predicted membrane-bound and intracellular proteins
are transferred to females in the seminal fluids, supporting the hypothesis
that Ae. aegypti Sfps are released from the accessory gland cells through
apocrine secretion, as occurs in mammals. Many of the Ae. aegypti predicted
sperm proteins were homologous to Drosophila melanogaster sperm proteins,
suggesting conservation of their sperm-related function across Diptera.

Conclusion and Significance

This is the first study to directly identify Sfps transferred from male Ae.
aegypti to females. Our data lay the groundwork for future functional analyses
to identify individual seminal proteins that may trigger female post-mating
changes (e.g., in feeding patterns and egg production). Therefore,
identification of these proteins may lead to new approaches for manipulating
the reproductive output and vectorial capacity of Ae. aegypti.

Author Summary

Dengue is a potentially lethal infection that impacts millions of humans
annually. This disease is caused by viruses transmitted by infected female
Aedes aegypti mosquitoes during blood feeding. No commercial vaccine or
treatment is available for dengue infection. One way to break the disease
transmission cycle is to develop new tools to reduce dengue vector
populations. Seminal fluid proteins (Sfps) produced in the reproductive glands
of male mosquitoes and transferred to females in the ejaculate during mating
could be the target of such a tool. In related insects, Sfps modulate female
reproduction and feeding patterns. Here we report 145 proteins that are
transferred to females in the Ae. aegypti ejaculate. The proteins, which
include Sfps and sperm proteins, fall into biochemical classes that suggest
important potential roles in mated females. Of particular interest are
proteins that could play roles in fertility and hormonal activity (including
pathways involved in egg develop</field></doc>
</add>
