<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000176</field>
<field name="doi">10.1371/journal.pntd.0000176</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Luis Fernando Chaves, Justin M. Cohen, Mercedes Pascual, Mark L. Wilson</field>
<field name="title">Social Exclusion Modifies Climate and Deforestation Impacts on a Vector-Borne Disease</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">2</field>
<field name="pages">e176</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The emergence of American Cutaneous Leishmaniasis (ACL) has been associated
with changes in the relationship between people and forests, leading to the
view that forest ecosystems increase infection risk and subsequent proposal
that deforestation could reduce re-emergence of this disease.

Methodology/Principal Findings

We analyzed county-level incidence rates of ACL in Costa Rica (1996–2000) as a
function of social and environmental variables relevant to transmission
ecology with statistical models that incorporate breakpoints. Once social
marginality was taken into account, the effect of living close to a forest on
infection risk was small, and diminished exponentially above a breakpoint.
Forest cover was associated with the modulation of temporal effects of El Niño
Southern Oscillation (ENSO) at small spatial scales, revealing an additional
complex interplay of environmental forces and disease patterns.

Conclusions/Significance

Social factors, which previously have not been evaluated rigorously together
with environmental and climatic factors, appear to play a critical role that
may ultimately determine disease risk.

Author Summary

American Cutaneous Leishmaniasis emergence has been associated with changes in
the interaction between people and forests. The association between outbreaks
and forest clearance, higher risk for populations living close to forests, and
the absence of this disease from urban settings has led to the proposal that
it will disappear with the destruction of primary forests. This view ignores
the complex nature of deforestation as a product of socioeconomic inequities.
Our study shows that such inequities, as measured by a marginalization index,
may ultimately determine risk within the country, with socially excluded
populations most affected by the disease. Contrary to the established view,
living close to the forest edge can diminish the risk provided other factors
are taken into account. Additionally, differences in vulnerability to climatic
variability appear to interact with forest cover to influence risk across
counties where the disease has its largest burden. Incidence exacerbation
associated with El Niño Southern Oscillation is observed in counties with
larger proportions of deforestation. Our study calls for control efforts
targeted to socially excluded populations and for more localized ecological
studies of transmission in vectors and reservoirs in order to understand the
role of biodiversity changes in driving the emergence of this disease.

Introduction

American cutaneous leishmaniasis (ACL), a neglected infectious disease
[1]–[4], is one of the main emerging and re-emerging vector-borne diseases in
the Americas. It is a zoonotic vector-borne disease, caused by several species
of Leishmania (Kinetoplastida: Trypanosomatidae) parasites and transmitted by
sand flies (Diptera: Psychodidae). The (re)emergence of ACL has been
associated with deforestation in the neotropics. For example, infection is
highest among people living close to forest edges [5],[6], and also elevated
among workers that extract natural resources in forested areas [7],[8]. This
association with forest proximity/deforestation has led to the view that
large-scale landscape transformation may reduce ACL emergence [6],[8],[9].
However, studies of ACL</field></doc>
</add>
