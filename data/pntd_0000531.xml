<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000531</field>
<field name="doi">10.1371/journal.pntd.0000531</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Gustavo Ponce García, Adriana E. Flores, Ildefonso Fernández-Salas, Karla Saavedra-Rodríguez, Guadalupe Reyes-Solis, Saul Lozano-Fuentes, J. Guillermo Bond, Mauricio Casas-Martínez, Janine M. Ramsey, Julián García-Rejón, Marco Domínguez-Galera, Hilary Ranson, Janet Hemingway, Lars Eisen, William C. Black IV</field>
<field name="title">Recent Rapid Rise of a Permethrin Knock Down Resistance Allele in Aedes aegypti in México</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">10</field>
<field name="pages">e531</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Aedes aegypti, the ‘yellow fever mosquito’, is the primary vector to humans of
dengue and yellow fever flaviviruses (DENV, YFV), and is a known vector of the
chikungunya alphavirus (CV). Because vaccines are not yet available for DENV
or CV or are inadequately distributed in developing countries (YFV),
management of Ae. aegypti remains the primary option to prevent and control
outbreaks of the diseases caused by these arboviruses. Permethrin is one of
the most widely used active ingredients in insecticides for suppression of
adult Ae. aegypti. In 2007, we documented a replacement mutation in codon
1,016 of the voltage-gated sodium channel gene (para) of Ae. aegypti that
encodes an isoleucine rather than a valine and confers resistance to
permethrin. Ile1,016 segregates as a recessive allele conferring knockdown
resistance to homozygous mosquitoes at 5–10 µg of permethrin in bottle
bioassays.

Methods and Findings

A total of 81 field collections containing 3,951 Ae. aegypti were made
throughout México from 1996 to 2009. These mosquitoes were analyzed for the
frequency of the Ile1,016 mutation using a melting-curve PCR assay. Dramatic
increases in frequencies of Ile1,016 were recorded from the late 1990&apos;s to
2006–2009 in several states including Nuevo León in the north, Veracruz on the
central Atlantic coast, and Yucatán, Quintana Roo and Chiapas in the south.
From 1996 to 2000, the overall frequency of Ile1,016 was 0.04% (95% confidence
interval (CI95) = 0.12%; n = 1,359 mosquitoes examined). The earliest
detection of Ile1,016 was in Nuevo Laredo on the U.S. border in 1997. By
2003–2004 the overall frequency of Ile1,016 had increased ∼100-fold to 2.7%
(±0.80% CI95; n = 808). When checked again in 2006, the frequency had
increased slightly to 3.9% (±1.15% CI95; n = 473). This was followed in
2007–2009 by a sudden jump in Ile1,016 frequency to 33.2% (±1.99% CI95; n =
1,074 mosquitoes). There was spatial heterogeneity in Ile1,016 frequencies
among 2007–2008 collections, which ranged from 45.7% (±2.00% CI95) in the
state of Veracruz to 51.2% (±4.36% CI95) in the Yucatán peninsula and 14.5%
(±2.23% CI95) in and around Tapachula in the state of Chiapas. Spatial
heterogeneity was also evident at smaller geographic scales. For example
within the city of Chetumal, Quintana Roo, Ile1,016 frequencies varied from
38.3%–88.3%. A linear regression analysis based on seven collections from 2007
revealed that the frequency of Ile1,016 homozygotes accurately predicted
knockdown rate for mosquitoes exposed to permethrin in a bioassay (R2 = 0.98).

Conclusions

We have recorded a dramatic increase in the frequency of the Ile1,016 mutation
in the voltage-gated sodium channel gene of Ae. aegypti in México from 1996 to
2009. This may be related to heavy use of permethrin-based insecticides in
mosquito control programs. Spatial heterogeneity in Ile1,016 frequencies in
2007 and 2008 collections may reflect differences in selection pressure or in
the initial frequency of Ile1,016. The ra</field></doc>
</add>
