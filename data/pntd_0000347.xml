<add>
<doc>
<field name="uid">doi:10.1371/journal.pntd.0000347</field>
<field name="doi">10.1371/journal.pntd.0000347</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Sue Welburn, Kim Picozzi, Paul G. Coleman, Craig Packer</field>
<field name="title">Patterns in Age-Seroprevalence Consistent with Acquired Immunity against Trypanosoma brucei in Serengeti Lions</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">12</field>
<field name="pages">e347</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Trypanosomes cause disease in humans and livestock throughout sub-Saharan
Africa. Although various species show evidence of clinical tolerance to
trypanosomes, until now there has been no evidence of acquired immunity to
natural infections. We discovered a distinct peak and decrease in age
prevalence of T. brucei s.l. infection in wild African lions that is
consistent with being driven by an exposure-dependent increase in cross-
immunity following infections with the more genetically diverse species, T.
congolense sensu latu. The causative agent of human sleeping sickness, T.
brucei rhodesiense, disappears by 6 years of age apparently in response to
cross-immunity from other trypanosomes, including the non-pathogenic
subspecies, T. brucei brucei. These findings may suggest novel pathways for
vaccinations against trypanosomiasis despite the notoriously complex antigenic
surface proteins in these parasites.

Author Summary

Trypanosomiasis is a major health threat in Africa, but vaccine development
has long been hampered by the extraordinarily diverse surface proteins of
these parasites. However, Serengeti lions show an asymptotic age prevalence of
the non-pathogenic Trypanosoma congolense in contrast to a strong peak and
decrease in age prevalence of the pathogenic T. brucei s.l.. This pattern
suggests that lions may gain cross-immunity to T. brucei from repeated
exposure to the more genetically diverse T. congolense. Although lions may
gain more effective cross immunity than other host species owing to their
frequent consumption of infected prey animals, these findings suggest possible
strategies for designing effective vaccines against sleeping sickness in
livestock and humans.

Introduction

Trypanosomes transmitted by tsetse flies are a major constraint to the health
and economic development of many of the poorest regions of Africa. Infections
in humans result in over 50,000 deaths each year, while animal trypanosomiasis
is one of the most important livestock diseases across Africa [1]. The search
for effective vaccines to protect both humans and their livestock populations
from the trypanosomiases has proved to be one of the greatest and most elusive
challenges in global public health [1]. This is because of the unique
mechanism of immune invasion employed by the trypanosomes.

Persistence of trypanosome infection depends on evasion of the host immune
response through a complex system of antigenic variation of the variant
surface glycoprotein (VSG) that shields the cell [2]. Once host antibodies
recognize any one VSG, trypanosomes expressing that VSG are killed. Antigenic
variation involves a stochastic switch in the VSG gene expressed from a
repertoire of possibly a thousand genes; the switching subset of trypanosomes
survives [2]. Although wildlife [3] and cattle [4] show evidence of clinical
tolerance to trypanosomes, until now there has been no evidence of acquired
immunity to natural infections. The absence of any natural examples of
immunity to trypanosomes has been a major constraint to vaccine development.

Materials and Methods

Study design

184 blood samples were taken from 179 Serengeti lions (Panthera leo) between
1984 and 1994 as part of long-term ecological and epidemiological studies [5].
The Serengeti lions have been studied continuously since 1</field></doc>
</add>
